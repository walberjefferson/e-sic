<?php

namespace Esic\Criteria;

use function foo\func;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class FindSolicitacaoOrgaoAndDiligenciaCriteria
 * @package namespace Esic\Criteria;
 */
class FindSolicitacaoOrgaoAndDiligenciaCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = auth()->user();
        $tipoMovimentacao = DB::table('tipo_movimentacao')->where([['nome', 'like', 'Diligenc%']])->first();
        $movimentacoes = DB::table('movimentacao')->whereIn('orgao_destino_id', $user->listOrgaos())->where('tipo_movimentacao_id', '=', $tipoMovimentacao->id)->get();

        $movimentacoes_id = collect([]);

        $movimentacoes->each(function ($value) use ($movimentacoes_id) {
            $movimentacoes_id->push($value->solicitacao_id);
        });

        $dados = $model->whereIn('orgao_id', $user->listOrgaos())
            ->orWhere(function ($query) use ($movimentacoes_id) {
                $query->whereIn('id', $movimentacoes_id);
            });
        return $dados;
    }
}
