<?php

namespace Esic\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FindWhereActiveCriteria
 * @package namespace Esic\Criteria;
 */
class FindWhereActiveCriteria implements CriteriaInterface
{
    /**
     * @var
     */
    private $field;
    /**
     * @var
     */
    private $value;

    /**
     * FindWhereActiveCriteria constructor.
     */
    public function __construct($field = 'ativo', $value = true)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, $this->value);
    }
}
