<?php

namespace Esic\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FindWhereCriteria
 * @package namespace Esic\Criteria;
 */
class FindWhereCriteria implements CriteriaInterface
{
    private $where;
    /**
     * FindWhereCriteria constructor.
     * @param null $field
     */
    public function __construct($where = null)
    {
        $this->where = $where;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->where);
    }
}
