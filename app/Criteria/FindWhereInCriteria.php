<?php

namespace Esic\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FindWhereInCriteria
 * @package namespace Esic\Criteria;
 */
class FindWhereInCriteria implements CriteriaInterface
{
    private $field;
    private $value;

    public function __construct($field = null, $value = true)
    {
        $this->field = $field;
        $this->value = $value;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->field){
            return $model->whereIn($this->field, $this->value);
        }
        return $model;
    }
}
