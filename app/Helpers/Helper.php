<?php

namespace Esic\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;

class Helper
{
    public static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function limpaString($valor)
    {
        return preg_replace("/[^0-9]/", "", $valor);
    }

    public function uploadFile(Request $request, $field, $folder = 'solicitacao')
    {
        /** @var UploadedFile $file */
        $file = $request->file($field);
        if ($file->isValid() && $file->isFile()) {
            $inputsArquivo['nome'] = $file->getClientOriginalName();
            $inputsArquivo['url'] = $file->hashName();
            $inputsArquivo['tamanho'] = $file->getClientSize();
            $inputsArquivo['extensao'] = $file->getClientOriginalExtension();
            \Storage::disk('public')->putFile($folder, $file);
            return $inputsArquivo;
        }
        return false;
    }

    public function deleteFile($file, $folder = 'public/solicitacao')
    {
        $folder = storage_path($folder . $file);
        if (\File::exists($folder)) {
            \File::delete($folder);
        }
    }

    public static function meses($value)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        return $meses[($value - 1)];
    }
}