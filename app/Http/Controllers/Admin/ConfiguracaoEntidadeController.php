<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\ConfiguracaoEntidadeRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class ConfiguracaoEntidadeController extends Controller
{
    use ControllerTrait;

    /**
     * ConfiguracaoEntidadeController constructor.
     * @param ConfiguracaoEntidadeRepository $repository
     */
    public function __construct(ConfiguracaoEntidadeRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Configuração Cadastrada com Sucesso!',
            'update' => 'Configuração Alterada com Sucesso!',
            'destroy' => 'Configuração Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.config_entidade.index',
            'create' => 'admin.config_entidade.create',
            'store' => 'admin.config_entidade.store',
            'edit' => 'admin.config_entidade.edit',
            'update' => 'admin.config_entidade.update',
            'destroy' => 'admin.config_entidade.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Configuração Entidade',
            'folder' => 'admin.configuracao.entidade.',
            'index' => 'admin.configuracao.entidade.index',
            'create' => 'admin.configuracao.entidade.create',
            'show' => 'admin.configuracao.entidade.show',
            'edit' => 'admin.configuracao.entidade.edit',
        ];
        $this->repository = $repository;
    }

    public function index()
    {
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->all()->first();
        return view($this->view->index, compact('dados', 'view', 'rota'));
    }

    public function store(Request $request)
    {
        $dados = $this->repository->all();
        if ($dados->count()) {
            $this->repository->update($request->all(), $dados->first()->id);
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
        } else {
            $this->repository->create($request->all());
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->store);
        }
    }
}
