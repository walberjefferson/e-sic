<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\ConfiguracaoLaiRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class ConfiguracaoLaiController extends Controller
{
    use ControllerTrait;

    public function __construct(ConfiguracaoLaiRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Configuração Cadastrada com Sucesso!',
            'update' => 'Configuração Alterada com Sucesso!',
            'destroy' => 'Configuração Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.config_lai.index',
            'create' => 'admin.config_lai.create',
            'store' => 'admin.config_lai.store',
            'edit' => 'admin.config_lai.edit',
            'update' => 'admin.config_lai.update',
            'destroy' => 'admin.config_lai.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Configuração de Lei de Acesso a Informação',
            'folder' => 'admin.configuracao.lai.',
            'index' => 'admin.configuracao.lai.index',
            'create' => 'admin.configuracao.lai.create',
            'show' => 'admin.configuracao.lai.show',
            'edit' => 'admin.configuracao.lai.edit',
        ];
        $this->repository = $repository;
    }

    public function index()
    {
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->all()->first();
        return view($this->view->index, compact('dados', 'view', 'rota'));
    }


    public function store(Request $request)
    {
        $dados = $this->repository->all();
        if ($dados->count()) {
            $this->repository->update($request->all(), $dados->first()->id);
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
        } else {
            $this->repository->create($request->all());
            return redirect()->route($this->rotas->index)->with('message',  $this->mensagem->store);
        }
    }
}
