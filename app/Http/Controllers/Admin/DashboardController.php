<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Criteria\FindWhereInCriteria;
use Esic\Repositories\SolicitacaoRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * @var SolicitacaoRepository
     */
    private $repository;

    /**
     * DashboardController constructor.
     * @param SolicitacaoRepository $repository
     */
    public function __construct(SolicitacaoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $dados = (object)[
            'total' => $this->repository->all()->count(),
            'respondidas' => $this->repository->findByField('situacao_id', 2)->count(),
            'aguardando' => $this->repository->findByField('situacao_id', 1)->count(),
            'negada' => $this->repository->findByField('situacao_id', 3)->count(),
            'solicitacoes' => $this->repository->orderBy('created_at', 'desc')->paginate(7)
        ];
        $user = auth()->user();
        if (!$user->isAdmin()) {

            $orgaos_id = $user->orgaos()->get()->map(function ($item){
                return $item->id;
            });
            $this->repository->pushCriteria(new FindWhereInCriteria('orgao_id', $orgaos_id));
            $solicitacoes = $this->repository->orderBy('data_solicitacao', 'desc')->paginate(7);
        } else {
            $solicitacoes = $this->repository->orderBy('data_solicitacao', 'desc')->paginate(7);
        }
        return view('admin.dashboard.index', compact('dados', 'solicitacoes'));
    }
}
