<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\Controller;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\EscolaridadeRepository;

class EscolaridadeController extends Controller
{
    use ControllerTrait;
    /**
     * EscolaridadeController constructor.
     * @param EscolaridadeRepository $repository
     */
    public function __construct(EscolaridadeRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Escolaridade Cadastrada com Sucesso!',
            'update' => 'Escolaridade Alterada com Sucesso!',
            'destroy' => 'Escolaridade Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.escolaridade.index',
            'create' => 'admin.escolaridade.create',
            'store' => 'admin.escolaridade.store',
            'edit' => 'admin.escolaridade.edit',
            'update' => 'admin.escolaridade.update',
            'destroy' => 'admin.escolaridade.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Escolaridades',
            'folder' => 'admin.escolaridade.',
            'index' => 'admin.escolaridade.index',
            'create' => 'admin.escolaridade.create',
            'show' => 'admin.escolaridade.show',
            'edit' => 'admin.escolaridade.edit',
        ];
        $this->repository = $repository;
    }
}
