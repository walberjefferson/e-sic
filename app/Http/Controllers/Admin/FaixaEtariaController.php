<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\Controller;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\FaixaEtariaRepository;
use Illuminate\Http\Request;

class FaixaEtariaController extends Controller
{
    use ControllerTrait;

    /**
     * FaixaEtariaController constructor.
     * @param FaixaEtariaRepository $repository
     */
    public function __construct(FaixaEtariaRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Faixa Etária Cadastrada com Sucesso!',
            'update' => 'Faixa Etária Alterada com Sucesso!',
            'destroy' => 'Faixa Etária Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.faixa-etaria.index',
            'create' => 'admin.faixa-etaria.create',
            'store' => 'admin.faixa-etaria.store',
            'edit' => 'admin.faixa-etaria.edit',
            'update' => 'admin.faixa-etaria.update',
            'destroy' => 'admin.faixa-etaria.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Faixa Etária',
            'folder' => 'admin.faixa_etaria.',
            'index' => 'admin.faixa_etaria.index',
            'create' => 'admin.faixa_etaria.create',
            'show' => 'admin.faixa_etaria.show',
            'edit' => 'admin.faixa_etaria.edit',
        ];
        $this->repository = $repository;
    }
}
