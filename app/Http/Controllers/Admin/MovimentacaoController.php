<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Criteria\FindWhereInCriteria;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\MovimentacaoRepository;
use Esic\Http\Controllers\Controller;
use Esic\Repositories\OrgaoRepository;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Repositories\TipoMovimentacaoRepository;
use Esic\Repositories\UserRepository;

class MovimentacaoController extends Controller
{
    use ControllerTrait;
    /**
     * @var TipoMovimentacaoRepository
     */
    private $tipoMovimentacaoRepository;
    /**
     * @var SolicitacaoRepository
     */
    private $solicitacaoRepository;
    /**
     * @var OrgaoRepository
     */
    private $orgaoRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * MovimentacaoController constructor.
     * @param MovimentacaoRepository $repository
     * @param TipoMovimentacaoRepository $tipoMovimentacaoRepository
     * @param SolicitacaoRepository $solicitacaoRepository
     * @param OrgaoRepository $orgaoRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        MovimentacaoRepository $repository,
        TipoMovimentacaoRepository $tipoMovimentacaoRepository,
        SolicitacaoRepository $solicitacaoRepository,
        OrgaoRepository $orgaoRepository,
        UserRepository $userRepository
    )
    {
        $this->mensagem = (object)[
            'store' => 'Movimentação Cadastrada com Sucesso!',
            'update' => 'Movimentação Alterada com Sucesso!',
            'destroy' => 'Movimentação Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.movimentacao.index',
            'create' => 'admin.movimentacao.create',
            'store' => 'admin.movimentacao.store',
            'edit' => 'admin.movimentacao.edit',
            'update' => 'admin.movimentacao.update',
            'destroy' => 'admin.movimentacao.destroy',
            'show' => 'admin.movimentacao.show'
        ];

        $this->view = (object)[
            'titulo' => 'Movimentações',
            'folder' => 'admin.movimentacao.',
            'index' => 'admin.movimentacao.index',
            'create' => 'admin.movimentacao.create',
            'show' => 'admin.movimentacao.show',
            'edit' => 'admin.movimentacao.edit',
        ];
        $this->repository = $repository;
        $this->tipoMovimentacaoRepository = $tipoMovimentacaoRepository;
        $this->solicitacaoRepository = $solicitacaoRepository;
        $this->orgaoRepository = $orgaoRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $user = auth()->user();
        if(!$user->isAdmin()){
            $orgaos_id = [];
            foreach ($user->orgaos()->get() as $orgaos){
                $orgaos_id[] = $orgaos->id;
            }
            $this->repository->pushCriteria(new FindWhereInCriteria('orgao_origem_id', $orgaos_id));
        }
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->paginate();
        return view($this->view->index, compact('dados', 'rota', 'view'));
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $tipoMovimentacao = $this->tipoMovimentacaoRepository->pluck('nome', 'id');
        $solicitacoes = $this->solicitacaoRepository->pluck('numero_protocolo', 'id');
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        $usuarios = $this->userRepository->pluck('name', 'id');
        return view($this->view->create, compact('rota', 'view', 'tipoMovimentacao', 'solicitacoes', 'orgaos', 'usuarios'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $tipoMovimentacao = $this->tipoMovimentacaoRepository->pluck('nome', 'id');
        $solicitacoes = $this->solicitacaoRepository->pluck('numero_protocolo', 'id');
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        $usuarios = $this->userRepository->pluck('name', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'tipoMovimentacao', 'solicitacoes', 'orgaos', 'usuarios'));
    }
}
