<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\OrgaoRepository;
use Esic\Http\Controllers\Controller;

class OrgaoController extends Controller
{
    use ControllerTrait;
    /**
     * @var OrgaoRepository
     */
    private $repository;
    private $rotas;
    private $view;
    private $mensagem;

    /**
     * OrgaoController constructor.
     * @param OrgaoRepository $repository
     */
    public function __construct(OrgaoRepository $repository)
    {
        $this->repository = $repository;

        $this->mensagem = (object)[
            'store' => 'Orgão Cadastrado com Sucesso!',
            'update' => 'Orgão Alterado com Sucesso!',
            'destroy' => 'Orgão Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.orgao.index',
            'create' => 'admin.orgao.create',
            'store' => 'admin.orgao.store',
            'edit' => 'admin.orgao.edit',
            'update' => 'admin.orgao.update',
            'show' => 'admin.orgao.show',
            'destroy' => 'admin.orgao.destroy'
        ];

        $this->view = (object)[
            'folder' => 'admin.orgao.',
            'index' => 'admin.orgao.index',
            'create' => 'admin.orgao.create',
            'show' => 'admin.orgao.show',
            'edit' => 'admin.orgao.edit',
        ];
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $orgaos = $this->repository->pluck('nome', 'id');
        return view($this->view->create, compact('orgaos', 'rota', 'view'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $orgaos = $this->repository->pluck('nome', 'id');
        return view($this->view->edit, compact('orgaos', 'dados', 'rota', 'view'));
    }
}
