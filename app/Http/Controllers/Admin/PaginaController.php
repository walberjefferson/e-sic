<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\PaginaRepository;
use Esic\Repositories\PaginaRepositoryEloquent;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class PaginaController extends Controller
{
    use ControllerTrait;
    /**
     * @var PaginaRepository
     */
    private $repository;

    /**
     * PaginaController constructor.
     */
    public function __construct(PaginaRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Página Cadastrado com Sucesso!',
            'update' => 'Página Alterado com Sucesso!',
            'destroy' => 'Página Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.pagina.index',
            'create' => 'admin.pagina.create',
            'store' => 'admin.pagina.store',
            'edit' => 'admin.pagina.edit',
            'update' => 'admin.pagina.update',
            'destroy' => 'admin.pagina.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Páginas',
            'folder' => 'admin.pagina.',
            'index' => 'admin.pagina.index',
            'create' => 'admin.pagina.create',
            'show' => 'admin.pagina.show',
            'edit' => 'admin.pagina.edit',
        ];
        $this->permissoes = (object)[
            'view' => 'pagina.view',
            'create' => 'pagina.create',
            'update' => 'pagina.update',
            'destroy' => 'pagina.destroy',
        ];
        $this->repository = $repository;
    }

    public function edit($slug)
    {
        if(isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->findByField('slug', $slug)->first();
        return view($this->view->edit, compact('dados', 'rota', 'view'));
    }
}
