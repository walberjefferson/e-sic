<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\PerfilRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class PerfilController extends Controller
{
    use ControllerTrait;

    private $repository;

    /**
     * PerfilController constructor.
     * @param PerfilRepository $repository
     */
    public function __construct(PerfilRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Perfil Cadastrado com Sucesso!',
            'update' => 'Perfil Alterado com Sucesso!',
            'destroy' => 'Perfil Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.perfil.index',
            'create' => 'admin.perfil.create',
            'store' => 'admin.perfil.store',
            'edit' => 'admin.perfil.edit',
            'update' => 'admin.perfil.update',
            'destroy' => 'admin.perfil.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Perfil',
            'folder' => 'admin.perfil.',
            'index' => 'admin.perfil.index',
            'create' => 'admin.perfil.create',
            'show' => 'admin.perfil.show',
            'edit' => 'admin.perfil.edit',
        ];
        $this->repository = $repository;
    }
}
