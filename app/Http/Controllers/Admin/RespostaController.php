<?php

namespace Esic\Http\Controllers\Admin;

use Carbon\Carbon;
use DB;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Mail\SolicitacaoRespostaAnexo;
use Esic\Repositories\OrgaoRepository;
use Esic\Repositories\RespostaRepository;
use Esic\Repositories\SituacaoRepository;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Repositories\SolicitanteRepository;
use Esic\Repositories\UserRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class RespostaController extends Controller
{
    use ControllerTrait;
    /**
     * @var RespostaRepository
     */
    private $repository;
    /**
     * @var OrgaoRepository
     */
    private $orgaoRepository;
    /**
     * @var SolicitanteRepository
     */
    private $solicitanteRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var SolicitacaoRepository
     */
    private $solicitacaoRepository;
    /**
     * @var SituacaoRepository
     */
    private $situacaoRepository;

    /**
     * RespostaController constructor.
     * @param RespostaRepository $repository
     * @param OrgaoRepository $orgaoRepository
     * @param SolicitacaoRepository $solicitacaoRepository
     * @param SituacaoRepository $situacaoRepository
     * @param UserRepository $userRepository
     * @internal param SolicitanteRepository $solicitanteRepository
     */
    public function __construct(
        RespostaRepository $repository,
        OrgaoRepository $orgaoRepository,
        SolicitacaoRepository $solicitacaoRepository,
        SituacaoRepository $situacaoRepository,
        UserRepository $userRepository
    )
    {
        $this->mensagem = (object)[
            'store' => 'Resposta Cadastrado com Sucesso!',
            'update' => 'Resposta Alterado com Sucesso!',
            'destroy' => 'Resposta Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.resposta.index',
            'create' => 'admin.resposta.create',
            'store' => 'admin.resposta.store',
            'edit' => 'admin.resposta.edit',
            'update' => 'admin.resposta.update',
            'destroy' => 'admin.resposta.destroy',
            'show' => 'admin.resposta.show'
        ];

        $this->view = (object)[
            'titulo' => 'Respostas',
            'folder' => 'admin.resposta.',
            'index' => 'admin.resposta.index',
            'create' => 'admin.resposta.create',
            'show' => 'admin.resposta.show',
            'edit' => 'admin.resposta.edit',
        ];
        $this->repository = $repository;
        $this->orgaoRepository = $orgaoRepository;
        $this->userRepository = $userRepository;
        $this->solicitacaoRepository = $solicitacaoRepository;
        $this->situacaoRepository = $situacaoRepository;
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        $solicitacao = $this->solicitacaoRepository->pluck('numero_protocolo', 'id');
        $usuarios = $this->userRepository->pluck('name', 'id');
        return view($this->view->create, compact('rota', 'view', 'orgaos', 'solicitacao', 'usuarios'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        $solicitacao = $this->solicitacaoRepository->pluck('numero_protocolo', 'id');
        $usuarios = $this->userRepository->pluck('name', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'orgaos', 'solicitacao', 'usuarios'));
    }

    public function update(Request $request, $id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $this->repository->update($request->except(['homologada', 'data_homologacao']), $id);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
    }

    public function homologar($solicitacao_id, $resposta_id)
    {
        try {
            DB::beginTransaction();

            $situacao = $this->situacaoRepository->findWhere([['nome', 'like', 'respondido%']])->first()->id;
            $resposta = $this->repository->with('anexos')
                ->withCount('anexos')
                ->find($resposta_id);
            $dados = ['homologada' => true, 'data_homologacao' => Carbon::now()->format('d/m/Y'), 'usuario_homologado_id' => auth()->id()];
            $solicitacao = $this->solicitacaoRepository->find($solicitacao_id);
            $solicitacao->situacao_id = $situacao;
            $solicitacao->save();
            $this->repository->update($dados, $resposta_id);
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoRespostaAnexo("Solicitação Respondida", $solicitacao, $resposta));
//            \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Solicitação Respondida"));
            }
            DB::commit();
            return redirect()->route('admin.solicitacao.show', $solicitacao_id)
                ->with('message', 'Resposta homologada com sucesso!');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('erro', 'Não foi possível homologar resposta. - ' . $e->getMessage());
        }
    }
}
