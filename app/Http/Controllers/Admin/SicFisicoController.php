<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\OrgaoRepository;
use Esic\Repositories\SicFisicoRepository;
use File;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class SicFisicoController extends Controller
{
    use ControllerTrait;


    /**
     * @var OrgaoRepository
     */
    private $orgaoRepository;

    public function __construct(SicFisicoRepository $repository, OrgaoRepository $orgaoRepository)
    {
        $this->mensagem = (object)[
            'store' => 'SIC Físico Cadastrada com Sucesso!',
            'update' => 'SIC Físico Alterada com Sucesso!',
            'destroy' => 'SIC Físico Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.sic_fisico.index',
            'create' => 'admin.sic_fisico.create',
            'store' => 'admin.sic_fisico.store',
            'edit' => 'admin.sic_fisico.edit',
            'update' => 'admin.sic_fisico.update',
            'destroy' => 'admin.sic_fisico.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'SIC Físico',
            'folder' => 'admin.configuracao.sic_fisico.',
            'index' => 'admin.configuracao.sic_fisico.index',
            'create' => 'admin.configuracao.sic_fisico.create',
            'show' => 'admin.configuracao.sic_fisico.show',
            'edit' => 'admin.configuracao.sic_fisico.edit',
        ];
        $this->repository = $repository;
        $this->orgaoRepository = $orgaoRepository;
    }

    public function index()
    {
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->all()->first();
        return view($this->view->index, compact('dados', 'view', 'rota'));
    }

    public function create()
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->create);
        $rota = $this->rotas;
        $view = $this->view;
        $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
        return view($this->view->create, compact('rota', 'view', 'orgaos'));
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $dados = $this->repository->all();
        $inputs = $request->all();
        if ($dados->count()) {
            $decreto = $this->upload($request, 'decreto');
            if ($decreto) {
                $inputs['decreto'] = $decreto;
            }
            $requerimento = $this->upload($request, 'formulario_requerimento');
            if ($requerimento) {
                $inputs['formulario_requerimento'] = $requerimento;
            }
            $recurso = $this->upload($request, 'formulario_recurso');
            if ($recurso) {
                $inputs['formulario_recurso'] = $recurso;
            }
            $this->repository->update($inputs, $dados->first()->id);
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
        } else {
            $decreto = $this->upload($request, 'decreto');
            if ($decreto) {
                $inputs['decreto'] = $decreto;
            }
            $requerimento = $this->upload($request, 'formulario_requerimento');
            if ($requerimento) {
                $inputs['formulario_requerimento'] = $requerimento;
            }
            $recurso = $this->upload($request, 'formulario_recurso');
            if ($recurso) {
                $inputs['formulario_recurso'] = $recurso;
            }
            $this->repository->create($inputs);
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->store);
        }
    }

    public function edit($id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'orgaos'));
    }

    public function update(Request $request, $id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $inputs = $request->all();
        $decreto = $this->upload($request, 'decreto');
        if ($decreto) {
            $inputs['decreto'] = $decreto;
        }
        $requerimento = $this->upload($request, 'formulario_requerimento');
        if ($requerimento) {
            $inputs['formulario_requerimento'] = $requerimento;
        }
        $recurso = $this->upload($request, 'formulario_recurso');
        if ($recurso) {
            $inputs['formulario_recurso'] = $recurso;
        }
        $this->repository->update($inputs, $id);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
    }

    protected function upload(Request $request, $field)
    {
        if ($request->hasFile($field)) {
            $arquivo = $request->file($field);
            $nome = explode('.' . $arquivo->getClientOriginalExtension(), $arquivo->getClientOriginalName());
            $nomeSalva = str_slug($nome[0]) . '-' . time() . '.' . $arquivo->getClientOriginalExtension();
            $diretorio = 'sic/';
            $arquivo->storePubliclyAs('public/' . $diretorio, $nomeSalva);
            return $nomeSalva;
        }
        return false;
    }

    public function destroyFile($id, $field)
    {
        $dados = $this->repository->find($id);
        $dir = storage_path('app/public/sic/' . $dados[$field]);
        if (File::exists($dir)) {
            File::delete($dir);
            $this->repository->update([$field => null], $id);
            return redirect()->route('admin.sic_fisico.index')->with('message', 'Arquivo excluído com sucesso!');
        }
        return redirect()->route('admin.sic_fisico.index')->with('message', 'Não foi possível excluri o arquivo!');
    }
}
