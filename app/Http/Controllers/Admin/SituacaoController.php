<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\SituacaoRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class SituacaoController extends Controller
{
    use ControllerTrait;
    private $repository;
    private $mensagem;
    private $rotas;
    private $view;

    /**
     * SituacaoController constructor.
     * @param SituacaoRepository $repository
     */
    public function __construct(SituacaoRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Situação Cadastrada com Sucesso!',
            'update' => 'Situação Alterada com Sucesso!',
            'destroy' => 'Situação Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.situacao.index',
            'create' => 'admin.situacao.create',
            'store' => 'admin.situacao.store',
            'edit' => 'admin.situacao.edit',
            'update' => 'admin.situacao.update',
            'destroy' => 'admin.situacao.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Situação',
            'folder' => 'admin.situacao.',
            'index' => 'admin.situacao.index',
            'create' => 'admin.situacao.create',
            'show' => 'admin.situacao.show',
            'edit' => 'admin.situacao.edit',
        ];

        $this->repository = $repository;
    }
}
