<?php

namespace Esic\Http\Controllers\Admin;

use Carbon\Carbon;
use Esic\Criteria\FindSolicitacaoOrgaoAndDiligenciaCriteria;
use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Criteria\FindWhereCriteria;
use Esic\Criteria\FindWhereInCriteria;
use Esic\Helpers\Helper;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Mail\SolicitacaoStatus;
use Esic\Models\ConfiguracaoLai;
use Esic\Models\Solicitante;
use Esic\Repositories\AnexoRepository;
use Esic\Repositories\MovimentacaoRepository;
use Esic\Repositories\OrgaoRepository;
use Esic\Repositories\ProrrogacaoRepository;
use Esic\Repositories\RespostaRepository;
use Esic\Repositories\SituacaoRepository;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Repositories\SolicitanteRepository;
use Esic\Repositories\TipoMovimentacaoRepository;
use Esic\Repositories\TipoRetornoRepository;
use Esic\Repositories\TipoSolicitacaoRepository;
use Esic\Repositories\UserRepository;
use Esic\Repositories\UsersSolicitanteRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class SolicitacaoController extends Controller
{
    use ControllerTrait;
    /**
     * @var SolicitacaoRepository
     */
    private $repository;
    private $rotas;
    private $view;
    private $mensagem;
    /**
     * @var SolicitanteRepository
     */
    private $solicitanteRepository;
    /**
     * @var SituacaoRepository
     */
    private $situacaoRepository;
    /**
     * @var TipoRetornoRepository
     */
    private $tipoRetornoRepository;
    /**
     * @var TipoSolicitacaoRepository
     */
    private $tipoSolicitacaoRepository;
    /**
     * @var UsersSolicitanteRepository
     */
    private $usersSolicitanteRepository;
    /**
     * @var RespostaRepository
     */
    private $respostaRepository;
    /**
     * @var OrgaoRepository
     */
    private $orgaoRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var Helper
     */
    private $helper;
    /**
     * @var AnexoRepository
     */
    private $anexoRepository;
    /**
     * @var MovimentacaoRepository
     */
    private $movimentacaoRepository;
    /**
     * @var TipoMovimentacaoRepository
     */
    private $tipoMovimentacaoRepository;
    /**
     * @var ConfiguracaoLai
     */
    private $configuracaoLai;
    /**
     * @var ProrrogacaoRepository
     */
    private $prorrogacaoRepository;

    /**
     * SolicitacaoController constructor.
     * @param SolicitacaoRepository $repository
     * @param SolicitanteRepository $solicitanteRepository
     * @param SituacaoRepository $situacaoRepository
     * @param TipoRetornoRepository $tipoRetornoRepository
     * @param UsersSolicitanteRepository $usersSolicitanteRepository
     * @param TipoSolicitacaoRepository $tipoSolicitacaoRepository
     * @param MovimentacaoRepository $movimentacaoRepository
     * @param TipoMovimentacaoRepository $tipoMovimentacaoRepository
     * @param RespostaRepository $respostaRepository
     * @param OrgaoRepository $orgaoRepository
     * @param UserRepository $userRepository
     * @param AnexoRepository $anexoRepository
     * @param ConfiguracaoLai $configuracaoLai
     * @param ProrrogacaoRepository $prorrogacaoRepository
     * @param Helper $helper
     */
    public function __construct(
        SolicitacaoRepository $repository,
        SolicitanteRepository $solicitanteRepository,
        SituacaoRepository $situacaoRepository,
        TipoRetornoRepository $tipoRetornoRepository,
        UsersSolicitanteRepository $usersSolicitanteRepository,
        TipoSolicitacaoRepository $tipoSolicitacaoRepository,
        MovimentacaoRepository $movimentacaoRepository,
        TipoMovimentacaoRepository $tipoMovimentacaoRepository,
        RespostaRepository $respostaRepository,
        OrgaoRepository $orgaoRepository,
        UserRepository $userRepository,
        AnexoRepository $anexoRepository,
        ConfiguracaoLai $configuracaoLai,
        ProrrogacaoRepository $prorrogacaoRepository,
        Helper $helper
    )
    {
        $this->mensagem = (object)[
            'store' => 'Solicitação Cadastrado com Sucesso!',
            'update' => 'Solicitação Alterado com Sucesso!',
            'destroy' => 'Solicitação Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.solicitacao.index',
            'create' => 'admin.solicitacao.create',
            'store' => 'admin.solicitacao.store',
            'edit' => 'admin.solicitacao.edit',
            'update' => 'admin.solicitacao.update',
            'show' => 'admin.solicitacao.show',
            'destroy' => 'admin.solicitacao.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Solicitações',
            'folder' => 'admin.solicitacao.',
            'index' => 'admin.solicitacao.index',
            'create' => 'admin.solicitacao.create',
            'show' => 'admin.solicitacao.show',
            'edit' => 'admin.solicitacao.edit',
        ];

        $this->repository = $repository;
        $this->solicitanteRepository = $solicitanteRepository;
        $this->situacaoRepository = $situacaoRepository;
        $this->tipoRetornoRepository = $tipoRetornoRepository;
        $this->tipoSolicitacaoRepository = $tipoSolicitacaoRepository;
        $this->usersSolicitanteRepository = $usersSolicitanteRepository;
        $this->respostaRepository = $respostaRepository;
        $this->orgaoRepository = $orgaoRepository;
        $this->userRepository = $userRepository;
        $this->helper = $helper;
        $this->anexoRepository = $anexoRepository;
        $this->movimentacaoRepository = $movimentacaoRepository;
        $this->tipoMovimentacaoRepository = $tipoMovimentacaoRepository;
        $this->configuracaoLai = $configuracaoLai;
        $this->prorrogacaoRepository = $prorrogacaoRepository;
    }

    public function index()
    {
        $user = auth()->user();
        if (!$user->isAdmin()) {
            $this->repository->pushCriteria(FindSolicitacaoOrgaoAndDiligenciaCriteria::class);
            $dados = $this->repository->paginate();
        } else {
            $dados = $this->repository->orderBy('data_solicitacao', 'desc')->paginate();
        }
        $view = $this->view;
        $rota = $this->rotas;
        return view($this->view->index, compact('dados', 'rota', 'view'));
    }

    public function show($id)
    {
        try {
            $dados = $this->repository->find($id);
            $this->authorize('solicitacao.view', $dados);
            $view = $this->view;
            $rota = $this->rotas;
            $this->orgaoRepository->pushCriteria(new FindWhereActiveCriteria('ativo', true));
            $orgaos = $this->orgaoRepository->findWhereNotIn('id', [$dados->orgao_id])->pluck('nome', 'id');
            $tramitacoes = $dados->movimentacao()->orderby('created_at', 'desc')->get();
            $respostas = $dados->respostas()->orderBy('created_at', 'DESC')->get();
            $diligencia = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Dilig%']])->first()->id;
            $negado = $this->situacaoRepository->findWhere([['nome', 'like', 'Neg%']])->first()->id;
            $recebido = empty($dados->movimentacao()->where('tipo_movimentacao_id', '!=', $diligencia)->get()->last()->data_recebimento);
            return view($this->view->show, compact('dados', 'rota', 'view', 'orgaos', 'tramitacoes', 'respostas', 'recebido', 'negado'));
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.index')->with('erro', 'Você não tem mais a permissão para visualizar essa solicitação!');
        }

    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $solicitacao = $this->repository->pluck('numero_protocolo', 'id');
        $solicitante = $this->usersSolicitanteRepository->pluck('name', 'id');
        $situacao = $this->situacaoRepository->pluck('nome', 'id');
        $tipo_retorno = $this->tipoRetornoRepository->pluck('nome', 'id');
        $tipo_solicitacao = $this->tipoSolicitacaoRepository->pluck('nome', 'id');
        return view($this->view->create, compact('rota', 'view', 'solicitacao', 'solicitante', 'situacao', 'tipo_retorno', 'tipo_solicitacao'));
    }

    public function edit($id)
    {
        try {
            $rota = $this->rotas;
            $view = $this->view;
            $dados = $this->repository->find($id);
            $this->authorize('solicitacao.edit', $dados);
            $solicitacao = $this->repository->pluck('numero_protocolo', 'id');
            $solicitante = $this->usersSolicitanteRepository->pluck('name', 'id');
            $situacao = $this->situacaoRepository->pluck('nome', 'id');
            $tipo_retorno = $this->tipoRetornoRepository->pluck('nome', 'id');
            $tipo_solicitacao = $this->tipoSolicitacaoRepository->pluck('nome', 'id');
            return view($this->view->edit, compact('dados', 'rota', 'view', 'solicitacao', 'solicitante', 'situacao', 'tipo_retorno', 'tipo_solicitacao'));
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.index')->with('erro', 'Você não tem mais a permissão para editar essa solicitação!');
        }
    }

    public function receber($id)
    {
        try {
            $solicitacao = $this->repository->find($id);
            $movimentacao = $solicitacao->movimentacao()->get()->last();
            if (!$movimentacao->data_recebimento) {
                $this->movimentacaoRepository->update([
                    'data_recebimento' => Carbon::now()->format('d/m/Y H:i:s'),
                    'usuario_recebimento_id' => auth()->id()
                ], $movimentacao->id);
            } else {
                return redirect()->route('admin.solicitacao.show', $id)->with('erro', "Solicitação não pode ser recebida novamente!");
            }
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Recebida"));
            }
            return redirect()->route('admin.solicitacao.show', $id)->with('message', "Solicitação Recebida com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.show', $id)->with('erro', "Solicitação não pode ser recebida!");
        }
    }

    public function receber_diligencia($id)
    {
        try {
            $solicitacao = $this->repository->find($id);
            $diligencia = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Dilig%']])->first()->id;
            $movimentacao = $solicitacao->movimentacao()->where('tipo_movimentacao_id', $diligencia)->whereNull('data_recebimento')->get()->last();
            if (!$movimentacao->data_recebimento) {
                $this->movimentacaoRepository->update([
                    'data_recebimento' => Carbon::now()->format('d/m/Y H:i:s'),
                    'usuario_recebimento_id' => auth()->id()
                ], $movimentacao->id);
            } else {
                return redirect()->route('admin.solicitacao.ver_diligencia', $id)->with('erro', "Solicitação não pode ser recebida novamente!");
            }
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Diligência Recebida"));
            }
            return redirect()->route('admin.solicitacao.ver_diligencia', $id)->with('message', "Solicitação Recebida com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.ver_diligencia', $id)->with('erro', "Solicitação não pode ser recebida!");
        }
    }

    public function ver_diligencia($id)
    {
        try {
            $dados = $this->repository->find($id);
            $view = $this->view;
            $rota = $this->rotas;
            $this->orgaoRepository->pushCriteria(new FindWhereActiveCriteria('ativo', true));
            $orgaos = $this->orgaoRepository->findWhereNotIn('id', [$dados->orgao_id])->pluck('nome', 'id');
            $tramitacoes = $dados->movimentacao()->orderby('created_at', 'desc')->get();
            $respostas = $dados->respostas()->orderBy('created_at', 'DESC')->get();
            $diligencia = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Dilig%']])->first()->id;
            $recebido = empty($dados->movimentacao()->where('tipo_movimentacao_id', '=', $diligencia)->whereNull('data_recebimento')->get()->last()->data_recebimento);
            return view('admin.solicitacao.diligencia', compact('dados', 'rota', 'view', 'orgaos', 'tramitacoes', 'respostas', 'recebido'));
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.index')->with('erro', 'Você não tem mais a permissão para visualizar essa solicitação!');
        }

    }

    public function tramitar(Request $request, $id)
    {
        $this->validate($request, [
            'anexo' => 'mimes:pdf',
            'orgao_origem_id' => 'required',
            'despacho' => 'required'
        ]);
        try {
            $solicitacao = $this->repository->find($id);
            $movimentacao = $solicitacao->movimentacao()->get()->last();
            $tramitacao_id = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Tramita%']])->first()->id;

            $inputs = collect($request->all())->reject(function ($value) {
                return empty($value);
            })->toArray();

            $solicitacao->orgao_id = $inputs['orgao_origem_id'];
            $solicitacao->save();

            $dadosNovaTramitacao = [
                'tipo_movimentacao_id' => $tramitacao_id,
                'data_movimentacao' => Carbon::now()->format('d/m/Y H:i:s'),
                'solicitacao_id' => $solicitacao->id,
                'orgao_origem_id' => $movimentacao->orgao_destino_id,
                'orgao_destino_id' => (int)$inputs['orgao_origem_id'],
                'usuario_origem_id' => auth()->id(),
                'despacho' => $inputs['despacho']
            ];

            $tramitacao = $this->movimentacaoRepository->create($dadosNovaTramitacao);
            if ($request->hasFile('anexo')) {
                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'public/movimentacao');
                $anexo = $this->anexoRepository->create($anexoInputs);
                $anexo->movimentacao()->attach($tramitacao->id);
            }
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Em tramitação"));
            }
            return redirect()->route('admin.solicitacao.show', $id)->with('message', "Solicitação Tramitada com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.show', $id)->with('erro', "Solicitação não pode ser tramitada!");
        }
    }

    public function diligencia(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'anexo' => 'mimes:pdf',
                'orgao_origem_id' => 'required',
                'despacho' => 'required'
            ]);

            $solicitacao = $this->repository->find($id);
            $movimentacao = $solicitacao->movimentacao()->get()->last();
            $diligencia = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Dilig%']])->first()->id;

            $inputs = collect($request->all())->reject(function ($value) {
                return empty($value);
            })->toArray();

            $dadosNovaTramitacao = [
                'tipo_movimentacao_id' => $diligencia,
                'data_movimentacao' => Carbon::now()->format('d/m/Y H:i:s'),
                'solicitacao_id' => $solicitacao->id,
                'orgao_origem_id' => $movimentacao->orgao_destino_id,
                'orgao_destino_id' => (int)$inputs['orgao_origem_id'],
                'usuario_origem_id' => auth()->id(),
                'despacho' => $inputs['despacho']
            ];

            $tramitacao = $this->movimentacaoRepository->create($dadosNovaTramitacao);
            if ($request->hasFile('anexo')) {
                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'public/movimentacao');
                $anexo = $this->anexoRepository->create($anexoInputs);
                $anexo->movimentacao()->attach($tramitacao->id);
            }
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Diligência Aberta"));
            }
            return redirect()->route('admin.solicitacao.show', $id)->with('message', "Diligência aberta com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.show', $id)->with('erro', "Diligência não pode ser aberta!");
        }
    }

    public function responder(Request $request, $id)
    {
        $this->validate($request, [
            'anexo' => 'mimes:pdf'
        ]);
        try {
            $inputs = $request->all();
            $solicitacao = $this->repository->find($id);

            $resposta_id = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Respost%']])->first()->id;
            $diligencia_id = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'Dilig%']])->first()->id;
            $orgao = $solicitacao->movimentacao()->whereNotIn('tipo_movimentacao_id', [$resposta_id, $diligencia_id])->get()->last()->orgao_destino_id;
            $inputs['data'] = Carbon::now()->format('d/m/Y');
            $inputs['usuario_id'] = auth()->id();
            $inputs['orgao_id'] = $orgao;
            if ($request->input('negar')) {
                $inputs['homologada'] = true;
            }
            $resposta = $solicitacao->respostas()->create($inputs);

            if ($request->hasFile('anexo')) {
                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'public/resposta');
                $anexo = $this->anexoRepository->create($anexoInputs);
                $anexo->resposta()->attach($resposta->id);
            }

            if ($request->input('negar')) {
                $negacao_id = $this->situacaoRepository->findWhere([['nome', 'like', 'Negad%']])->first()->id;
                $solicitacao->situacao_id = $negacao_id;
                $solicitacao->save();
                if (env('MAIL_SEND', false)) {
                    \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Negado"));
                }
                return redirect()->route('admin.solicitacao.show', $id)->with('message', "Solicitação Respondida e Negada com Sucesso!");
            }
//            else {
//                $situacao_id = $this->situacaoRepository->findWhere([['nome', 'like', 'Respondid%']])->first()->id;
//                $solicitacao->situacao_id = $situacao_id;
//                $solicitacao->save();
//                if (env('MAIL_SEND', false)) {
//                    \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Respondido"));
//                }
//            }
            return redirect()->route('admin.solicitacao.show', $id)->with('message', "Solicitação Respondida com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.show', $id)->with('erro', $e->getMessage());
        }
    }

    public function responder_diligencia(Request $request, $id)
    {
        $this->validate($request, [
            'anexo' => 'mimes:pdf'
        ]);
        try {
            $inputs = $request->all();
            $tipo_movimentacao = $this->tipoMovimentacaoRepository->findWhere([['nome', 'like', 'respos%']])->first()->id;
            $movimentacao = $this->movimentacaoRepository->find($id);
            $inputs['data_movimentacao'] = Carbon::now()->format('d/m/Y H:i:s');
            $inputs['tipo_movimentacao_id'] = $tipo_movimentacao;
            $inputs['orgao_origem_id'] = $movimentacao->orgao_destino_id;
            $inputs['orgao_destino_id'] = $movimentacao->orgao_origem_id;
            $inputs['solicitacao_id'] = $movimentacao->solicitacao_id;
            $inputs['usuario_origem_id'] = auth()->id();

            if ($request->hasFile('anexo')) {
                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'public/movimentacao');
                $anexo = $this->anexoRepository->create($anexoInputs);
                $anexo->movimentacao()->attach($movimentacao->id);
            }
            $this->movimentacaoRepository->create($inputs);
            return redirect()->route('admin.solicitacao.ver_diligencia', $movimentacao->solicitacao_id)->with('message', "Diligência Respondida com Sucesso!");
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.ver_diligencia', $movimentacao->solicitacao_id)->with('erro', $e->getMessage());
        }
    }

    public function prorrogar(Request $request, $id)
    {
        try {
            $solicitacao = $this->repository->find($id);
            $configuracao = $this->configuracaoLai->get()->first();

            $data_previsao_resposta_convertida = Carbon::createFromFormat('d/m/Y H:i', $solicitacao->data_previsao_resposta)->format('Y-m-d H:i:s');
            $data_previsao_resposta = Carbon::createFromFormat('Y-m-d H:i:s', $data_previsao_resposta_convertida);

            $inputProrrogacao = $request->all();
            $inputProrrogacao['data_prorrogacao'] = $data_previsao_resposta->addDays($configuracao->prazo_prorrogacao_resposta);
            $inputProrrogacao['solicitacao_id'] = $solicitacao->id;
            $this->prorrogacaoRepository->create($inputProrrogacao);
            if (env('MAIL_SEND', false)) {
                \Mail::to($solicitacao->solicitante)->send(new SolicitacaoStatus("Prorrogado"));
            }
            return redirect()->route('admin.solicitacao.show', $id)->with('message', 'Solicitação Prorrogada com Sucesso!');
        } catch (\Exception $e) {
            return redirect()->route('admin.solicitacao.show', $id)->with('erro', 'Erro ao tentar Prorrogar Solicitação!' . $e->getMessage());
        }
    }
}
