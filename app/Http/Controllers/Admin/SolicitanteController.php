<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\Controller;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Models\Solicitante;
use Esic\Repositories\EscolaridadeRepository;
use Esic\Repositories\FaixaEtariaRepository;
use Esic\Repositories\ProfissaoRepository;
use Esic\Repositories\SolicitanteRepository;
use Esic\Repositories\TipoTelefoneRepository;
use Illuminate\Http\Request;

class SolicitanteController extends Controller
{
    use ControllerTrait;
    /**
     * @var SolicitanteRepository
     */
    private $repository;
    /**
     * @var EscolaridadeRepository
     */
    private $escolaridadeRepository;
    /**
     * @var TipoTelefoneRepository
     */
    private $tipoTelefoneRepository;
    /**
     * @var FaixaEtariaRepository
     */
    private $faixaEtariaRepository;
    /**
     * @var ProfissaoRepository
     */
    private $profissaoRepository;

    /**
     * SolicitanteController constructor.
     * @param SolicitanteRepository $repository
     * @param EscolaridadeRepository $escolaridadeRepository
     * @param TipoTelefoneRepository $tipoTelefoneRepository
     * @param FaixaEtariaRepository $faixaEtariaRepository
     * @param ProfissaoRepository $profissaoRepository
     */
    public function __construct(
        SolicitanteRepository $repository,
        EscolaridadeRepository $escolaridadeRepository,
        TipoTelefoneRepository $tipoTelefoneRepository,
        FaixaEtariaRepository $faixaEtariaRepository,
        ProfissaoRepository $profissaoRepository
    )
    {
        $this->mensagem = (object)[
            'store' => 'Solicitante Cadastrado com Sucesso!',
            'update' => 'Solicitante Alterado com Sucesso!',
            'destroy' => 'Solicitante Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.solicitante.index',
            'create' => 'admin.solicitante.create',
            'store' => 'admin.solicitante.store',
            'edit' => 'admin.solicitante.edit',
            'update' => 'admin.solicitante.update',
            'destroy' => 'admin.solicitante.destroy',
            'show' => 'admin.solicitante.show',
        ];

        $this->view = (object)[
            'titulo' => 'Solicitante',
            'folder' => 'admin.solicitante.',
            'index' => 'admin.solicitante.index',
            'create' => 'admin.solicitante.create',
            'show' => 'admin.solicitante.show',
            'edit' => 'admin.solicitante.edit',
        ];
        $this->repository = $repository;
        $this->escolaridadeRepository = $escolaridadeRepository;
        $this->tipoTelefoneRepository = $tipoTelefoneRepository;
        $this->faixaEtariaRepository = $faixaEtariaRepository;
        $this->profissaoRepository = $profissaoRepository;
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $tipoPessoa = Solicitante::TIPO_PESSOA;
        $tipoDocumento = Solicitante::TIPO_DOCUMENTO;
        $genero = Solicitante::GENERO;
        $escolaridade = $this->escolaridadeRepository->pluck('nome', 'id');
        $tipoTelefone = $this->tipoTelefoneRepository->pluck('nome', 'id');
        $faixaEtaria = $this->faixaEtariaRepository->pluck('nome', 'id');
        $profissoes = $this->profissaoRepository->pluck('nome', 'id');
        return view($this->view->create, compact('rota', 'view', 'escolaridade', 'tipoTelefone', 'faixaEtaria', 'tipoPessoa', 'profissoes', 'genero', 'tipoDocumento'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $tipoPessoa = Solicitante::TIPO_PESSOA;
        $tipoDocumento = Solicitante::TIPO_DOCUMENTO;
        $genero = Solicitante::GENERO;
        $escolaridade = $this->escolaridadeRepository->pluck('nome', 'id');
        $tipoTelefone = $this->tipoTelefoneRepository->pluck('nome', 'id');
        $faixaEtaria = $this->faixaEtariaRepository->pluck('nome', 'id');
        $profissoes = $this->profissaoRepository->pluck('nome', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'escolaridade', 'tipoTelefone', 'faixaEtaria', 'tipoPessoa', 'profissoes', 'genero', 'tipoDocumento'));
    }

}
