<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\TipoMovimentacaoRepository;
use Esic\Http\Controllers\Controller;

class TipoMovimentacaoController extends Controller
{
    use ControllerTrait;

    private $mensagem;
    private $rotas;
    private $view;
    private $repository;

    /**
     * TipoMovimentacaoController constructor.
     */
    public function __construct(TipoMovimentacaoRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Tipo de Movimentação Cadastrado com Sucesso!',
            'update' => 'Tipo de Movimentação Alterado com Sucesso!',
            'destroy' => 'Tipo de Movimentação Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.tipo_movimentacao.index',
            'create' => 'admin.tipo_movimentacao.create',
            'store' => 'admin.tipo_movimentacao.store',
            'edit' => 'admin.tipo_movimentacao.edit',
            'update' => 'admin.tipo_movimentacao.update',
            'destroy' => 'admin.tipo_movimentacao.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Tipo de Movimentação',
            'folder' => 'admin.tipo_movimentacao.',
            'index' => 'admin.tipo_movimentacao.index',
            'create' => 'admin.tipo_movimentacao.create',
            'show' => 'admin.tipo_movimentacao.show',
            'edit' => 'admin.tipo_movimentacao.edit',
        ];

        $this->repository = $repository;
    }
}
