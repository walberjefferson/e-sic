<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Repositories\TipoRetornoRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

/**
 * Class TipoRetornoController
 * @package Esic\Http\Controllers\Admin
 */
class TipoRetornoController extends Controller
{
    private $mensagem;
    private $rotas;
    private $view;
    private $repository;

    /**
     * TipoRetornoController constructor.
     * @param TipoRetornoRepository $repository
     */
    public function __construct(TipoRetornoRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Tipo de Retorno Cadastrado com Sucesso!',
            'update' => 'Tipo de Retorno Alterado com Sucesso!',
            'destroy' => 'Tipo de Retorno Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.tipo_retorno.index',
            'create' => 'admin.tipo_retorno.create',
            'store' => 'admin.tipo_retorno.store',
            'edit' => 'admin.tipo_retorno.edit',
            'update' => 'admin.tipo_retorno.update',
            'destroy' => 'admin.tipo_retorno.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Tipo de Retorno',
            'folder' => 'admin.tipo_retorno.',
            'index' => 'admin.tipo_retorno.index',
            'create' => 'admin.tipo_retorno.create',
            'show' => 'admin.tipo_retorno.show',
            'edit' => 'admin.tipo_retorno.edit',
        ];

        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->all();
        return view($this->view->index, compact('dados', 'rota', 'view'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        return view($this->view->create, compact('rota', 'view'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->repository->create($request->all());
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->store);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        return view($this->view->edit, compact('dados', 'rota', 'view'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->destroy);
    }
}
