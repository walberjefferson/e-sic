<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\TipoSolicitacaoRepository;
use Esic\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipoSolicitacaoController extends Controller
{
    use ControllerTrait;

    private $view;
    /**
     * TipoSolicitacaoController constructor.
     * @param TipoSolicitacaoRepository $repository
     */
    public function __construct(TipoSolicitacaoRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Tipo de Solicitação Cadastrado com Sucesso!',
            'update' => 'Tipo de Solicitação Alterado com Sucesso!',
            'destroy' => 'Tipo de Solicitação Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.tipo_solicitacao.index',
            'create' => 'admin.tipo_solicitacao.create',
            'store' => 'admin.tipo_solicitacao.store',
            'edit' => 'admin.tipo_solicitacao.edit',
            'update' => 'admin.tipo_solicitacao.update',
            'destroy' => 'admin.tipo_solicitacao.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Tipo de Solicitação',
            'folder' => 'admin.tipo_solicitacao.',
            'index' => 'admin.tipo_solicitacao.index',
            'create' => 'admin.tipo_solicitacao.create',
            'show' => 'admin.tipo_solicitacao.show',
            'edit' => 'admin.tipo_solicitacao.edit',
        ];
        $this->repository = $repository;
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $tipo_solicitacao = $this->repository->pluck('nome', 'id');
        return view($this->view->create, compact('rota', 'view', 'tipo_solicitacao'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $tipo_solicitacao = $this->repository->pluck('nome', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'tipo_solicitacao'));
    }
}
