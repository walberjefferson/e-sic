<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Http\Controllers\Controller;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Http\Controllers\Response;
use Esic\Repositories\TipoTelefoneRepository;

class TipoTelefonesController extends Controller
{
    use ControllerTrait;

    public function __construct(TipoTelefoneRepository $repository)
    {
        $this->mensagem = (object)[
            'store' => 'Tipo de Telefone Cadastrada com Sucesso!',
            'update' => 'Tipo de Telefone Alterada com Sucesso!',
            'destroy' => 'Tipo de Telefone Excluida com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.tipo_telefone.index',
            'create' => 'admin.tipo_telefone.create',
            'store' => 'admin.tipo_telefone.store',
            'edit' => 'admin.tipo_telefone.edit',
            'update' => 'admin.tipo_telefone.update',
            'destroy' => 'admin.tipo_telefone.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Tipo de Telefone',
            'folder' => 'admin.tipo_telefone.',
            'index' => 'admin.tipo_telefone.index',
            'create' => 'admin.tipo_telefone.create',
            'show' => 'admin.tipo_telefone.show',
            'edit' => 'admin.tipo_telefone.edit',
        ];
        $this->repository = $repository;
    }

}
