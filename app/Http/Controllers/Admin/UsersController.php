<?php

namespace Esic\Http\Controllers\Admin;

use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Http\Controllers\Controller;
use Esic\Http\Controllers\ControllerTrait;
use Esic\Repositories\OrgaoRepository;
use Esic\Repositories\PerfilRepository;
use Esic\Repositories\UserRepository;
use Illuminate\Http\Request;


class UsersController extends Controller
{
    use ControllerTrait;
    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var PerfilRepository
     */
    private $perfilRepository;
    /**
     * @var OrgaoRepository
     */
    private $orgaoRepository;

    /**
     * UsersController constructor.
     * @param UserRepository $repository
     * @param PerfilRepository $perfilRepository
     * @param OrgaoRepository $orgaoRepository
     */
    public function __construct(UserRepository $repository, PerfilRepository $perfilRepository, OrgaoRepository $orgaoRepository)
    {
        $this->mensagem = (object)[
            'store' => 'Usuário Cadastrado com Sucesso!',
            'update' => 'Usuário Alterado com Sucesso!',
            'destroy' => 'Usuário Excluido com Sucesso!',
        ];

        $this->rotas = (object)[
            'index' => 'admin.usuarios.index',
            'create' => 'admin.usuarios.create',
            'store' => 'admin.usuarios.store',
            'edit' => 'admin.usuarios.edit',
            'update' => 'admin.usuarios.update',
            'destroy' => 'admin.usuarios.destroy'
        ];

        $this->view = (object)[
            'titulo' => 'Usuário',
            'folder' => 'admin.usuarios.',
            'index' => 'admin.usuarios.index',
            'create' => 'admin.usuarios.create',
            'show' => 'admin.usuarios.show',
            'edit' => 'admin.usuarios.edit',
        ];
        $this->repository = $repository;
        $this->perfilRepository = $perfilRepository;
        $this->orgaoRepository = $orgaoRepository;
    }

    public function create()
    {
        $rota = $this->rotas;
        $view = $this->view;
        $perfil = $this->perfilRepository->pluck('nome', 'id');
        $this->orgaoRepository->pushCriteria(FindWhereActiveCriteria::class);
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        return view($this->view->create, compact('rota', 'view', 'perfil', 'orgaos'));
    }

    public function edit($id)
    {
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        $perfil = $this->perfilRepository->pluck('nome', 'id');
        $this->orgaoRepository->pushCriteria(FindWhereActiveCriteria::class);
        $orgaos = $this->orgaoRepository->pluck('nome', 'id');
        return view($this->view->edit, compact('dados', 'rota', 'view', 'perfil', 'orgaos'));
    }

    public function update(Request $request, $id)
    {
        $orgaos = [];
        if ($request->get('orgaos')) {
            foreach ($request->get('orgaos') as $orgao_id) {
                if ($orgao_id) $orgaos[] = $orgao_id;
            }
        }
        $usuario = $this->repository->update($request->except('orgaos'), $id);
        if (!empty($orgaos)) $usuario->orgaos()->sync($orgaos);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
    }

}
