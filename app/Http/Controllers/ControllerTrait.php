<?php

namespace Esic\Http\Controllers;


use Illuminate\Http\Request;

trait ControllerTrait
{
    private $mensagem;
    private $rotas;
    private $view;
    private $repository;
    protected $permissoes;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->view);
        $view = $this->view;
        $rota = $this->rotas;
        $dados = $this->repository->paginate();
        return view($this->view->index, compact('dados', 'rota', 'view'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->create);
        $rota = $this->rotas;
        $view = $this->view;
        return view($this->view->create, compact('rota', 'view'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->create);
        $this->repository->create($request->all());
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->store);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->view);
        $rota = $this->rotas;
        $dados = $this->repository->find($id);
        $view = $this->view;
        return view($this->view->show, compact('dados', 'rota', 'view'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $rota = $this->rotas;
        $view = $this->view;
        $dados = $this->repository->find($id);
        return view($this->view->edit, compact('dados', 'rota', 'view'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->update);
        $this->repository->update($request->all(), $id);
        return redirect()->route($this->rotas->index)->with('message', $this->mensagem->update);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (isset($this->permissoes))
            $this->authorize($this->permissoes->destroy);
        try {
            $this->repository->delete($id);
            return redirect()->route($this->rotas->index)->with('message', $this->mensagem->destroy);
        } catch (\Exception $e) {
            return redirect()->route($this->rotas->index)->with('erro', "Não é possivel remover!");
        }

    }
}