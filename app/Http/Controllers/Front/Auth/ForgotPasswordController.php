<?php

namespace Esic\Http\Controllers\Front\Auth;

use Esic\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:solicitante');
    }

    public function showLinkRequestForm()
    {
        return view('solicitante.auth.passwords.email');
    }

    public function broker()
    {
        return Password::broker('solicitantes');
    }
}
