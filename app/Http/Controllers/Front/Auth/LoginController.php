<?php

namespace Esic\Http\Controllers\Front\Auth;

use Auth;
use Esic\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:solicitante')->except('logout');
    }

    public function showLoginForm()
    {
        return view('home');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard('solicitante');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect($this->redirectTo)->with('message', 'Usuário Logado com Sucesso!');
    }
}
