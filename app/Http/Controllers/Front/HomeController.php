<?php

namespace Esic\Http\Controllers\Front;


use Esic\Helpers\Helper;
use Illuminate\Http\Request;
use Esic\Models\Solicitacao;
use Esic\Http\Controllers\Controller;
use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Repositories\SicFisicoRepository;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Repositories\SolicitanteRepository;

class HomeController extends Controller
{
    private $solicitanteRepository;
    private $solicitacaoRepository;
    private $model;
    private $sicFisicoRepository;

    public function __construct(
        SolicitanteRepository $solicitanteRepository,
        SolicitacaoRepository $solicitacaoRepository,
        SicFisicoRepository $sicFisicoRepository,
        Solicitacao $model)
    {
        $this->solicitanteRepository = $solicitanteRepository;
        $this->solicitacaoRepository = $solicitacaoRepository;
        $this->sicFisicoRepository = $sicFisicoRepository;
        $this->model = $model;
    }

    public function index()
    {
        if (auth()->guard('solicitante')->check()) {
            $this->solicitacaoRepository->pushCriteria(new FindWhereActiveCriteria('users_solicitantes_id', auth()->guard('solicitante')->id()));
            $solicitacoes = collect([
                'total' => $this->solicitacaoRepository->all()->count(),
                'pendentes' => $this->solicitacaoRepository->findWhere(['situacao_id' => 1])->count(),
                'negadas' => $this->solicitacaoRepository->findWhere(['situacao_id' => 3])->count(),
                'respondidas' => $this->solicitacaoRepository->findWhere(['situacao_id' => 2])->count(),
                'ultimas_solicitacoes' => $this->solicitacaoRepository->orderBy('data_solicitacao', 'desc')->paginate(3)
            ]);
            return view('front.solicitante.index', compact('solicitacoes'));
        } else {
            $dados = $this->sicFisicoRepository->all()->first();
            return view('home', compact('dados'));
        }
    }

    public function estatisticas()
    {
        $exercicio = \request()->get('exercicio');
        $lista_anos = $this->model->listaAnos('data_solicitacao');
        if ($exercicio) {

            return response()->json([
                'status' => $this->status($exercicio),
                'total_lista' => $this->total_lista($exercicio)
            ]);
        }
        return view('front.estatisticas', compact('lista_anos'));
    }

    protected function lista($situacao, $ano = null)
    {
        $ano = ($ano) ? $ano : date('Y');
        $dados = [];
        for ($i = 1; $i <= 12; $i++) :
            $dados[] = $this->model->where(['situacao_id' => $situacao])
                ->whereMonth('data_solicitacao', $i)
                ->whereYear('data_solicitacao', $ano)
                ->count();
        endfor;
        return $dados;
    }

    public function total_lista($ano = null)
    {
        $ano = ($ano) ? $ano : date('Y');
        $dados = [
            'pendentes' => $this->model->where(['situacao_id' => 1])
                ->whereYear('data_solicitacao', $ano)
                ->count(),
            'negadas' => $this->model->where(['situacao_id' => 3])
                ->whereYear('data_solicitacao', $ano)
                ->count(),
            'respondidas' => $this->model->where(['situacao_id' => 2])
                ->whereYear('data_solicitacao', $ano)
                ->count(),
        ];
        return $dados;
    }

    protected function status($ano = null)
    {
        $ano = ($ano) ? $ano : date('Y');
        $dados = [[$ano, 'Pendentes', 'Negadas', 'Respondidas']];
        for ($i = 1; $i <= 12; $i++) :
            $dados[] = [
                Helper::meses($i),
                $this->model->where(['situacao_id' => 1])
                    ->whereMonth('data_solicitacao', $i)
                    ->whereYear('data_solicitacao', $ano)
                    ->count(),
                $this->model->where(['situacao_id' => 3])
                    ->whereMonth('data_solicitacao', $i)
                    ->whereYear('data_solicitacao', $ano)
                    ->count(),
                $this->model->where(['situacao_id' => 2])
                    ->whereMonth('data_solicitacao', $i)
                    ->whereYear('data_solicitacao', $ano)
                    ->count()
            ];
        endfor;
        return $dados;
    }
}
