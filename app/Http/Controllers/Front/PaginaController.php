<?php

namespace Esic\Http\Controllers\Front;

use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Repositories\PaginaRepository;
use Illuminate\Http\Request;
use Esic\Http\Controllers\Controller;

class PaginaController extends Controller
{
    private $repository;

    public function __construct(PaginaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
//        $this->repository->pushCriteria(FindWhereActiveCriteria::class);
//        $dados = $this->repository->all();
//        dd($dados);
        return '';
    }

    public function show($slug)
    {
        $pagina = $this->repository->findByField('slug', $slug)->first();
        return view('front.pagina.show', compact('pagina'));
    }
}
