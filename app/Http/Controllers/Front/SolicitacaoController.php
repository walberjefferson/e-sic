<?php

namespace Esic\Http\Controllers\Front;

use Carbon\Carbon;
use Esic\Models\User;
use Esic\Helpers\Helper;
use Esic\Models\Situacao;
use Esic\Models\Solicitante;
use Esic\Models\Orgao;
use Illuminate\Http\Request;
use Esic\Mail\SolicitacaoNovo;
use Esic\Models\TipoSolicitacao;
use Esic\Models\TipoMovimentacao;
use Esic\Http\Controllers\Controller;
use Esic\Repositories\AnexoRepository;
use Esic\Repositories\OrgaoRepository;
use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Repositories\SolicitanteRepository;
use Esic\Repositories\TipoRetornoRepository;
use Esic\Repositories\MovimentacaoRepository;
use Esic\Repositories\UsersSolicitanteRepository;

class SolicitacaoController extends Controller
{
    private $repository;
    private $tipoRetornoRepository;
    private $orgaoRepository;
    private $movimentacaoRepository;
    private $helper;
    private $anexoRepository;
    private $solicitanteRepository;
    private $usersSolicitanteRepository;

    public function __construct(
        SolicitacaoRepository $repository,
        SolicitanteRepository $solicitanteRepository,
        UsersSolicitanteRepository $usersSolicitanteRepository,
        TipoRetornoRepository $tipoRetornoRepository,
        MovimentacaoRepository $movimentacaoRepository,
        OrgaoRepository $orgaoRepository,
        AnexoRepository $anexoRepository,
        Helper $helper
    )
    {
        $this->repository = $repository;
        $this->tipoRetornoRepository = $tipoRetornoRepository;
        $this->orgaoRepository = $orgaoRepository;
        $this->movimentacaoRepository = $movimentacaoRepository;
        $this->anexoRepository = $anexoRepository;
        $this->helper = $helper;
        $this->solicitanteRepository = $solicitanteRepository;
        $this->usersSolicitanteRepository = $usersSolicitanteRepository;
    }

    public function index()
    {
        $this->repository->pushCriteria(new FindWhereActiveCriteria('users_solicitantes_id', auth()->guard('solicitante')->id()));
        $dados = $this->repository->orderBy('data_solicitacao', 'desc')->paginate(3);
        return view('front.solicitacao.index', compact('dados'));
    }

    public function create()
    {
        $tipoRetorno = $this->tipoRetornoRepository->pluck('nome', 'id');
        $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
        return view('front.solicitacao.create', compact('tipoRetorno', 'orgaos'));
    }

    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $this->validate($request, [
                'orgao_id' => 'required',
                'tipo_retorno_id' => 'required',
                'descricao_solicitacao' => 'required',
                'anexo' => 'mimes:pdf|extension:pdf'
            ]);

            $situacao_id = Situacao::where('nome', 'like', 'aguardando%')->first(['id'])->id;
            $tipo_solicitacao_id = TipoSolicitacao::where('nome', 'like', 'inicial%')->first(['id'])->id;
            $tipo_movimentacao_id = TipoMovimentacao::where('nome', 'like', 'inicial%')->first(['id'])->id;

            $input = $request->all();
            $input['users_solicitantes_id'] = auth()->guard('solicitante')->id();
            $input['situacao_id'] = $situacao_id;
            $input['tipo_solicitacao_id'] = $tipo_solicitacao_id;

            $solicitacao = $this->repository->create($input);

            $dadosMovimentacao = [
                'data_movimentacao' => Carbon::now()->format('d/m/Y H:i:s'),
                'tipo_movimentacao_id' => $tipo_movimentacao_id,
                'despacho' => "Despacho inicial",
                'orgao_destino_id' => $request->input('orgao_id')
            ];

            $solicitacao->movimentacao()->create($dadosMovimentacao);

//            if ($request->hasFile('anexo')) {
//                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'solicitacao');
//                $anexo = $this->anexoRepository->create($anexoInputs);
//                $anexo->solicitacao()->attach($solicitacao->id);
//            }
//            if (env('MAIL_SEND', false)) {
//                \Mail::to(auth()->guard('solicitante')->user())->send(new SolicitacaoNovo($solicitacao));
//            }

            $orgao = Orgao::where('central_sic', true)->first();
            if (!$orgao) throw new \Exception('Não foi possível cadastrar solicitação. Por favor entrar em contato com o município.');

            $usuarios = $orgao->usuarios()->get();
            $to = collect();
            foreach ($usuarios as $usuario) {
                if ($usuario->email)
                    $to->push(['name' => $usuario->name, 'email' => $usuario->email]);
            }
            if (env('MAIL_SEND', false) && $to->count()) {
                \Mail::to($to)->send(new SolicitacaoNovo($solicitacao));
            }


            \DB::commit();
            return redirect()->route('front.solicitacao.index')->with('message', 'Solicitação Cadastrada com Sucesso!');
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->back()->withInput()->with('erro', 'Erro ao tentar cadastrar solicitação!');
        }
    }

    public function show($id)
    {
        $dados = $this->repository->find($id);
        $tipoRetorno = $this->tipoRetornoRepository->pluck('nome', 'id');
        $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
        $respostas = $dados->respostas()->orderBy('created_at', 'DESC')->where('homologada', true)->get();
        $existe = $this->repository->findWhere(['solicitacao_origem_id' => $id])->count();
        $negada = $this->repository->findWhere(['id' => $id, 'situacao_id' => Situacao::where('nome', 'like', 'negad%')->first(['id'])->id])->count();
        return view('front.solicitacao.show', compact('dados', 'respostas', 'tipoRetorno', 'orgaos', 'existe', 'negada'));
    }

    public function recurso(Request $request)
    {
        $solicitacao_origem = $request->input('solicitacao_origem_id');
        $negada = $this->repository->findWhere(['id' => $solicitacao_origem, 'situacao_id' => Situacao::where('nome', 'like', 'negad%')->first(['id'])->id])->count();
        if (!$negada) {
            return redirect()->route('front.solicitacao.index')->with('erro', 'Solicitação não foi negada para poder entrar com recurso!');
        }

        $existe = $this->repository->findWhere(['solicitacao_origem_id' => $solicitacao_origem])->count();
        if ($existe) {
            return redirect()->route('front.solicitacao.index')->with('erro', 'Já foi aberto recurso para esta solicitacao!');
        }

        $this->validate($request, [
            'solicitacao_origem_id' => 'required',
            'orgao_id' => 'required',
            'tipo_retorno_id' => 'required',
            'descricao_solicitacao' => 'required',
            'anexo' => 'mimes:pdf'
        ]);

        $situacao_id = Situacao::where('nome', 'like', 'aguardando%')->first(['id'])->id;
        $tipo_solicitacao_id = TipoSolicitacao::where('nome', 'like', '%primeira%')->first(['id'])->id;
        $tipo_movimentacao_id = TipoMovimentacao::where('nome', 'like', 'inicial%')->first(['id'])->id;

        $input = $request->all();
        $input['users_solicitantes_id'] = auth()->guard('solicitante')->id();
        $input['situacao_id'] = $situacao_id;
        $input['tipo_solicitacao_id'] = $tipo_solicitacao_id;

        $solicitacao = $this->repository->create($input);

        $dadosMovimentacao = [
            'data_movimentacao' => Carbon::now()->format('d/m/Y H:i:s'),
            'tipo_movimentacao_id' => $tipo_movimentacao_id,
            'despacho' => "Despacho inicial",
            'orgao_destino_id' => $request->input('orgao_id')
        ];

        $solicitacao->movimentacao()->create($dadosMovimentacao);


//        if ($request->hasFile('anexo')) {
//            $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'public/solicitacao');
//            $anexo = $this->anexoRepository->create($anexoInputs);
//            $anexo->solicitacao()->attach($solicitacao->id);
//        }
        $orgao = Orgao::where('central_sic', true)->first();
        if (!$orgao) throw new \Exception('Não foi possível cadastrar solicitação. Por favor entrar em contato com o município.');
       // $request->merge(['orgao_id' => $orgao->id]);

        $usuarios = $orgao->usuarios()->get();
        $to = collect();
        foreach ($usuarios as $usuario) {
            if ($usuario->email)
                $to->push(['name' => $usuario->name, 'email' => $usuario->email]);
        }
        if (env('MAIL_SEND', false) && $to->count()) {
            \Mail::to($to)->send(new SolicitacaoNovo($solicitacao));
        }

        //if (env('MAIL_SEND', false)) {
        //    \Mail::to(auth()->guard('solicitante')->user())->send(new SolicitacaoNovo($solicitacao));
        //}

        return redirect()->route('front.solicitacao.index')->with('message', 'Solicitação Cadastrada com Sucesso!');
    }

    public function nova()
    {
        $tipoRetorno = $this->tipoRetornoRepository->pluck('nome', 'id');
        $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
        $tipoDocumento = Solicitante::TIPO_DOCUMENTO;
        return view('front.solicitacao.nova', compact('tipoRetorno', 'orgaos', 'tipoDocumento'));
    }

    public function storerNova(Request $request)
    {
        try {
            \DB::beginTransaction();
            $email = $request->get('email');

            $user = $this->usersSolicitanteRepository->findByField('email', $email)->first();

            if (!$user) {
                $this->validate($request, [
                    'email' => 'required|email|unique:users_solicitantes|max:200',
                    'name' => 'required|max:200',
                ]);

                if ($request->input('tipo_pessoa') == 'f') {

                    $fisica = $request->only([
                        'tipo_pessoa', 'documento', 'tipo_documento', 'orgao_expedidor', 'data_expedicao',
                        'logradouro', 'numero', 'bairro', 'uf', 'cidade', 'cep', 'complemento']);

                    $solicitante = $this->solicitanteRepository->create($fisica);
                }

                if ($request->input('tipo_pessoa') == 'j') {
                    $juridica = $request->only([
                        'tipo_pessoa', 'documento', 'logradouro', 'numero', 'bairro', 'uf', 'cidade', 'cep', 'complemento']);
                    $solicitante = $this->solicitanteRepository->create($juridica);
                }

                $usuario = $request->only('name', 'email');
                $usuario['password'] = User::generatePassword();
                $usuario['solicitante_id'] = $solicitante->id;
                $user = $this->usersSolicitanteRepository->create($usuario);
            } else {
                $usuario = $request->only('name');
                $user = $this->usersSolicitanteRepository->update($usuario, $user->id);
            }

            // Inicio da Criação da Solicitação

            $this->validate($request, [
                'orgao_id' => 'required',
                'tipo_retorno_id' => 'required',
                'descricao_solicitacao' => 'required',
                'anexo' => 'mimes:pdf|extension:pdf'
            ]);

            $situacao = Situacao::where('nome', 'like', 'aguardand%')->first(['id']);
            if(!$situacao) throw new \Exception('Situação não encontrada.');
            $tipo_solicitacao_id = TipoSolicitacao::where('nome', 'like', 'inicial%')->first(['id'])->id;
            $tipo_movimentacao_id = TipoMovimentacao::where('nome', 'like', 'inicial%')->first(['id'])->id;

            $input = $request->only(['orgao_id', 'tipo_retorno_id', 'descricao_solicitacao', 'anexo']);
            $input['users_solicitantes_id'] = $user->id;
            $input['situacao_id'] = $situacao->id;
            $input['tipo_solicitacao_id'] = $tipo_solicitacao_id;

            $solicitacao = $this->repository->create($input);

            $dadosMovimentacao = [
                'data_movimentacao' => Carbon::now()->format('d/m/Y H:i:s'),
                'tipo_movimentacao_id' => $tipo_movimentacao_id,
                'despacho' => "Despacho inicial",
                'orgao_destino_id' => $request->input('orgao_id')
            ];

            $solicitacao->movimentacao()->create($dadosMovimentacao);

//            if ($request->hasFile('anexo')) {
//                $anexoInputs = $this->helper->uploadFile($request, 'anexo', 'solicitacao');
//                $anexo = $this->anexoRepository->create($anexoInputs);
//                $anexo->solicitacao()->attach($solicitacao->id);
//            }
//           if (env('MAIL_SEND', false)) {
//                \Mail::to($request->only('email'))->send(new SolicitacaoNovo($solicitacao));
//            }

            $orgao = Orgao::where('central_sic', true)->first();
            if (!$orgao) throw new \Exception('Não foi possível cadastrar solicitação. Por favor entrar em contato com o município.');
            // $request->merge(['orgao_id' => $orgao->id]);

            $usuarios = $orgao->usuarios()->get();
            $to = collect();
            foreach ($usuarios as $usuario) {
                if ($usuario->email)
                    $to->push(['name' => $usuario->name, 'email' => $usuario->email]);
            }
            if (env('MAIL_SEND', false) && $to->count()) {
                \Mail::to($to)->send(new SolicitacaoNovo($solicitacao));
            }

            \DB::commit();
            return redirect('/')->with('message', 'Solicitação Criada com Sucesso!');
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->back()->withInput()->with('erro', 'Erro ao tentar cadastrar solicitação! - ' . $e->getMessage());
        }
    }

    public function consulta()
    {
        return view('front.solicitacao.consulta');
    }

    public function consultaProtocolo(Request $request)
    {
        try {
            $protocolo = $request->get('numero_protocolo');
            $nProtocoloClean = (int) str_replace('-', '', $protocolo);
            $dados = $this->repository->with(['respostas'])->findByField('numero_protocolo', $nProtocoloClean)->first();
            if(!$dados) throw new \Exception();
            $tipoRetorno = $this->tipoRetornoRepository->pluck('nome', 'id');
            $orgaos = $this->orgaoRepository->findWhere(['ativo' => true])->pluck('nome', 'id');
            $respostas = $dados->respostas()->orderBy('created_at', 'DESC')->where('homologada', true)->get();
            $existe = $this->repository->findWhere(['solicitacao_origem_id' => $dados->id])->count();
            $negada = $this->repository->findWhere(['id' => $dados->id, 'situacao_id' => Situacao::where('nome', 'like', 'negad%')->first(['id'])->id])->count();
            return view('front.solicitacao.show', compact('dados', 'respostas', 'tipoRetorno', 'orgaos', 'existe', 'negada'));
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->with('erro', 'Processo não encontrado.');
        }
    }
}
