<?php

namespace Esic\Http\Controllers\Front;

use Illuminate\Http\Request;
use Esic\Models\Solicitante;
use Esic\Mail\SolicitanteCriado;
use Esic\Http\Controllers\Controller;
use Esic\Repositories\ProfissaoRepository;
use Esic\Repositories\SolicitanteRepository;
use Esic\Repositories\TipoTelefoneRepository;
use Esic\Repositories\EscolaridadeRepository;
use Esic\Repositories\UsersSolicitanteRepository;

class SolicitanteController extends Controller
{
    private $tipoTelefoneRepository;
    private $escolaridadeRepository;
    private $profissaoRepository;
    private $solicitanteRepository;
    private $usersSolicitanteRepository;

    public function __construct(
        TipoTelefoneRepository $tipoTelefoneRepository,
        EscolaridadeRepository $escolaridadeRepository,
        ProfissaoRepository $profissaoRepository,
        UsersSolicitanteRepository $usersSolicitanteRepository,
        SolicitanteRepository $solicitanteRepository
    )
    {
        $this->tipoTelefoneRepository = $tipoTelefoneRepository;
        $this->escolaridadeRepository = $escolaridadeRepository;
        $this->profissaoRepository = $profissaoRepository;
        $this->solicitanteRepository = $solicitanteRepository;
        $this->usersSolicitanteRepository = $usersSolicitanteRepository;
    }

    public function index()
    {
        if (auth()->guard('solicitante')->check()) {
            return redirect('/');
        } else {
            $tipoDocumento = Solicitante::TIPO_DOCUMENTO;
            $genero = Solicitante::GENERO;
            $tipoTelefone = $this->tipoTelefoneRepository->pluck('nome', 'id');
            $escolaridade = $this->escolaridadeRepository->pluck('nome', 'id');
            $profissao = $this->profissaoRepository->pluck('nome', 'id');
            return view('front.solicitante.create', compact('tipoDocumento', 'tipoTelefone', 'escolaridade', 'profissao', 'genero'));
        }
    }

    public function store(Request $request)
    {
        try {
            \DB::beginTransaction();
            $this->validate($request, [
                'email' => 'required|email|unique:users_solicitantes|max:200',
                'name' => 'required|max:200',
                'password' => 'required|confirmed|min:6',
            ]);

            if ($request->input('tipo_pessoa') == 'f') {
                $fisica = $request->only([
                    'tipo_pessoa', 'documento', 'tipo_documento', 'orgao_expedidor', 'data_expedicao',
                    'data_nascimento', 'telefone', 'genero', 'tipo_telefone_id', 'profissao_id', 'logradouro',
                    'numero', 'bairro', 'uf', 'cidade', 'cep', 'complemento', 'escolaridade_id', 'faixaetaria_id']);

                $this->validate($request, [
                    'tipo_pessoa' => 'required',
//                'data_nascimento' => 'date_format:d/m/Y',
                ]);

                $solicitante = $this->solicitanteRepository->create($fisica);
            }

            if ($request->input('tipo_pessoa') == 'j') {
                $juridica = $request->only([
                    'tipo_pessoa', 'documento', 'data_nascimento', 'telefone', 'tipo_telefone_id', 'profissao_id',
                    'logradouro', 'numero', 'bairro', 'uf', 'cidade', 'cep', 'complemento', 'escolaridade_id', 'faixaetaria_id']);

                $this->validate($request, [
                    'tipo_pessoa' => 'required',
//                'data_nascimento' => 'date_format:d/m/Y',
                ]);

                $solicitante = $this->solicitanteRepository->create($juridica);
            }

            $usuario = $request->only(['name', 'email', 'password']);
            $usuario['solicitante_id'] = $solicitante->id;
            $this->usersSolicitanteRepository->create($usuario);

            if (env('MAIL_SEND', false)) {
                \Mail::to($request->only('email'))->send(new SolicitanteCriado($request->only('email', 'password', 'name')));
            }
            \DB::commit();
            return redirect('/')->with('message', 'Solicitante cadastrado com sucesso!');
        } catch (\Exception $e) {
            \DB::rollBack();
            return redirect()->back()->withInput()->with('erro', 'Erro ao tentar cadastrar usuário.');
        }
    }

    public function create()
    {
        $tipoDocumento = Solicitante::TIPO_DOCUMENTO;
        $genero = Solicitante::GENERO;
        $tipoTelefone = $this->tipoTelefoneRepository->pluck('nome', 'id');
        $escolaridade = $this->escolaridadeRepository->pluck('nome', 'id');
        $profissao = $this->profissaoRepository->pluck('nome', 'id');

        $usuario = auth()->guard('solicitante')->user();
        $solicitante = $this->solicitanteRepository->find($usuario->solicitante_id);
        $solicitante->email = $usuario->email;
        $solicitante->name = $usuario->name;

        return view('front.usuario.index', compact('solicitante', 'tipoDocumento', 'tipoTelefone', 'genero', 'escolaridade', 'profissao'));
    }

    public function update(Request $request, $id)
    {
        try{
            \DB::beginTransaction();
            $this->validate($request, [
                'name' => 'required|max:200',
            ]);
            $inputSolicitante = $request->except(['name', 'email']);
            $this->solicitanteRepository->update($inputSolicitante, $id);

            $inputUsuario = $request->only(['name']);
            $this->usersSolicitanteRepository->update($inputUsuario, auth()->guard('solicitante')->id());
            \DB::commit();
            return redirect()->route('front.usuario.ver')->with('message', 'Solicitante atualizado com sucesso!');
        }catch (\Exception $e){
            \DB::rollBack();
            return redirect()->back()->withInput()->with('erro', 'Erro ao tentar atualizar solicitante!');
        }

    }

    public function alteraSenha()
    {
        return view('front.usuario.update-senha');
    }

    public function updateSenha(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
        $usuario = auth()->guard('solicitante')->id();
        $inputs = $request->only('password');
        $inputs['password'] = bcrypt($inputs['password']);
        $this->usersSolicitanteRepository->update($inputs, $usuario);
        return redirect()->route('front.usuario.ver')->with('message', 'Senha alterada com sucesso!');

    }
}
