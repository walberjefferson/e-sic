<?php

namespace Esic\Mail;

use Esic\Models\Solicitacao;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitacaoNovo extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Solicitacao
     */
    private $solicitacao;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Solicitacao $solicitacao)
    {
        $this->solicitacao = $solicitacao;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Nova solicitação criada')
            ->markdown('emails.solicitacao.novo')
            ->with([
                'solicitacao' => $this->solicitacao
            ]);
    }
}
