<?php

namespace Esic\Mail;

use Esic\Models\Resposta;
use Esic\Models\Solicitacao;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SolicitacaoRespostaAnexo extends Mailable
{
    use Queueable, SerializesModels;

    private $status;
    public $resposta;
    public $solicitacao;

    public function __construct($status, Solicitacao $solicitacao, Resposta $resposta)
    {
        $this->status = $status;
        $this->resposta = $resposta;
        $this->solicitacao = $solicitacao;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->subject('Resposta de Solicitação!')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->markdown('emails.solicitante.resposta_anexo');


        if ($this->resposta->anexos_count) {
            $anexo = $this->resposta->anexos()->first();
            $path = storage_path("app/public/public/resposta/{$anexo->url}");
            if (is_file($path)) {
                $mail = $mail->attach($path, [
                    'as' => $anexo->nome
                ]);
            }
        }

        $mail = $mail->with([
            'status' => "A solicitação de nº {$this->solicitacao['numero_protocolo']} foi respondida."
        ]);

        return $mail;
    }
}
