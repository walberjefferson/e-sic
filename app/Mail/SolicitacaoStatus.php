<?php

namespace Esic\Mail;

use Esic\Models\Solicitacao;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitacaoStatus extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    private $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($status)
    {
        //
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Sua solicitação mudou de status!')
            ->markdown('emails.solicitacao.status')
            ->with([
                'status' => $this->status
            ]);
    }
}
