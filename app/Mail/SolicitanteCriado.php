<?php

namespace Esic\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitanteCriado extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    private $dados;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados)
    {
        //
        $this->dados = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Solicitante cadastrado com sucesso!')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->markdown('emails.solicitante.novo')
            ->with([
                'email' => $this->dados['email'],
                'senha' => $this->dados['password'],
                'nome' => $this->dados['name']
            ]);
    }
}
