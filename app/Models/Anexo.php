<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Anexo extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'anexo';

    protected $fillable = ['nome', 'url', 'extensao', 'tamanho', 'numero_downloads'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movimentacao()
    {
        return $this->belongsToMany(Movimentacao::class);
    }

    public function solicitacao()
    {
        return $this->belongsToMany(Solicitacao::class);
    }

    public function resposta()
    {
        return $this->belongsToMany(Resposta::class, 'anexo_resposta');
    }
}
