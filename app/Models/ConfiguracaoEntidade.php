<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ConfiguracaoEntidade extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'configuracao_entidade';

    protected $fillable = ['entidade','cnpj','logradouro','numero','bairro','cidade','uf','cep','logo'];

}
