<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ConfiguracaoLai extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'configuracao_lai';

    protected $fillable = ['prazo_resposta', 'prazo_prorrogacao_resposta', 'prazo_recurso', 'prazo_recurso_resposta'];

}
