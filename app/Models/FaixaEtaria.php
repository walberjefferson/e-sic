<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class FaixaEtaria extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'faixa_etaria';

    protected $fillable = ['nome'];

    public function solicitante()
    {
        return $this->hasMany(Solicitante::class);
    }
}
