<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Movimentacao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'movimentacao';

    protected $fillable = ['data_movimentacao', 'despacho', 'data_recebimento', 'tipo_movimentacao_id',
        'solicitacao_id', 'orgao_origem_id', 'orgao_destino_id', 'usuario_origem_id', 'usuario_recebimento_id'];

    public function getDataMovimentacaoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataMovimentacaoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_movimentacao'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->toDateTimeString();
        } else {
            $this->attributes['data_movimentacao'] = null;
        }
    }

    public function getDataRecebimentoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function setDataRecebimentoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_recebimento'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->toDateTimeString();
        } else {
            $this->attributes['data_recebimento'] = null;
        }
    }

    public function tipo_movimentacao()
    {
        return $this->belongsTo(TipoMovimentacao::class);
    }

    public function solicitacao()
    {
        return $this->belongsTo(Solicitacao::class, 'solicitacao_id');
    }

    public function orgao_origem()
    {
        return $this->belongsTo(Orgao::class, 'orgao_origem_id');
    }

    public function orgao_destino()
    {
        return $this->belongsTo(Orgao::class, 'orgao_destino_id');
    }

    public function usuario_origem()
    {
        return $this->belongsTo(User::class, 'usuario_origem_id');
    }

    public function usuario_recebimento()
    {
        return $this->belongsTo(User::class, 'usuario_recebimento_id');
    }

    public function anexo()
    {
        return $this->belongsToMany(Anexo::class);
    }
}
