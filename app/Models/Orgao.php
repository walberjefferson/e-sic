<?php

namespace Esic\Models;

use Esic\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Orgao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'orgao';

    protected $fillable = ['nome', 'sigla', 'telefone', 'email', 'responsavel', 'central_sic', 'ativo', 'orgao_superior_id'];

    public function getTelefoneAttribute($value)
    {
        if (strlen($value) == 10) {
            $mask = '(##) ####-####';
        } else {
            $mask = '(##) #####-####';
        }
        return Helper::mask($value, $mask);
    }

    public function setTelefoneAttribute($value)
    {
        return $this->attributes['telefone'] = Helper::limpaString($value);
    }

    public function orgao_superior()
    {
        return $this->belongsTo(Orgao::class);
    }

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'orgao_usuario', 'orgao_id', 'usuario_id');
    }
}
