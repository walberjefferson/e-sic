<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Perfil extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'perfil';

    protected $fillable = ['nome'];

    public function usuario()
    {
        return $this->hasMany(User::class);
    }

}
