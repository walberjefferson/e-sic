<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Profissao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'profissao';

    protected $fillable = ['nome'];

}
