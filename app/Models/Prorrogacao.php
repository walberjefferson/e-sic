<?php

namespace Esic\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Prorrogacao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'prorrogacao';

    protected $fillable = ['data_prorrogacao', 'motivo', 'solicitacao_id'];

    public function solicitacoes()
    {
        return $this->belongsTo(Solicitacao::class);
    }

    public function getDataProrrogacaoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y H:i');
    }

}
