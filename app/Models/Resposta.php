<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Resposta extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['data', 'descricao', 'homologada', 'data_homologacao',
        'usuario_homologado_id', 'usuario_id', 'orgao_id', 'solicitacao_id'
    ];

    public function getDataAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        } else {
            $this->attributes['data'] = null;
        }
    }

    public function getDataHomologacaoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataHomologacaoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_homologacao'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        } else {
            $this->attributes['data_homologacao'] = null;
        }
    }

    public function orgao()
    {
        return $this->belongsTo(Orgao::class);
    }

    public function solicitacao()
    {
        return $this->belongsTo(Solicitacao::class);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function homologado()
    {
        return $this->belongsTo(User::class);
    }

    public function anexos()
    {
        return $this->belongsToMany(Anexo::class, 'anexo_resposta');
    }

}
