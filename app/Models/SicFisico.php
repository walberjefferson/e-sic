<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class SicFisico extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'sic_fisico';

    protected $fillable = ['endereco', 'numero', 'bairro', 'cep', 'cidade', 'estado', 'decreto',
        'formulario_requerimento', 'formulario_recurso', 'informacao', 'horario_funcionamento', 'orgao_id'];

    public function orgao()
    {
        return $this->belongsTo(Orgao::class);
    }

}
