<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Solicitacao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'solicitacao';

    protected $appends = ['qtd_resposta', 'em_atraso', 'status'];

    protected $fillable = ['numero_protocolo', 'ano_protocolo', 'data_solicitacao',
        'data_previsao_resposta', 'descricao_solicitacao', 'solicitacao_origem_id', 'users_solicitantes_id', 'situacao_id',
        'tipo_retorno_id', 'tipo_solicitacao_id', 'orgao_id'];

    protected $dates = ['data_previsao_resposta', 'data_solicitacao'];

    public function getStatusAttribute()
    {
        /**
         * Respondido - respondido
         * Aguardando resposta - aguardando-resposta
         * Negado - negado
         *
         * Inicial - inicial
         * Tramitação - tramitacao
         * Resposta - resposta
         */
        $situacao = str_slug($this->situacao->nome);
        $tipo_movimentacao = $this->movimentacao()->get();
        $movimentacao = NULL;
        $movimentacao_slug = NULL;

        if ($tipo_movimentacao->count()) {
            $movimentacao = $tipo_movimentacao->last()->tipo_movimentacao()->first()->nome;
            $movimentacao_slug = str_slug($movimentacao);
        }

        if ($situacao == 'respondido' and $movimentacao_slug != 'inicial') {
            return $movimentacao;
        } elseif ($situacao == 'respondido' and $movimentacao_slug == 'tramitacao') {
            return $movimentacao;
        } elseif ($situacao == 'respondido' and $movimentacao_slug == 'resposta') {
            return $movimentacao;
        } elseif ($situacao == 'aguardando-resposta' and $movimentacao_slug == 'inicial') {
            return $movimentacao;
        } elseif ($situacao == 'aguardando-resposta' and $movimentacao_slug == 'tramitacao') {
            return $movimentacao;
        } elseif ($situacao == 'aguardando-resposta' and $movimentacao_slug == 'resposta') {
            return $movimentacao;
        } elseif ($situacao == 'aguardando-resposta') {
            return $this->situacao->nome;
        } elseif ($situacao == 'negado') {
            return $this->situacao->nome;
        } else {
            return $this->situacao->nome;
        }

    }

    public function getQtdRespostaAttribute()
    {
        return $this->respostas()->where('homologada', true)->count();
    }

    public function getEmAtrasoAttribute()
    {
        if ($this->prorrogacao()->get()->count()) {
            $data_prorrogacao_convertida = Carbon::createFromFormat('d/m/Y H:i', $this->prorrogacao()->first()->data_prorrogacao)->format('Y-m-d H:i:s');
            $data_prorrogacao = Carbon::createFromFormat('Y-m-d H:i:s', $data_prorrogacao_convertida);
            return (Carbon::now()->diffInHours($data_prorrogacao, false) > 0) ? false : true;
        } else {
            $data_previsao_resposta_convertida = Carbon::createFromFormat('d/m/Y H:i', $this->data_previsao_resposta)->format('Y-m-d H:i:s');
            $data_previsao_resposta = Carbon::createFromFormat('Y-m-d H:i:s', $data_previsao_resposta_convertida);
            return (Carbon::now()->diffInHours($data_previsao_resposta, false) > 0) ? false : true;
        }


    }

    public function getNumeroProtocoloAttribute($value)
    {
        if (strlen($value) == 10) {
            $value = substr_replace($value, '-', 5, 0);
            return $value;
        } else {
            return $value;
        }
    }

    public function getDataSolicitacaoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function setDataSolicitacaoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_solicitacao'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->toDateTimeString();
        } else {
            $this->attributes['data_solicitacao'] = null;
        }
    }

    public function getDataPrevisaoRespostaAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y H:i');
    }

    public function setDataPrevisaoRespostaAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_previsao_resposta'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->toDateTimeString();
        } else {
            $this->attributes['data_previsao_resposta'] = null;
        }
    }

    public function solicitacao_origem()
    {
        return $this->belongsTo(Solicitacao::class, 'solicitacao_origem_id');
    }

    public function solicitante()
    {
        return $this->belongsTo(UsersSolicitante::class, 'users_solicitantes_id');
    }

    public function situacao()
    {
        return $this->belongsTo(Situacao::class);
    }

    public function tipo_retorno()
    {
        return $this->belongsTo(TipoRetorno::class, 'tipo_retorno_id');
    }

    public function tipo_solicitacao()
    {
        return $this->belongsTo(TipoSolicitacao::class, 'tipo_solicitacao_id');
    }

    public function respostas()
    {
        return $this->hasMany(Resposta::class);
    }

    public function movimentacao()
    {
        return $this->hasMany(Movimentacao::class);
    }

    public function anexos()
    {
        return $this->belongsToMany(Anexo::class);
    }

    public function prorrogacao()
    {
        return $this->hasOne(Prorrogacao::class);
    }

    public function diligencia()
    {
        return empty($this->movimentacao()
            ->where('tipo_movimentacao_id', 4)
            ->whereIn('orgao_destino_id', auth()->user()->listOrgaos())
            ->get()
            ->last());
    }

    public function orgao()
    {
        return $this->belongsTo(Orgao::class, 'orgao_id');
    }

    public function listaAnos($coluna)
    {
        $years = $this->selectRaw("substr({$coluna},1,4) as year")
            ->distinct('year')
            ->pluck('year', 'year')
            ->toArray();
        if (empty($years)) {
            $years = [date('Y') => date('Y')];
        }
        if (!in_array(date('Y'), $years)) $years[date('Y')] = date('Y');
        return $years;
    }
}
