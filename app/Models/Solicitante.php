<?php

namespace Esic\Models;

use Carbon\Carbon;
use Esic\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Solicitante extends Model implements Transformable
{
    const TIPO_PESSOA = [
        'f' => 'Física',
        'j' => 'Jurídica'
    ];

    const TIPO_DOCUMENTO = [
        1 => 'CPF',
        2 => 'RG',
        3 => 'Habilitação Nacional',
        4 => 'Passaporte',
        5 => 'Carteira Trabalho'
    ];

    const GENERO = [
        1 => 'Masculino',
        2 => 'Feminino'
    ];

    use TransformableTrait;

    protected $table = 'solicitantes';

    protected $hidden = ['password'];

    protected $fillable = ['tipo_pessoa', 'documento', 'tipo_documento', 'orgao_expedidor', 'data_expedicao',
        'data_nascimento', 'genero', 'telefone', 'logradouro', 'numero', 'complemento', 'bairro', 'cep', 'cidade',
        'uf', 'tipo_telefone_id', 'escolaridade_id', 'faixaetaria_id', 'profissao_id'];


    /* Mutators */

    public function getDocumentoAttribute($value)
    {
        if (strlen($value) == 11) {
            return Helper::mask($value, "###.###.###-##");
        } else {
            return Helper::mask($value, "##.###.###/####-##");
        }
    }

    public function getTelefoneAttribute($value)
    {
        if (strlen($value) == 10) {
            return Helper::mask($value, "(##) ####-####");
        } else {
            return Helper::mask($value, "(##) #####-####");
        }
    }

    public function setTelefoneAttribute($value)
    {
        return $this->attributes['telefone'] = Helper::limpaString($value);
    }

    public function getDataExpedicaoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataExpedicaoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_expedicao'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        } else {
            $this->attributes['data_expedicao'] = null;
        }
    }

    public function getDataNascimentoAttribute($value)
    {
        if (!empty($value))
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataNascimentoAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        } else {
            $this->attributes['data_nascimento'] = null;
        }
    }

    public function setDocumentoAttribute($value)
    {
        return $this->attributes['documento'] = Helper::limpaString($value);
    }

    /* Relacionamentos */

    public function tipoPessoa()
    {
        return self::TIPO_PESSOA[$this->tipo_pessoa];
    }

    public function tipoTelefone()
    {
        return $this->belongsTo(TipoTelefone::class, 'tipo_telefone_id');
    }

    public function escolaridade()
    {
        return $this->belongsTo(Escolaridade::class, 'escolaridade_id');
    }

    public function faixaEtaria()
    {
        return $this->belongsTo(FaixaEtaria::class, 'faixaetaria_id');
    }

    public function profissao()
    {
        return $this->belongsTo(Profissao::class, 'profissao_id');
    }

    public function solicitante()
    {
        return $this->hasOne(UsersSolicitante::class);
    }

}
