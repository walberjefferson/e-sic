<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TipoMovimentacao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'tipo_movimentacao';

    protected $fillable = ['nome','abreviacoes','despacho_padrao'];

}
