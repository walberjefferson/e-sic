<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TipoRetorno extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'tipo_retorno';

    protected $fillable = ['nome'];

}
