<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TipoSolicitacao extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'tipo_solicitacao';

    protected $fillable = ['nome', 'ativo', 'instancia', 'tipo_seguinte_id'];

    public function tipo_seguinte()
    {
        return $this->belongsTo(TipoSolicitacao::class);
    }

}
