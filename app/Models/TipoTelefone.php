<?php

namespace Esic\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TipoTelefone extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'tipo_telefone';

    protected $fillable = ['nome'];

    public function solicitante()
    {
        return $this->hasMany(Solicitante::class);
    }

}
