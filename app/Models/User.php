<?php

namespace Esic\Models;

use Esic\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements Transformable
{
    use TransformableTrait, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'perfil_id', 'ativo'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function generatePassword($password = null)
    {
        return !$password ? bcrypt(str_random(8)) : bcrypt($password);
    }

    public function orgaos()
    {
        return $this->belongsToMany(Orgao::class, 'orgao_usuario', 'usuario_id');
    }

    public function perfil()
    {
        return $this->belongsTo(Perfil::class);
    }

    public function isAdmin()
    {
        return ($this->perfil()->where('nome', 'like', 'Admin%')->count()) ? true : false;
    }

    public function isCentral()
    {
        return $this->orgaos()->get()->contains(function ($item) {
            return $item->central_sic;
        });
    }

    public function listOrgaos()
    {
        return $this->orgaos()->get()->map(function ($item) {
            return $item->id;
        })->toArray();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

}
