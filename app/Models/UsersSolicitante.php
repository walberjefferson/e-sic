<?php

namespace Esic\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UsersSolicitante extends Authenticatable implements Transformable
{
    use TransformableTrait, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'ativo', 'solicitante_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function solicitante()
    {
        return $this->belongsTo(Solicitante::class, 'solicitante_id');
    }

}
