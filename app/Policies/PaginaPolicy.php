<?php

namespace Esic\Policies;

use Esic\Models\User;
use Esic\Models\Pagina;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaginaPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the pagina.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create paginas.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the pagina.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the pagina.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();
    }
}
