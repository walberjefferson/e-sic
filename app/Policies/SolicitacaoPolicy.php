<?php

namespace Esic\Policies;

use Esic\Models\Solicitacao;
use Esic\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SolicitacaoPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Solicitacao $solicitacao)
    {
        return $user->isAdmin() ?: collect($user->listOrgaos())->contains($solicitacao->orgao_id);
    }

    /**
     * Determine whether the user can create paginas.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function create(User $user, Solicitacao $solicitacao)
    {
        return $user->isAdmin() ?: collect($user->listOrgaos())->contains($solicitacao->orgao_id);
    }

    /**
     * Determine whether the user can update the pagina.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function update(User $user, Solicitacao $solicitacao)
    {
        return $user->isAdmin() ?: collect($user->listOrgaos())->contains($solicitacao->orgao_id);
    }

    /**
     * Determine whether the user can delete the pagina.
     *
     * @param  \Esic\Models\User $user
     * @return mixed
     */
    public function delete(User $user, Solicitacao $solicitacao)
    {
        return $user->isAdmin() ?: collect($user->listOrgaos())->contains($solicitacao->orgao_id);
    }
}
