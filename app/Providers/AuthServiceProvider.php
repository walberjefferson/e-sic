<?php

namespace Esic\Providers;

use Esic\Models\Solicitacao;
use Esic\Models\User;
use Esic\Policies\PaginaPolicy;
use Esic\Policies\SolicitacaoPolicy;
use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //Pagina::class => PaginaPolicy::class,
//        Solicitacao::class => SolicitacaoPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('user-admin', function (User $user) {
            return $user->isAdmin();
        });

        Gate::define('user-central', function (User $user) {
            return ($user->isCentral() || $user->isAdmin());
        });

        Gate::resource('pagina', PaginaPolicy::class);

        Gate::resource('solicitacao', 'Esic\Policies\SolicitacaoPolicy');
        //
    }
}
