<?php

namespace Esic\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app', 'Esic\ViewCompose\MenuCompose');
        View::composer('layouts.lagoa', 'Esic\ViewCompose\MenuCompose');
        View::composer('layouts.jacare', 'Esic\ViewCompose\MenuCompose');
        View::composer('layouts.poco', 'Esic\ViewCompose\MenuCompose');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
