<?php

namespace Esic\Providers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\ServiceProvider;

class ExtensionValidServiceProvider extends ServiceProvider
{
    protected $message;

    public function boot()
    {
        \Validator::extend('extension', function ($attribute, $value, $parameters, $validator) {
            /** @var UploadedFile $value */
            if ($value->isValid() && $value->isFile()) {
                $extension = $value->getClientOriginalExtension();
                if (in_array($extension, $parameters)) {
                    return true;
                }
                $this->message = 'Extenssão inválida';
                return false;
            }
            $this->message = 'Arquivo inválido';
            return false;
        });

        \Validator::replacer('extension', function ($message, $attribute, $rule, $parameters) {
            return $this->message ?: $message;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
