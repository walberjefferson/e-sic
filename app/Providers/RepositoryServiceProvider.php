<?php

namespace Esic\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Esic\Repositories\UserRepository::class, \Esic\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\TipoTelefoneRepository::class, \Esic\Repositories\TipoTelefoneRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\EscolaridadeRepository::class, \Esic\Repositories\EscolaridadeRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\FaixaEtariaRepository::class, \Esic\Repositories\FaixaEtariaRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\SolicitanteRepository::class, \Esic\Repositories\SolicitanteRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\SituacaoRepository::class, \Esic\Repositories\SituacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\TipoRetornoRepository::class, \Esic\Repositories\TipoRetornoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\PaginaRepository::class, \Esic\Repositories\PaginaRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\ConfiguracaoEntidadeRepository::class, \Esic\Repositories\ConfiguracaoEntidadeRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\ConfiguracaoLaiRepository::class, \Esic\Repositories\ConfiguracaoLaiRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\PerfilRepository::class, \Esic\Repositories\PerfilRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\AnexoRepository::class, \Esic\Repositories\AnexoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\OrgaoRepository::class, \Esic\Repositories\OrgaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\TipoMovimentacaoRepository::class, \Esic\Repositories\TipoMovimentacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\TipoSolicitacaoRepository::class, \Esic\Repositories\TipoSolicitacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\SolicitacaoRepository::class, \Esic\Repositories\SolicitacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\ProrrogacaoRepository::class, \Esic\Repositories\ProrrogacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\RespostaRepository::class, \Esic\Repositories\RespostaRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\MovimentacaoRepository::class, \Esic\Repositories\MovimentacaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\ProfissaoRepository::class, \Esic\Repositories\ProfissaoRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\UsersSolicitanteRepository::class, \Esic\Repositories\UsersSolicitanteRepositoryEloquent::class);
        $this->app->bind(\Esic\Repositories\SicFisicoRepository::class, \Esic\Repositories\SicFisicoRepositoryEloquent::class);
        //:end-bindings:
    }
}
