<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnexoRepository
 * @package namespace Esic\Repositories;
 */
interface AnexoRepository extends RepositoryInterface
{
    //
}
