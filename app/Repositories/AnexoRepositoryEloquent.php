<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\AnexoRepository;
use Esic\Models\Anexo;
use Esic\Validators\AnexoValidator;

/**
 * Class AnexoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class AnexoRepositoryEloquent extends BaseRepository implements AnexoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Anexo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
