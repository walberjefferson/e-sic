<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfiguracaoEntidadeRepository
 * @package namespace Esic\Repositories;
 */
interface ConfiguracaoEntidadeRepository extends RepositoryInterface
{
    //
}
