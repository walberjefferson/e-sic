<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\ConfiguracaoEntidadeRepository;
use Esic\Models\ConfiguracaoEntidade;
use Esic\Validators\ConfiguracaoEntidadeValidator;

/**
 * Class ConfiguracaoEntidadeRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class ConfiguracaoEntidadeRepositoryEloquent extends BaseRepository implements ConfiguracaoEntidadeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ConfiguracaoEntidade::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
