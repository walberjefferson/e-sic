<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfiguracaoLaiRepository
 * @package namespace Esic\Repositories;
 */
interface ConfiguracaoLaiRepository extends RepositoryInterface
{
    //
}
