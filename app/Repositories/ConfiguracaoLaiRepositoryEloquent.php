<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\ConfiguracaoLaiRepository;
use Esic\Models\ConfiguracaoLai;
use Esic\Validators\ConfiguracaoLaiValidator;

/**
 * Class ConfiguracaoLaiRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class ConfiguracaoLaiRepositoryEloquent extends BaseRepository implements ConfiguracaoLaiRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ConfiguracaoLai::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
