<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EscolaridadeRepository
 * @package namespace Esic\Repositories;
 */
interface EscolaridadeRepository extends RepositoryInterface
{
    //
}
