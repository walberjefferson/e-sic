<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\EscolaridadeRepository;
use Esic\Models\Escolaridade;
use Esic\Validators\EscolaridadeValidator;

/**
 * Class EscolaridadeRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class EscolaridadeRepositoryEloquent extends BaseRepository implements EscolaridadeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Escolaridade::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
