<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FaixaEtariaRepository
 * @package namespace Esic\Repositories;
 */
interface FaixaEtariaRepository extends RepositoryInterface
{
    //
}
