<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\FaixaEtariaRepository;
use Esic\Models\FaixaEtaria;
use Esic\Validators\FaixaEtariaValidator;

/**
 * Class FaixaEtariaRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class FaixaEtariaRepositoryEloquent extends BaseRepository implements FaixaEtariaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FaixaEtaria::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
