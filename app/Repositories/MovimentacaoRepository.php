<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MovimentacaoRepository
 * @package namespace Esic\Repositories;
 */
interface MovimentacaoRepository extends RepositoryInterface
{
    //
}
