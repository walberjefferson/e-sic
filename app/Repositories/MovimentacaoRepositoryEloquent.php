<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\MovimentacaoRepository;
use Esic\Models\Movimentacao;
use Esic\Validators\MovimentacaoValidator;

/**
 * Class MovimentacaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class MovimentacaoRepositoryEloquent extends BaseRepository implements MovimentacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Movimentacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
