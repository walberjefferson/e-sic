<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrgaoRepository
 * @package namespace Esic\Repositories;
 */
interface OrgaoRepository extends RepositoryInterface
{
    //
}
