<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\OrgaoRepository;
use Esic\Models\Orgao;
use Esic\Validators\OrgaoValidator;

/**
 * Class OrgaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class OrgaoRepositoryEloquent extends BaseRepository implements OrgaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Orgao::class;
    }

    public function create(array $attributes)
    {

        if (!isset($attributes['central_sic'])) {
            $attributes['central_sic'] = false;
        } else {
            $attributes['central_sic'] = true;
        }
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        if (!isset($attributes['central_sic'])) {
            $attributes['central_sic'] = false;
        } else {
            $attributes['central_sic'] = true;
        }
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        return parent::update($attributes, $id);
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
