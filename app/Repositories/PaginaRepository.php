<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaginaRepository
 * @package namespace Esic\Repositories;
 */
interface PaginaRepository extends RepositoryInterface
{
    //
}
