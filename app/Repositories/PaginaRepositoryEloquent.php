<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\PaginaRepository;
use Esic\Models\Pagina;
use Esic\Validators\PaginaValidator;

/**
 * Class PaginaRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class PaginaRepositoryEloquent extends BaseRepository implements PaginaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Pagina::class;
    }

    public function create(array $attributes)
    {
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        return parent::update($attributes, $id);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
