<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PerfilRepository
 * @package namespace Esic\Repositories;
 */
interface PerfilRepository extends RepositoryInterface
{
    //
}
