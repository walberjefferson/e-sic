<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\PerfilRepository;
use Esic\Models\Perfil;
use Esic\Validators\PerfilValidator;

/**
 * Class PerfilRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class PerfilRepositoryEloquent extends BaseRepository implements PerfilRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Perfil::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
