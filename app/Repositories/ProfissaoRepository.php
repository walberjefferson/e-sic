<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProfissaoRepository
 * @package namespace Esic\Repositories;
 */
interface ProfissaoRepository extends RepositoryInterface
{
    //
}
