<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\ProfissaoRepository;
use Esic\Models\Profissao;
use Esic\Validators\ProfissaoValidator;

/**
 * Class ProfissaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class ProfissaoRepositoryEloquent extends BaseRepository implements ProfissaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Profissao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
