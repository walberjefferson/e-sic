<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProrrogacaoRepository
 * @package namespace Esic\Repositories;
 */
interface ProrrogacaoRepository extends RepositoryInterface
{
    //
}
