<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\ProrrogacaoRepository;
use Esic\Models\Prorrogacao;
use Esic\Validators\ProrrogacaoValidator;

/**
 * Class ProrrogacaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class ProrrogacaoRepositoryEloquent extends BaseRepository implements ProrrogacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Prorrogacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
