<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RespostaRepository
 * @package namespace Esic\Repositories;
 */
interface RespostaRepository extends RepositoryInterface
{
    //
}
