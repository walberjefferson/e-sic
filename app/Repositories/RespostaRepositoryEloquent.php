<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\RespostaRepository;
use Esic\Models\Resposta;
use Esic\Validators\RespostaValidator;

/**
 * Class RespostaRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class RespostaRepositoryEloquent extends BaseRepository implements RespostaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Resposta::class;
    }

    public function create(array $attributes)
    {
        if (!isset($attributes['homologada'])) {
            $attributes['homologada'] = false;
        } else {
            $attributes['homologada'] = true;
        }
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        if (!isset($attributes['homologada'])) {
            $attributes['homologada'] = false;
        } else {
            $attributes['homologada'] = true;
        }
        return parent::update($attributes, $id);
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
