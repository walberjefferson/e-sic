<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SicFisicoRepository
 * @package namespace Esic\Repositories;
 */
interface SicFisicoRepository extends RepositoryInterface
{
    //
}
