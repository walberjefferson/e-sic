<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\SicFisicoRepository;
use Esic\Models\SicFisico;
use Esic\Validators\SicFisicoValidator;

/**
 * Class SicFisicoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class SicFisicoRepositoryEloquent extends BaseRepository implements SicFisicoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SicFisico::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
