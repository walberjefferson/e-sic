<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SituacaoRepository
 * @package namespace Esic\Repositories;
 */
interface SituacaoRepository extends RepositoryInterface
{
    //
}
