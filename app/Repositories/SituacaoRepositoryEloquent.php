<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\SituacaoRepository;
use Esic\Models\Situacao;
use Esic\Validators\SituacaoValidator;

/**
 * Class SituacaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class SituacaoRepositoryEloquent extends BaseRepository implements SituacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Situacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
