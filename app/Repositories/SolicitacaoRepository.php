<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SolicitacaoRepository
 * @package namespace Esic\Repositories;
 */
interface SolicitacaoRepository extends RepositoryInterface
{
    //
}
