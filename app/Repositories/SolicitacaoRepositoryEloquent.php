<?php

namespace Esic\Repositories;

use Carbon\Carbon;
use Esic\Models\ConfiguracaoLai;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\SolicitacaoRepository;
use Esic\Models\Solicitacao;
use Keygen;

/**
 * Class SolicitacaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class SolicitacaoRepositoryEloquent extends BaseRepository implements SolicitacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Solicitacao::class;
    }

    public function create(array $attributes)
    {
        $prazo_resposta = ConfiguracaoLai::first(['prazo_resposta'])->prazo_resposta;

        $attributes['numero_protocolo'] = Keygen::numeric(5)->generate() . date('zy');
        $attributes['ano_protocolo'] = date('Y');
        $attributes['data_solicitacao'] = Carbon::now()->format('d/m/Y H:i:s');
        $attributes['data_previsao_resposta'] = Carbon::now()->addDays($prazo_resposta)->format('d/m/Y H:i:s');
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        $attributes['data_previsao_resposta'] = $attributes['data_previsao_resposta'] . ' ' . date('H:i:s');
        unset($attributes['data_solicitacao']);

        return parent::update($attributes, $id);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
