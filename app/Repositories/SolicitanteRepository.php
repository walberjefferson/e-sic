<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SolicitanteRepository
 * @package namespace Esic\Repositories;
 */
interface SolicitanteRepository extends RepositoryInterface
{
    //
}
