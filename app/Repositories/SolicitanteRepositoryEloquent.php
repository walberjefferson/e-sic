<?php

namespace Esic\Repositories;

use Esic\Criteria\FindWhereActiveCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\SolicitanteRepository;
use Esic\Models\Solicitante;
use Esic\Validators\SolicitanteValidator;

/**
 * Class SolicitanteRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class SolicitanteRepositoryEloquent extends BaseRepository implements SolicitanteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Solicitante::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
//        $this->pushCriteria(new FindWhereActiveCriteria('confirmado'));
    }
}
