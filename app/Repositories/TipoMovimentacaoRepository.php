<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TipoMovimentacaoRepository
 * @package namespace Esic\Repositories;
 */
interface TipoMovimentacaoRepository extends RepositoryInterface
{
    //
}
