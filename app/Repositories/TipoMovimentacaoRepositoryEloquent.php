<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\TipoMovimentacaoRepository;
use Esic\Models\TipoMovimentacao;
use Esic\Validators\TipoMovimentacaoValidator;

/**
 * Class TipoMovimentacaoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class TipoMovimentacaoRepositoryEloquent extends BaseRepository implements TipoMovimentacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TipoMovimentacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
