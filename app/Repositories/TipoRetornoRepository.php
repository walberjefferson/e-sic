<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TipoRetornoRepository
 * @package namespace Esic\Repositories;
 */
interface TipoRetornoRepository extends RepositoryInterface
{
    //
}
