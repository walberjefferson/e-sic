<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\TipoRetornoRepository;
use Esic\Models\TipoRetorno;
use Esic\Validators\TipoRetornoValidator;

/**
 * Class TipoRetornoRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class TipoRetornoRepositoryEloquent extends BaseRepository implements TipoRetornoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TipoRetorno::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
