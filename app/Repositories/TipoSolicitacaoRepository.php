<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TipoSolicitacaoRepository
 * @package namespace Esic\Repositories;
 */
interface TipoSolicitacaoRepository extends RepositoryInterface
{
    //
}
