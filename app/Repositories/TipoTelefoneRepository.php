<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TipoTelefoneRepository
 * @package namespace Esic\Repositories;
 */
interface TipoTelefoneRepository extends RepositoryInterface
{
    //
}
