<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\TipoTelefoneRepository;
use Esic\Models\TipoTelefone;
use Esic\Validators\TipoTelefoneValidator;

/**
 * Class TipoTelefoneRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class TipoTelefoneRepositoryEloquent extends BaseRepository implements TipoTelefoneRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TipoTelefone::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
