<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Esic\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
