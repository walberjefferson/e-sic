<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Models\User;

/**
 * Class UserRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function create(array $attributes)
    {
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        $attributes['password'] = bcrypt($attributes['password']);
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        if (!isset($attributes['ativo'])) {
            $attributes['ativo'] = false;
        } else {
            $attributes['ativo'] = true;
        }
        if ($attributes['password'] == null) {
            unset($attributes['password'], $attributes['confirm_password']);
        } else {
            $attributes['password'] = bcrypt($attributes['password']);
        }

        if ($attributes['perfil_id'] == null) {
            unset($attributes['perfil_id']);
        } else {
            $attributes['perfil_id'] = (int)$attributes['perfil_id'];
        }

//        dd($attributes, $id);
        return parent::update($attributes, $id);
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
