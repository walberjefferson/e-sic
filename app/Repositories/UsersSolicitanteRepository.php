<?php

namespace Esic\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsersSolicitanteRepository
 * @package namespace Esic\Repositories;
 */
interface UsersSolicitanteRepository extends RepositoryInterface
{
    //
}
