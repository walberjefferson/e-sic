<?php

namespace Esic\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Esic\Repositories\UsersSolicitanteRepository;
use Esic\Models\UsersSolicitante;
use Esic\Validators\UsersSolicitanteValidator;

/**
 * Class UsersSolicitanteRepositoryEloquent
 * @package namespace Esic\Repositories;
 */
class UsersSolicitanteRepositoryEloquent extends BaseRepository implements UsersSolicitanteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsersSolicitante::class;
    }

    public function create(array $attributes)
    {
        $attributes['password'] = bcrypt($attributes['password']);
        return parent::create($attributes);
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
