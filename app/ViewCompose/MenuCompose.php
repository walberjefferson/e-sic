<?php
/**
 * Created by PhpStorm.
 * User: Binho
 * Date: 13/12/2017
 * Time: 17:38
 */

namespace Esic\ViewCompose;

use Esic\Criteria\FindWhereActiveCriteria;
use Esic\Repositories\PaginaRepository;
use Illuminate\View\View;

class MenuCompose
{
    /**
     * @var PaginaRepository
     */
    private $paginaRepository;

    /**
     * MenuCompose constructor.
     */
    public function __construct(PaginaRepository $paginaRepository)
    {
        $this->paginaRepository = $paginaRepository;
    }

    public function compose(View $view)
    {
        $this->paginaRepository->pushCriteria(FindWhereActiveCriteria::class);
        $paginas = $this->paginaRepository->all();
        $view->with(compact('paginas'));
    }
}