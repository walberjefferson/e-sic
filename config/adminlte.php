<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'E-sic',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>E-</b>sic',

    'logo_mini' => '<b>E</b>sic',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MENU NAVEGAÇÃO',
        [
            'text' => 'Solicitações',
            'icon' => 'ticket',
            'icon_color' => 'yellow',
            'route' => 'admin.solicitacao.index',
        ],
//        [
//            'text' => 'Movimentações',
//            'icon' => 'ticket',
//            'icon_color' => 'yellow',
//            'route' => 'admin.movimentacao.index',
//            'can' => 'user-admin'
//        ],
        [
            'text' => 'Páginas',
            'icon' => 'file-text-o',
            'icon_color' => 'red',
            'route' => 'admin.pagina.index',
            'can' => 'user-admin'
        ],
        [
            'text' => 'Orgãos',
            'icon' => 'handshake-o',
            'icon_color' => 'blue',
            'route' => 'admin.orgao.index',
        ],
//        [
//            'text' => 'Respostas',
//            'icon' => 'handshake-o',
//            'icon_color' => 'red',
//            'route' => 'admin.resposta.index',
//            'can' => 'user-admin'
//        ],
        [
            'text' => 'Solicitante',
            'icon' => 'address-book-o',
            'icon_color' => 'purple',
            'route' => 'admin.solicitante.index',
            'can' => 'user-admin'
        ],

        [
            'header' => 'CONFIGURAÇÕES DE CONTAS',
            'can' => 'user-admin'
        ],
        [
            'text' => 'Perfis',
            'route' => 'admin.perfil.index',
            'icon' => 'user',
            'can' => 'user-admin'
        ],
        [
            'text' => 'Usuários',
            'route' => 'admin.usuarios.index',
            'icon' => 'users',
            'can' => 'user-admin'
        ],
//        [
//            'text' => 'Change Password',
//            'url' => 'admin/settings',
//            'icon' => 'lock',
//        ],
//        [
//            'text' => 'Multilevel',
//            'icon' => 'share',
//            'submenu' => [
//                [
//                    'text' => 'Level One',
//                    'url' => '#',
//                ],
//                [
//                    'text' => 'Level One',
//                    'url' => '#',
//                    'submenu' => [
//                        [
//                            'text' => 'Level Two',
//                            'url' => '#',
//                        ],
//                        [
//                            'text' => 'Level Two',
//                            'url' => '#',
//                            'submenu' => [
//                                [
//                                    'text' => 'Level Three',
//                                    'url' => '#',
//                                ],
//                                [
//                                    'text' => 'Level Three',
//                                    'url' => '#',
//                                ],
//                            ],
//                        ],
//                    ],
//                ],
//                [
//                    'text' => 'Level One',
//                    'url' => '#',
//                ],
//            ],
//        ],

        [
            'header' => 'CONFIGURAÇÕES',
            'can' => 'user-admin'
        ],
        [
            'text' => 'Pré-cadastro',
            'icon' => 'folder-open-o',
            'icon_color' => 'blue',
            'can' => 'user-admin',
            'submenu' => [
                [
                    'text' => 'Escolaridade',
                    'icon_color' => 'red',
                    'route' => 'admin.escolaridade.index',
                ],

                [
                    'text' => 'Situação',
                    'icon_color' => 'yellow',
                    'route' => 'admin.situacao.index',
                ],


                [
                    'text' => 'Faixa Etária',
                    'icon_color' => 'green',
                    'route' => 'admin.faixa-etaria.index',
                ],
                [
                    'text' => 'Tipo de Retorno',
                    'icon_color' => 'yellow',
                    'route' => 'admin.tipo_retorno.index',
                ],
                [
                    'text' => 'Tipo de Movimentação',
                    'icon_color' => 'yellow',
                    'route' => 'admin.tipo_movimentacao.index',
                ],
                [
                    'text' => 'Tipo de Solicitação',
                    'icon_color' => 'yellow',
                    'route' => 'admin.tipo_solicitacao.index',
                ],
                [
                    'text' => 'Tipo Telefone',
                    'icon_color' => 'yellow',
                    'route' => 'admin.tipo_telefone.index',
                ],
            ]
        ],
        [
            'text' => 'Entidade',
            'route' => 'admin.config_entidade.index',
            'icon' => 'university',
            'icon_color' => 'teal',
            'can' => 'user-admin'
        ],
        [
            'text' => 'SIC Físico',
            'route' => 'admin.sic_fisico.index',
            'icon' => 'home',
            'icon_color' => 'green',
            'can' => 'user-admin'
        ],
        [
            'text' => 'Lei de Acesso a Informação',
            'route' => 'admin.config_lai.index',
            'icon' => 'file-o',
            'icon_color' => 'red',
            'can' => 'user-admin'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2' => false,
    ],
];
