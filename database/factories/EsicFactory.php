<?php

$faker = \Faker\Factory::create('pt_BR');

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\Esic\Models\Solicitante::class, function () use ($faker) {
    return [
        'telefone' => $faker->landlineNumber(false),
        'logradouro' => $faker->streetName,
        'numero' => $faker->buildingNumber,
        'complemento' => $faker->text(20),
        'bairro' => $faker->streetSuffix,
        'cep' => $faker->postcode,
        'cidade' => $faker->city,
        'uf' => $faker->stateAbbr(),
        'data_nascimento' => $faker->date('d/m/Y'),
        'genero' => array_rand(\Esic\Models\Solicitante::GENERO)
    ];
});

$factory->state(\Esic\Models\Solicitante::class, 'f', function () use ($faker) {
    return [
        'tipo_pessoa' => 'f',
        'documento' => $faker->cpf(false),
        'tipo_documento' => array_rand(\Esic\Models\Solicitante::TIPO_DOCUMENTO),
        'orgao_expedidor' => 'S',
        'data_expedicao' => $faker->date('d/m/Y'),
    ];
});

$factory->state(\Esic\Models\Solicitante::class, 'j', function () use ($faker) {
    return [
        'tipo_pessoa' => 'j',
        'documento' => $faker->cnpj(false),
        'tipo_documento' => 0,
    ];
});

$factory->define(\Esic\Models\Orgao::class, function () use ($faker) {
    return [
        'nome' => $faker->name,
        'sigla' => $faker->stateAbbr(),
        'telefone' => $faker->landlineNumber(false),
        'email' => $faker->email,
        'responsavel' => $faker->name,
        'central_sic' => rand(0, 1),
        'ativo' => rand(0, 1)
    ];
});

$factory->define(\Esic\Models\Pagina::class, function () use ($faker) {
    return [
        'nome' => $faker->name,
        'conteudo' => $faker->paragraph(15, true)
    ];
});

$factory->define(\Esic\Models\Solicitacao::class, function () use ($faker) {
    return [
        'numero_protocolo' => \Keygen\Keygen::numeric(5)->generate() . date('zy'),
        'ano_protocolo' => $faker->year('now'),
        'data_solicitacao' => \Carbon\Carbon::now()->format('d/m/Y H:i:s'),
        'data_previsao_resposta' => $faker->dateTimeThisCentury()->format('d/m/Y H:i:s'),
        'descricao_solicitacao' => $faker->text(150)
    ];
});

$factory->define(\Esic\Models\Resposta::class, function () use ($faker) {
    return [
        'data' => $faker->date('d/m/Y'),
        'descricao' => $faker->text(),
        'homologada' => rand(0, 1),
        'data_homologacao' => $faker->date('d/m/Y')
    ];
});

$factory->define(\Esic\Models\Movimentacao::class, function () use ($faker) {
    return [
        'data_movimentacao' => $faker->dateTime()->format('d/m/Y H:i:s'),
        'despacho' => $faker->text(),
        'data_recebimento' => $faker->dateTime()->format('d/m/Y H:i:s')
    ];
});

$factory->define(Esic\Models\UsersSolicitante::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'ativo' => rand(0, 1)
    ];
});