<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitantesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitantes', function (Blueprint $table) {
            $table->increments('id');
            // tipo solicitante
            $table->enum('tipo_pessoa', ['f', 'j']);

            // identificação
            $table->string('documento', 14);
            $table->integer('tipo_documento')->nullable();
            $table->string('orgao_expedidor', 10)->nullable();
            $table->date('data_expedicao')->nullable();

            // dados pessoais
            $table->date('data_nascimento')->nullable();
            $table->string('telefone', 16)->nullable(); // (82) 12345-7890
            $table->integer('genero');

            $table->integer('tipo_telefone_id')->unsigned()->nullable();
            $table->foreign('tipo_telefone_id')->references('id')->on('tipo_telefone');
            $table->integer('profissao_id')->unsigned()->nullable();
            $table->foreign('profissao_id')->references('id')->on('profissao');

            // endereço
            $table->string('logradouro', 200)->nullable();
            $table->string('numero', 10)->nullable();
            $table->string('bairro', 80)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('cidade', 100)->nullable();
            $table->string('cep', 9)->nullable();
            $table->string('complemento', 90)->nullable();

            $table->integer('escolaridade_id')->unsigned()->nullable();
            $table->foreign('escolaridade_id')->references('id')->on('escolaridade');

            $table->integer('faixaetaria_id')->unsigned()->nullable();
            $table->foreign('faixaetaria_id')->references('id')->on('faixa_etaria');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitantes');
    }

}
