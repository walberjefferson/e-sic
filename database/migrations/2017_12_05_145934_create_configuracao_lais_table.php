<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracaoLaisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracao_lai', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prazo_resposta')->nullable();
            $table->integer('prazo_prorrogacao_resposta')->nullable();
            $table->integer('prazo_recurso')->nullable();
            $table->integer('prazo_recurso_resposta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configuracao_lai');
    }

}
