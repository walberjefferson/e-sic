<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgaosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 120);
            $table->string('sigla', 10)->nullable();
            $table->string('telefone', 16)->nullable();
            $table->string('email', 120)->nullable();
            $table->string('responsavel', '120')->nullable();
            $table->boolean('central_sic')->default(false);
            $table->boolean('ativo')->nullable();

            $table->integer('orgao_superior_id')->nullable()->unsigned();
            $table->foreign('orgao_superior_id')->references('id')->on('orgao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orgao');
    }

}
