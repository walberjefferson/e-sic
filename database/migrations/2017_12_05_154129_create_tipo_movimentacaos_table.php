<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoMovimentacaosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_movimentacao', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 60);
            $table->string('abreviacoes', 20)->nullable();
            $table->text('despacho_padrao')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_movimentacao');
	}

}
