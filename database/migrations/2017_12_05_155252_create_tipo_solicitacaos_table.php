<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoSolicitacaosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipo_solicitacao', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 45);
            $table->string('instancia', 10)->nullable();
            $table->boolean('ativo')->default(true);

            $table->integer('tipo_seguinte_id')->nullable()->unsigned();
            $table->foreign('tipo_seguinte_id')->references('id')->on('tipo_solicitacao');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipo_solicitacao');
	}

}
