<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacaosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solicitacao', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('numero_protocolo');
            $table->integer('ano_protocolo');
            $table->dateTime('data_solicitacao')->nullable();
            $table->dateTime('data_previsao_resposta')->nullable();
            $table->text('descricao_solicitacao');
            $table->integer('solicitacao_origem_id')->nullable();

            $table->integer('users_solicitantes_id')->unsigned();
            $table->foreign('users_solicitantes_id')->references('id')->on('users_solicitantes');

            $table->integer('situacao_id')->unsigned();
            $table->foreign('situacao_id')->references('id')->on('situacao');

            $table->integer('tipo_retorno_id')->unsigned();
            $table->foreign('tipo_retorno_id')->references('id')->on('tipo_retorno');

            $table->integer('tipo_solicitacao_id')->unsigned();
            $table->foreign('tipo_solicitacao_id')->references('id')->on('tipo_solicitacao');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solicitacao');
	}

}
