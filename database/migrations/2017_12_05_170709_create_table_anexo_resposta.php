<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnexoResposta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexo_resposta', function (Blueprint $table) {
            $table->integer('resposta_id')->unsigned();
            $table->foreign('resposta_id')->references('id')->on('respostas');

            $table->integer('anexo_id')->unsigned();
            $table->foreign('anexo_id')->references('id')->on('anexo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anexo_resposta');
    }
}
