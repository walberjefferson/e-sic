<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimentacaosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movimentacao', function(Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_movimentacao')->nullable();
            $table->text('despacho')->nullable();
            $table->dateTime('data_recebimento')->nullable();

            $table->integer('tipo_movimentacao_id')->unsigned();
            $table->foreign('tipo_movimentacao_id')->references('id')->on('tipo_movimentacao');

            $table->integer('solicitacao_id')->unsigned();
            $table->foreign('solicitacao_id')->references('id')->on('solicitacao');

            $table->integer('orgao_origem_id')->unsigned()->nullable();
            $table->foreign('orgao_origem_id')->references('id')->on('orgao');

            $table->integer('orgao_destino_id')->unsigned()->nullable();
            $table->foreign('orgao_destino_id')->references('id')->on('orgao');

            $table->integer('usuario_origem_id')->unsigned()->nullable();
            $table->foreign('usuario_origem_id')->references('id')->on('users');

            $table->integer('usuario_recebimento_id')->unsigned()->nullable();
            $table->foreign('usuario_recebimento_id')->references('id')->on('users');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movimentacao');
	}

}
