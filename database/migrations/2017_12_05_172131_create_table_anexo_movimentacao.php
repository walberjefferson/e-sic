<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnexoMovimentacao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexo_movimentacao', function (Blueprint $table) {
            $table->integer('movimentacao_id')->unsigned();
            $table->foreign('movimentacao_id')->references('id')->on('movimentacao');

            $table->integer('anexo_id')->unsigned();
            $table->foreign('anexo_id')->references('id')->on('anexo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anexo_movimentacao');
    }
}
