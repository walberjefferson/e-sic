<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $perfil = \Esic\Models\Perfil::create(['nome' => 'Admin']);;
        $dados = [
            'name' => 'Administrador',
            'email' => 'admin@user.com',
            'password' => bcrypt('secret'),
            'ativo' => true,
            'perfil_id' => $perfil->id
        ];
        \Esic\Models\User::create($dados);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
