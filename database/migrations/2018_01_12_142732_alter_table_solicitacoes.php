<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSolicitacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitacao', function (Blueprint $table) {
            $table->integer('orgao_id')->unsigned()->nullable()->after('users_solicitantes_id');
            $table->foreign('orgao_id')->references('id')->on('orgao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('solicitacao', function (Blueprint $table) {
            $table->dropForeign('solicitacao_orgao_id_foreign');
            $table->dropColumn('orgao_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
