<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSicFisicosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sic_fisico', function (Blueprint $table) {
            $table->increments('id');
            $table->string('endereco')->nullable();
            $table->string('numero', 5)->nullable();
            $table->string('bairro', 60)->nullable();
            $table->string('cep', 10)->nullable();
            $table->string('cidade', 70)->nullable();
            $table->string('estado', 20)->nullable();

            $table->string('decreto')->nullable();
            $table->string('formulario_requerimento')->nullable();
            $table->string('formulario_recurso')->nullable();

            $table->integer('orgao_id', false, true)->nullable();
            $table->foreign('orgao_id')->on('orgao')->references('id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sic_fisico');
    }

}
