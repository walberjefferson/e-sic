<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsInSicFisico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sic_fisico', function (Blueprint $table) {
            $table->text('informacao')->nullable()->after('estado');
            $table->string('horario_funcionamento', 120)->nullable()->after('informacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sic_fisico', function (Blueprint $table) {
            $table->dropColumn('informacao');
            $table->dropColumn('horario_funcionamento');
        });
    }
}
