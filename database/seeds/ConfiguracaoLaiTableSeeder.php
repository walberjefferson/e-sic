<?php

use Illuminate\Database\Seeder;

class ConfiguracaoLaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \Esic\Models\ConfiguracaoLai::truncate();

        \Esic\Models\ConfiguracaoLai::updateOrCreate([
            'prazo_resposta' => 20,
            'prazo_prorrogacao_resposta' => 10,
            'prazo_recurso' => 10,
            'prazo_recurso_resposta' => 10,
        ]);
        dump("Configuração de Lei de Acesso a Informação Inserida com Sucesso!");
    }
}
