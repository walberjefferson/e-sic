<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('database.default') == 'mysql') {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        // $this->call(UsersTableSeeder::class);
        $this->call(FaixaEtariaTableSeeder::class);
        $this->call(TipoTelefoneTableSeeder::class);
        $this->call(EscolaridadeTableSeeder::class);
        $this->call(ConfiguracaoLaiTableSeeder::class);
        $this->call(TipoMovimentacaoTableSeeder::class);
        $this->call(TipoRetornoTableSeeder::class);
        $this->call(TipoSolicitacaoTableSeeder::class);
        $this->call(SituacaoTableSeeder::class);
        $this->call(ProfissaoTableSeeder::class);


        if (config('app.seed_faker')) {
            $this->call(PaginasTableSeeder::class);
            $this->call(OrgaoTableSeeder::class);
            $this->call(SolicitanteTableSeeder::class);
            $this->call(UserSolicitanteTableSeeder::class);
            $this->call(SolicitacaoTableSeeder::class);
            $this->call(MovimentacaoTableSeeder::class);
            $this->call(RespostaTableSeeder::class);
        }
        if (config('database.default') == 'mysql') {
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }
    }
}
