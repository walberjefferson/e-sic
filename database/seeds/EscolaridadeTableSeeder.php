<?php

use Illuminate\Database\Seeder;

class EscolaridadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Sem instrução formal',
            'Ensino fundamental',
            'Ensino médio',
            'Ensino Superior',
            'Pós-graduação',
            'Mestrado/Doutorado'
        ];
        foreach ($dados as $k => $dado){
            \Esic\Models\Escolaridade::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\Escolaridade::count();
        dump("Foram salvos {$dados} registros");
    }
}
