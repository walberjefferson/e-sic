<?php

use Illuminate\Database\Seeder;

class FaixaEtariaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Ate 20 anos',
            'de 21 a 40 anos',
            'de 41 a 60 anos',
            'Acima de 60 anos',
        ];
        foreach ($dados as $k => $dado){
            \Esic\Models\FaixaEtaria::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\FaixaEtaria::count();
        dump("Foram salvos {$dados} registros");
    }
}
