<?php

use Illuminate\Database\Seeder;

class MovimentacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Movimentacao::truncate();

        $tipoMovimentacao = \Esic\Models\TipoMovimentacao::all();
        $solicitacoes = \Esic\Models\Solicitacao::all();
        $orgaos = \Esic\Models\Orgao::all();
        $usuarios = \Esic\Models\User::all();

        /** @var \Illuminate\Database\Eloquent\Collection $movimentacoes */
        $movimentacoes = factory(\Esic\Models\Movimentacao::class, 10)->make();

        $movimentacoes->each(function ($movimentacao) use ($tipoMovimentacao, $solicitacoes, $orgaos, $usuarios) {
            $movimentacao->tipo_movimentacao_id = $tipoMovimentacao->random()->id;
            $movimentacao->solicitacao_id = $solicitacoes->random()->id;
            $movimentacao->orgao_origem_id = $orgaos->random()->id;
            $movimentacao->orgao_destino_id = $orgaos->random()->id;
            $movimentacao->usuario_origem_id = $usuarios->random()->id;
            $movimentacao->usuario_recebimento_id = $usuarios->random()->id;
            $movimentacao->save();
        });

        $dados = \Esic\Models\Movimentacao::count();
        dump("Foram salvos {$dados} registros");
    }
}
