<?php

use Illuminate\Database\Seeder;

class OrgaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Orgao::truncate();
        factory(\Esic\Models\Orgao::class, 5)->create();
        $orgaos = \Esic\Models\Orgao::all()->count();
        dump("Foram salvos {$orgaos} registros") ;
    }
}
