<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Pagina::truncate();

        factory(\Esic\Models\Pagina::class, 3)->create();

        $dados = \Esic\Models\Pagina::count();
        dump("Foram salvos {$dados} registros");
    }
}
