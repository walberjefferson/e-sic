<?php

use Illuminate\Database\Seeder;

class ProfissaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Empregado - setor privado',
            'Empresário/empreendedor',
            'Estudante',
            'Jornalista',
            'Membro de ONG Internacional',
            'Membro de ONG nacional',
            'Membro de partido político',
            'Outra',
            'Pesquisador',
            'Professor',
            'Profissional Liberal',
            'Representando de sindicado',
            'Servidor público',
            'Servidor público municipal',
            'Servidor público estadual',
            'Servidor público federal'
        ];

        foreach ($dados as $dado) {
            \Esic\Models\Profissao::create(['nome' => $dado]);
        }

        $quantidade = \Esic\Models\Profissao::count();
        dump("Foram salvos {$quantidade} registros");
    }
}
