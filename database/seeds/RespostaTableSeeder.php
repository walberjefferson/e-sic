<?php

use Illuminate\Database\Seeder;

class RespostaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Resposta::truncate();

        $usuarios = \Esic\Models\User::all();
        $orgaos = \Esic\Models\Orgao::all();
        $solicitacoes = \Esic\Models\Solicitacao::all();

        /** @var \Illuminate\Database\Eloquent\Collection $respostas */
        $respostas = factory(\Esic\Models\Resposta::class, 20)->make();

        $respostas->each(function($resposta) use ($usuarios, $orgaos, $solicitacoes) {
            $resposta->usuario_homologado_id = $usuarios->random()->id;
            $resposta->usuario_id = $usuarios->random()->id;
            $resposta->orgao_id = $orgaos->random()->id;
            $resposta->solicitacao_id = $solicitacoes->random()->id;
            $resposta->save();
        });

        $dados = \Esic\Models\Resposta::count();
        dump("Foram salvos {$dados} registros");
    }
}
