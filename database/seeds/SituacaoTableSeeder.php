<?php

use Illuminate\Database\Seeder;

class SituacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Aguardando resposta',
            'Respondido',
            'Negada'
        ];

        foreach ($dados as $dado){
            \Esic\Models\Situacao::updateOrCreate(['nome' => $dado]);
        }

        $quantidade = \Esic\Models\Situacao::count();
        dump("Foram salvos {$quantidade} registros");
    }
}
