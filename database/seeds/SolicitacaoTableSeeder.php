<?php

use Illuminate\Database\Seeder;

class SolicitacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Solicitacao::truncate();

        $solicitantes = \Esic\Models\UsersSolicitante::all();
        $situacao = \Esic\Models\Situacao::all();
        $tipoRetorno = \Esic\Models\TipoRetorno::all();
        $tipoSolicitacao = \Esic\Models\TipoSolicitacao::all();
        $orgaos = \Esic\Models\Orgao::all();

        /** @var \Illuminate\Database\Eloquent\Collection $solicitacoes */
        $solicitacoes = factory(\Esic\Models\Solicitacao::class, 30)->make();

        $solicitacoes->each(function ($solicitacao) use ($solicitantes, $situacao, $tipoRetorno, $tipoSolicitacao, $orgaos) {
            $solicitacao->users_solicitantes_id = $solicitantes->random()->id;
            $solicitacao->orgao_id = $orgaos->random()->id;
            $solicitacao->situacao_id = $situacao->random()->id;
            $solicitacao->tipo_retorno_id = $tipoRetorno->random()->id;
            $solicitacao->tipo_solicitacao_id = $tipoSolicitacao->random()->id;
            $solicitacao->save();
        });

        $dados = \Esic\Models\Solicitacao::count();
        dump("Foram salvos {$dados} registros");

    }
}
