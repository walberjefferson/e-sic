<?php

use Illuminate\Database\Seeder;

class SolicitanteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\Solicitante::truncate();

        $escolaridades = \Esic\Models\Escolaridade::all();
        $tipoTelefone = \Esic\Models\TipoTelefone::all();
        $faixaEtaria = \Esic\Models\FaixaEtaria::all();
        $profissoes = \Esic\Models\Profissao::all();

        /** @var \Illuminate\Database\Eloquent\Collection $solicitantesFisicos */
        $solicitantesFisicos = factory(\Esic\Models\Solicitante::class, 20)->states('f')->make();

        /** @var \Illuminate\Database\Eloquent\Collection $solicitantesJuridicos */
        $solicitantesJuridicos = factory(\Esic\Models\Solicitante::class, 20)->states('j')->make();

        $solicitantesFisicos->each(function ($solicitante) use ($escolaridades, $tipoTelefone, $faixaEtaria, $profissoes) {
            $solicitante->profissao_id = $profissoes->random()->id;
            $solicitante->tipo_telefone_id = $tipoTelefone->random()->id;
            $solicitante->escolaridade_id = $tipoTelefone->random()->id;
            $solicitante->faixaetaria_id = $faixaEtaria->random()->id;
            $solicitante->save();
        });

        $solicitantesJuridicos->each(function ($solicitante) use ($escolaridades, $tipoTelefone, $faixaEtaria, $profissoes) {
            $solicitante->profissao_id = $profissoes->random()->id;
            $solicitante->tipo_telefone_id = $tipoTelefone->random()->id;
            $solicitante->escolaridade_id = $tipoTelefone->random()->id;
            $solicitante->faixaetaria_id = $faixaEtaria->random()->id;
            $solicitante->save();
        });

        $dados = \Esic\Models\Solicitante::count();
        dump("Foram salvos {$dados} registros");
    }
}
