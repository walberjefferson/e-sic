<?php

use Illuminate\Database\Seeder;

class TipoMovimentacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Inicial',
            'Tramitação',
            'Resposta',
            'Diligência'
        ];

        foreach ($dados as $k => $dado){
            \Esic\Models\TipoMovimentacao::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\TipoMovimentacao::count();
        dump("Foram salvos {$dados} registros");
    }
}
