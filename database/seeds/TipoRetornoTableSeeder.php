<?php

use Illuminate\Database\Seeder;

class TipoRetornoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Email',
            'Correios',
            'Fax'
        ];

        foreach ($dados as $k => $dado) {
            \Esic\Models\TipoRetorno::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\TipoRetorno::count();
        dump("Foram salvos {$dados} registros");
    }
}
