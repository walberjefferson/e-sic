<?php

use Illuminate\Database\Seeder;

class TipoSolicitacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Inicial',
            'Recurso em primeira Instância',
            'Recurso em segunda Instância'
        ];

        foreach ($dados as $k => $dado){
            \Esic\Models\TipoSolicitacao::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\TipoSolicitacao::count();
        dump("Foram salvos {$dados} registros");
    }
}
