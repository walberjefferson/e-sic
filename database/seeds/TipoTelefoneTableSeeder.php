<?php

use Illuminate\Database\Seeder;

class TipoTelefoneTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dados = [
            'Comercial',
            'Residencial',
            'Pessoal'
        ];
        foreach ($dados as $k => $dado) {
            \Esic\Models\TipoTelefone::updateOrCreate(['nome' => $dado]);
        }
        $dados = \Esic\Models\TipoTelefone::count();
        dump("Foram salvos {$dados} registros");
    }
}
