<?php

use Illuminate\Database\Seeder;

class UserSolicitanteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Esic\Models\UsersSolicitante::truncate();

        $usuarios = factory(\Esic\Models\UsersSolicitante::class, 40)->make();
        $solicitantes = \Esic\Models\Solicitante::all();

        $usuarios->each(function ($usuario) use ($solicitantes) {
            $usuario->solicitante_id = $solicitantes->random()->id;
            $usuario->save();
        });

        $dados = \Esic\Models\UsersSolicitante::count();
        dump("Foram salvos {$dados} registros");

    }
}
