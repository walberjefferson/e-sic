# E-SIC

Sistema para o uso dos municípios que tenham interesse de implementar.

#### Sistema desenvolvido com Laravel 5.4

- [Documentação do Laravel](https://laravel.com/docs/5.4).

#### Pacotes utilizados

* [Laravel 5 IDE Helper Generator](https://github.com/barryvdh/laravel-ide-helper)
* [Easy AdminLTE integration with Laravel 5](https://github.com/jeroennoten/Laravel-AdminLTE)
* [Laravel Collective](https://laravelcollective.com/docs/5.3/html)
* [Laravel 5 Repositories](https://github.com/andersao/l5-repository)
* [MaterializeCss](http://materializecss.com)

## Licença
O framework Laravel é um software de código aberto licenciado sob a  [licença MIT](http://opensource.org/licenses/MIT).
