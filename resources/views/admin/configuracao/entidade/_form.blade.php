<div class="row">
    {!! Html::openFormGroup('entidade', $errors, 'col-md-8') !!}
    {!! Form::label('entidade', 'Nome Entidade', ['class' => 'control-label']) !!}
    {!! Form::text('entidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('entidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cnpj', $errors, 'col-md-4') !!}
    {!! Form::label('cnpj', 'CNPJ', ['class' => 'control-label']) !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
    {!! Form::error('cnpj', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('logradouro', $errors, 'col-md-6') !!}
    {!! Form::label('logradouro', 'Endereço', ['class' => 'control-label']) !!}
    {!! Form::text('logradouro', null, ['class' => 'form-control']) !!}
    {!! Form::error('logradouro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('numero', $errors, 'col-md-3') !!}
    {!! Form::label('numero', 'Número', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
    {!! Form::error('numero', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('bairro', $errors, 'col-md-3') !!}
    {!! Form::label('bairro', 'Bairro', ['class' => 'control-label']) !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
    {!! Form::error('bairro', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('cep', $errors, 'col-md-3') !!}
    {!! Form::label('cep', 'CEP', ['class' => 'control-label']) !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
    {!! Form::error('cep', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cidade', $errors, 'col-md-6') !!}
    {!! Form::label('cidade', 'Cidade', ['class' => 'control-label']) !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('cidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('uf', $errors, 'col-md-3') !!}
    {!! Form::label('uf', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::text('uf', null, ['class' => 'form-control']) !!}
    {!! Form::error('uf', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>
