@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                @if($dados)
                    <a href="{{ route($rota->edit, $dados->id) }}" class="btn bg-olive btn-flat"><i class="fa fa-pencil"></i> Editar</a>
                @else
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i> Adicionar</a>
                @endif
            </div>
        </div>
        <div class="box-body">
            @if($dados)
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <th width="15%">Entidade</th>
                            <td>{{ $dados->entidade }}</td>
                        </tr>
                        <tr>
                            <th>CNPJ</th>
                            <td>{{ $dados->cnpj }}</td>
                        </tr>
                        <tr>
                            <th>logradouro</th>
                            <td>{{ $dados->logradouro }}</td>
                        </tr>
                        <tr>
                            <th>número</th>
                            <td>{{ $dados->numero }}</td>
                        </tr>
                        <tr>
                            <th>bairro</th>
                            <td>{{ $dados->bairro }}</td>
                        </tr>
                        <tr>
                            <th>cidade</th>
                            <td>{{ $dados->cidade }}</td>
                        </tr>
                        <tr>
                            <th>UF</th>
                            <td>{{ $dados->uf }}</td>
                        </tr>
                        <tr>
                            <th>CEP</th>
                            <td>{{ $dados->cep }}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            @else
                <h3 class="text-center">Inserir informações da entidade!</h3>
            @endif
        </div>
    </div>
@stop

@section('css')
    <style>
        .table > tbody > tr > th {
            text-align: right;
            text-transform: capitalize;
        }
    </style>
@stop

@section('js')

@stop