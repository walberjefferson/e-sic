{!! Html::openFormGroup('prazo_resposta', $errors) !!}
{!! Form::label('prazo_resposta', 'Prazo Resposta', ['class' => 'control-label']) !!}
{!! Form::text('prazo_resposta', null, ['class' => 'form-control']) !!}
{!! Form::error('prazo_resposta', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('prazo_prorrogacao_resposta', $errors) !!}
{!! Form::label('prazo_prorrogacao_resposta', 'Prazo Prorrogacao Resposta', ['class' => 'control-label']) !!}
{!! Form::text('prazo_prorrogacao_resposta', null, ['class' => 'form-control']) !!}
{!! Form::error('prazo_prorrogacao_resposta', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('prazo_recurso', $errors) !!}
{!! Form::label('prazo_recurso', 'Prazo Recurso', ['class' => 'control-label']) !!}
{!! Form::text('prazo_recurso', null, ['class' => 'form-control']) !!}
{!! Form::error('prazo_recurso', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('prazo_recurso_resposta', $errors) !!}
{!! Form::label('prazo_recurso_resposta', 'Prazo Recurso Resposta', ['class' => 'control-label']) !!}
{!! Form::text('prazo_recurso_resposta', null, ['class' => 'form-control']) !!}
{!! Form::error('prazo_recurso_resposta', $errors) !!}
{!! Html::closeFormGroup() !!}

