@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                @if($dados->count())
                    <a href="{{ route($rota->edit, $dados->id) }}" class="btn bg-olive btn-flat"><i class="fa fa-pencil"></i> Editar</a>
                @else
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i> Adicionar</a>
                @endif
            </div>
        </div>
        <div class="box-body">
            @if($dados->count())
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <th width="25%">Prazo de Resposta</th>
                            <td>{{ $dados->prazo_resposta }}</td>
                        </tr>
                        <tr>
                            <th>Prazo de Prorrogação de Resposta</th>
                            <td>{{ $dados->prazo_prorrogacao_resposta }}</td>
                        </tr>
                        <tr>
                            <th>Prazo de Recurso</th>
                            <td>{{ $dados->prazo_recurso }}</td>
                        </tr>
                        <tr>
                            <th>Prazo Recurso Resposta</th>
                            <td>{{ $dados->prazo_recurso_resposta }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @else
                <h3 class="text-center">Inserir informações!</h3>
            @endif
        </div>
    </div>
@stop

@section('css')
    <style>
        .table > tbody > tr > th {
            text-align: right;
        }
    </style>
@stop

@section('js')

@stop