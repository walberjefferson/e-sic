{!! Html::openFormGroup('orgao_id', $errors) !!}
{!! Form::label('orgao_id', 'Orgãos', ['class' => 'control-label']) !!}
{!! Form::select('orgao_id', $orgaos, null, ['class' => 'form-control']) !!}
{!! Form::error('orgao_id', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="row">
    {!! Html::openFormGroup('endereco', $errors, 'col-md-6') !!}
    {!! Form::label('endereco', 'Endereço', ['class' => 'control-label']) !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
    {!! Form::error('endereco', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('numero', $errors, 'col-md-3') !!}
    {!! Form::label('numero', 'Número', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
    {!! Form::error('numero', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('bairro', $errors, 'col-md-3') !!}
    {!! Form::label('bairro', 'Bairro', ['class' => 'control-label']) !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
    {!! Form::error('bairro', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('cep', $errors, 'col-md-3') !!}
    {!! Form::label('cep', 'CEP', ['class' => 'control-label']) !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
    {!! Form::error('cep', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cidade', $errors, 'col-md-3') !!}
    {!! Form::label('cidade', 'Cidade', ['class' => 'control-label']) !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('cidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('estado', $errors, 'col-md-3') !!}
    {!! Form::label('estado', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
    {!! Form::error('estado', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('horario_funcionamento', $errors, 'col-md-3') !!}
    {!! Form::label('horario_funcionamento', 'Horário de Funcionamento', ['class' => 'control-label']) !!}
    {!! Form::text('horario_funcionamento', null, ['class' => 'form-control']) !!}
    {!! Form::error('horario_funcionamento', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('informacao', $errors) !!}
{!! Form::label('informacao', 'Informações', ['class' => 'control-label']) !!}
{!! Form::textarea('informacao', null, ['class' => 'form-control']) !!}
{!! Form::error('informacao', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="row">
    @if(!isset($dados->decreto))
        {!! Html::openFormGroup('decreto', $errors, 'col-md-4') !!}
        {!! Form::label('decreto', 'Decreto', ['class' => 'control-label']) !!}
        {!! Form::file('decreto') !!}
        {!! Form::error('decreto', $errors) !!}
        {!! Html::closeFormGroup() !!}
    @endif

    @if(!isset($dados->formulario_requerimento))
        {!! Html::openFormGroup('formulario_requerimento', $errors, 'col-md-4') !!}
        {!! Form::label('formulario_requerimento', 'Formulário Requerimento', ['class' => 'control-label']) !!}
        {!! Form::file('formulario_requerimento') !!}
        {!! Form::error('formulario_requerimento', $errors) !!}
        {!! Html::closeFormGroup() !!}
    @endif

    @if(!isset($dados->formulario_recurso))
        {!! Html::openFormGroup('formulario_recurso', $errors, 'col-md-4') !!}
        {!! Form::label('formulario_recurso', 'Formulário de Recurso', ['class' => 'control-label']) !!}
        {!! Form::file('formulario_recurso') !!}
        {!! Form::error('formulario_recurso', $errors) !!}
        {!! Html::closeFormGroup() !!}
    @endif
</div>

