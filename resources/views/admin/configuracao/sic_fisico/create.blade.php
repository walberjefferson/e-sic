@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{$view->titulo}}
        <small>Adicionar</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::open(['route' => $rota->store, 'class' => 'form', 'files' => true]) !!}
            @include($view->folder . '_form')
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Cadastrar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop