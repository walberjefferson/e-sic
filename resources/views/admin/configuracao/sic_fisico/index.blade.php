@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                @if($dados)
                    <a href="{{ route($rota->edit, $dados->id) }}" class="btn bg-olive btn-flat"><i
                                class="fa fa-pencil"></i> Editar</a>
                @else
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i>
                        Adicionar</a>
                @endif
            </div>
        </div>
        <div class="box-body">
            @if($dados)
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <th width="15%">Orgão</th>
                            <td>{{ $dados->orgao['nome'] }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $dados->orgao['email'] }}</td>
                        </tr>
                        <tr>
                            <th>endereço</th>
                            <td>{{ $dados->endereco }}</td>
                        </tr>
                        <tr>
                            <th>número</th>
                            <td>{{ $dados->numero }}</td>
                        </tr>
                        <tr>
                            <th>bairro</th>
                            <td>{{ $dados->bairro }}</td>
                        </tr>
                        <tr>
                            <th>cidade</th>
                            <td>{{ $dados->cidade }}</td>
                        </tr>
                        <tr>
                            <th>UF</th>
                            <td>{{ $dados->estado }}</td>
                        </tr>
                        <tr>
                            <th>CEP</th>
                            <td>{{ $dados->cep }}</td>
                        </tr>
                        <tr>
                            <th>Horário de Funcionamento</th>
                            <td>{{ $dados->horario_funcionamento }}</td>
                        </tr>
                        <tr>
                            <th>Informações</th>
                            <td>{{ $dados->informacao }}</td>
                        </tr>


                        </tbody>
                    </table>

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="33%" class="text-center">Decreto</th>
                            <th class="text-center">Formulário de Requerimento</th>
                            <th width="33%" class="text-center">Formulário de Recurso</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                @if(empty($dados->decreto))
                                    <p class="text-center">Não há decreto publicado!</p>
                                @else
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-primary"
                                           href="{{ asset('/storage/sic/' . $dados->decreto) }}">
                                            <i class="fa fa-download"></i> Download
                                        </a>
                                        <a class="btn btn-danger"
                                           onclick="if(confirm('Deseja realmente excluir?')){return true}else{return false}"
                                           href="{{ route('admin.sic_fisico.destroy-file', [$dados->id, 'decreto']) }}">
                                            <i class="fa fa-trash"></i> Excluir
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if(empty($dados->formulario_requerimento))
                                    <p class="text-center">Não há formulário de requerimento publicado!</p>
                                @else
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-primary"
                                           href="{{ asset('/storage/sic/' . $dados->formulario_requerimento) }}">
                                            <i class="fa fa-download"></i> Download
                                        </a>
                                        <a class="btn btn-danger"
                                           onclick="if(confirm('Deseja realmente excluir?')){return true}else{return false}"
                                           href="{{ route('admin.sic_fisico.destroy-file', [$dados->id, 'formulario_requerimento']) }}">
                                            <i class="fa fa-trash"></i> Excluir
                                        </a>
                                    </div>
                                @endif
                            </td>
                            <td>
                                @if(empty($dados->formulario_recurso))
                                    <p class="text-center">Não há formulário de recurso publicado!</p>
                                @else
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-primary"
                                           href="{{ asset('/storage/sic/' . $dados->formulario_recurso) }}">
                                            <i class="fa fa-download"></i> Download
                                        </a>
                                        <a class="btn btn-danger"
                                           onclick="if(confirm('Deseja realmente excluir?')){return true}else{return false}"
                                           href="{{ route('admin.sic_fisico.destroy-file', [$dados->id, 'formulario_recurso']) }}">
                                            <i class="fa fa-trash"></i> Excluir
                                        </a>
                                    </div>
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @else
                <h3 class="text-center">Inserir informações do SIC Físico!</h3>
            @endif
        </div>
    </div>
@stop

@section('css')
    <style>
        .table > tbody > tr > th {
            text-align: right;
            text-transform: capitalize;
        }
    </style>
@stop

@section('js')

@stop