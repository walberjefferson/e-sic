@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $dados->total }}</h3>

                    <p>Total Solicitações</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-paper-outline"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $dados->respondidas }}</h3>

                    <p>Respondido</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-paper-outline"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $dados->aguardando }}</h3>

                    <p>Aguardando resposta</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-paper-outline"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $dados->negada }}</h3>

                    <p>Negada</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-paper-outline"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Últimas Solicitações</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Nº Protocolo</th>
                                <th>Solicitante</th>
                                <th>Forma Retorno</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($solicitacoes as $d)
                                @php
                                    if($d->situacao->nome == "Aguardando resposta") :
                                        $classe = 'label-warning';
                                    elseif($d->situacao->nome == "Respondido") :
                                        $classe = 'label-success';
                                    elseif($d->situacao->nome == "Negada") :
                                        $classe = 'label-danger';
                                    else :
                                        $classe = 'label-info';
                                    endif
                                @endphp
                                <tr>
                                    <td><a href="{{ route('admin.solicitacao.show', $d->id) }}">{{ $d->numero_protocolo }}</a></td>
                                    <td>{{ $d->solicitante->name }}</td>
                                    <td>{{ $d->tipo_retorno->nome }}</td>
                                    <td><span class="label {{ $classe }}">{{ $d->situacao->nome }}</span></td>
                                </tr>
                                {{--{{ $d }}--}}
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix" style="">
                    <a href="{{ route('admin.solicitacao.index') }}" class="btn btn-sm btn-default btn-flat pull-right">Ver todas</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop