@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{$view->titulo}}
        <small>Editar</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($dados, ['route' => [$rota->update, $dados->id], 'class' => 'form', 'method' => 'PUT']) !!}
            @include($view->folder . '_form')
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Atualizar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#data_solicitacao, #data_previsao_resposta').mask('00/00/0000');
        });
    </script>
@stop