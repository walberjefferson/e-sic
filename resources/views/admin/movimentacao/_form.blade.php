<?php
if (isset($dados)) :
    $disable = true;
else :
    $disable = false;
endif;
?>

<div class="row">
    {!! Html::openFormGroup('data_movimentacao', $errors, 'col-md-3') !!}
    {!! Form::label('data_movimentacao', 'Data Movimentação', ['class' => 'control-label']) !!}
    {!! Form::text('data_movimentacao', null, ['class' => 'form-control', 'disabled' => $disable]) !!}
    {!! Form::error('data_movimentacao', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('data_recebimento', $errors, 'col-md-3') !!}
    {!! Form::label('data_recebimento', 'Data Recebimento', ['class' => 'control-label']) !!}
    {!! Form::text('data_recebimento', null, ['class' => 'form-control']) !!}
    {!! Form::error('data_recebimento', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('usuario_origem_id', $errors, 'col-md-3') !!}
    {!! Form::label('usuario_origem_id', 'Usuário Origem', ['class' => 'control-label']) !!}
    {!! Form::select('usuario_origem_id', $usuarios, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Usuário de Origem']) !!}
    {!! Form::error('usuario_origem_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('usuario_recebimento_id', $errors, 'col-md-3') !!}
    {!! Form::label('usuario_recebimento_id', 'Usuário Recebimento', ['class' => 'control-label']) !!}
    {!! Form::select('usuario_recebimento_id', $usuarios, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Usuário de Recebimento']) !!}
    {!! Form::error('usuario_recebimento_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('orgao_origem_id', $errors, 'col-md-3') !!}
    {!! Form::label('orgao_origem_id', 'Orgão Origem', ['class' => 'control-label']) !!}
    {!! Form::select('orgao_origem_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão de Origem', 'disabled' => $disable]) !!}
    {!! Form::error('orgao_origem_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('orgao_destino_id', $errors, 'col-md-3') !!}
    {!! Form::label('orgao_destino_id', 'Orgão Destino', ['class' => 'control-label']) !!}
    {!! Form::select('orgao_destino_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão de Destino']) !!}
    {!! Form::error('orgao_destino_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('solicitacao_id', $errors, 'col-md-3') !!}
    {!! Form::label('solicitacao_id', 'Solicitação', ['class' => 'control-label']) !!}
    {!! Form::select('solicitacao_id', $solicitacoes, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Solicitação', 'disabled' => $disable]) !!}
    {!! Form::error('solicitacao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_movimentacao_id', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_movimentacao_id', 'Tipo de Movimentação', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_movimentacao_id', $tipoMovimentacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Movimentação']) !!}
    {!! Form::error('tipo_movimentacao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('despacho', $errors) !!}
{!! Form::label('despacho', 'Despacho', ['class' => 'control-label']) !!}
{!! Form::textarea('despacho', null, ['class' => 'form-control', 'placeholder' => 'Descreva a Despacho']) !!}
{!! Form::error('despacho', $errors) !!}
{!! Html::closeFormGroup() !!}

@push('js')
    <script>
        $(document).ready(function () {
            $('#data_movimentacao, #data_recebimento').mask('00/00/0000');
        });
    </script>
@endpush