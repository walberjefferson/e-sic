@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                <div class="btn-box-tool">
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i>
                        Adicionar</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th>Data</th>
                        <th>Resumo do Despacho</th>
                        <th>Recebimento</th>
                        <th>Tipo</th>
                        <th width="15%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dados as $d)
                        <tr>
                            <td>{{$d->id}}</td>
                            <td>{{$d->data_movimentacao }}</td>
                            <td>{{ str_limit(strip_tags($d->despacho), 50)  }}</td>
                            <td>{{$d->data_recebimento}}</td>
                            <td>{{$d->tipo_movimentacao['nome']}}</td>
                            <td>
                                <div class="btn-group btn-group-xs btn-group-justified">
                                    <a href="#" data-toggle="modal" data-target="#r-{{$d->id}}" class="btn bg-navy btn-flat"><i
                                                class="fa fa-folder-open-o"></i></a>
                                    <a href="{{ route($rota->edit, $d->id) }}" class="btn bg-olive btn-flat"><i
                                                class="fa fa-pencil"></i></a>

                                    <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                    <a href="{{ route($rota->destroy, $d->id) }}"
                                       class="btn btn-danger btn-flat"
                                       onclick="if(confirm('Deseja realmente excluir?')) {event.preventDefault(); document.getElementById('{{$deleteForm}}').submit(); }else{ return false; }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    {!! Form::open(['route' => [$rota->destroy, $d->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <nav class="text-center">{{ $dados->render() }}</nav>

        </div>
    </div>

    @foreach($dados as $d)
        <div class="modal modal-primary fade" id="r-{{$d->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h3>Nº Solicitação: {{$d->solicitacao['numero_protocolo']}}</h3>


                        <table class="table table-bordered">
                            <tr>
                                <th>Data Movimentação</th>
                                <td>{{ $d->data_movimentacao }}</td>
                            </tr>
                            <tr>
                                <th width="20%">Data Recebimento</th>
                                <td>{{ $d->data_recebimento }}</td>
                            </tr>
                            <tr>
                                <th>Despacho</th>
                                <td>{!! $d->despacho !!}</td>
                            </tr>
                            <tr>
                                <th>Tipo Movimentacao</th>
                                <td>{{ $d->tipo_movimentacao['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Nº da Solicitação</th>
                                <td>{{ $d->solicitacao['numero_protocolo'] }}</td>
                            </tr>
                            <tr>
                                <th>Orgão de Origem</th>
                                <td>{{ $d->orgao_origem['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Orgão de Destino</th>
                                <td>{{ $d->orgao_destino['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Usuário de Origem</th>
                                <td>{{ $d->usuario_origem['name'] }}</td>
                            </tr>
                            <tr>
                                <th>Usuário de Recebimento</th>
                                <td>{{ $d->usuario_recebimento['name'] }}</td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
        </div>
    @endforeach
@stop

@section('css')

@stop

@section('js')

@stop