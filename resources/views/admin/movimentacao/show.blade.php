@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>{{ $dados->nome }}</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th width="15%">Solicitante</th>
                        <td>{{ $dados->solicitante->nome }}</td>
                    </tr>
                    <tr>
                        <th>Nº Protocolo</th>
                        <td>{{ $dados->numero_protocolo }}</td>
                    </tr>
                    <tr>
                        <th>Data Solicitação</th>
                        <td>{{ $dados->data_solicitacao }}</td>
                    </tr>
                    <tr>
                        <th>Previsão de Resposta</th>
                        <td>{{ $dados->data_previsao_resposta }}</td>
                    </tr>
                    <tr>
                        <th>Descrição</th>
                        <td>{{ $dados->descricao_solicitacao }}</td>
                    </tr>
                    <tr>
                        <th>Situação</th>
                        <td>{{ $dados->situacao->nome }}</td>
                    </tr>
                    <tr>
                        <th>Retorno</th>
                        <td>{{ $dados->tipo_retorno->nome }}</td>
                    </tr>
                    <tr>
                        <th>Tipo Solicitação</th>
                        <td>{{ $dados->tipo_solicitacao->nome }}</td>
                    </tr>

                    </tbody>
                </table>
                @if($respostas->count())
                    <h3>Respostas</h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Data</th>
                            <th>Resumo</th>
                            <th colspan="2">Orgão</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($respostas as $resposta)
                            <tr>
                                <td>{{ $resposta->data }}</td>
                                <td>{{ str_limit($resposta->descricao, 50, '...') }}</td>
                                <td>{{ $resposta->orgao->nome }}</td>
                                <td width="5%"><a href="#" class="btn btn-block btn-xs btn-flat bg-navy" data-toggle="modal" data-target="#r-{{$resposta->id}}"><i class="fa fa-folder-open-o"></i> Ver</a></td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="r-{{$resposta->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Conteúdo da Resposta</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{ $resposta->descricao }}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>

        <div class="box-footer">
            <div class="btn-group">
                <a href="{{ route($rota->index) }}" class="btn btn-flat btn-primary"> <i class="fa fa-arrow-left"></i>
                    Voltar</a>
                <a href="{{ route($rota->edit, $dados->id) }}" class="btn btn-flat bg-olive"> <i
                            class="fa fa-pencil"></i> Editar</a>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        table > tbody > tr > th {
            text-align: right;

        }

        table > tbody > tr > th:after {
            content: ':';
        }
    </style>

@stop

@section('js')

@stop