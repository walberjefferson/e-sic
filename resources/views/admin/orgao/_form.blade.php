<div class="row">
    {!! Html::openFormGroup('nome', $errors, 'col-md-9') !!}
    {!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
    {!! Form::error('nome', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('sigla', $errors, 'col-md-3') !!}
    {!! Form::label('sigla', 'Sigla', ['class' => 'control-label']) !!}
    {!! Form::text('sigla', null, ['class' => 'form-control']) !!}
    {!! Form::error('sigla', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('telefone', $errors, 'col-md-3') !!}
    {!! Form::label('telefone', 'Telefone', ['class' => 'control-label']) !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
    {!! Form::error('telefone', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('email', $errors, 'col-md-3') !!}
    {!! Form::label('email', 'E-mail', ['class' => 'control-label']) !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
    {!! Form::error('email', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('responsavel', $errors, 'col-md-3') !!}
    {!! Form::label('responsavel', 'Responsável', ['class' => 'control-label']) !!}
    {!! Form::text('responsavel', null, ['class' => 'form-control']) !!}
    {!! Form::error('responsavel', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('orgao_superior_id', $errors, 'col-md-3') !!}
    {!! Form::label('orgao_superior_id', 'Orgão Superior', ['class' => 'control-label']) !!}
    {!! Form::select('orgao_superior_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão']) !!}
    {!! Form::error('orgao_superior_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>
<div class="form-group">
    <div class="">
        <label for="central_sic">
            {!! Form::checkbox('central_sic', null, null, ['class' => 'minimal']) !!}
            Central SIC?
            {!! Form::error('central_sic', $errors) !!}
        </label>
    </div>

    <div class="">
        <label for="ativo">
            {!! Form::checkbox('ativo', null, null, ['class' => 'minimal']) !!}
            Ativo?
            {!! Form::error('ativo', $errors) !!}
        </label>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function(){
            var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                    onKeyPress: function(val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };

            $('#telefone').mask(SPMaskBehavior, spOptions);

            //iCheck for checkbox and radio inputs

        });
    </script>
@endpush