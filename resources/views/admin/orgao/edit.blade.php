@extends('adminlte::page')

@section('title', 'Orgãos')

@section('content_header')
    <h1>Orgãos
        <small>Editar</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Form::model($dados, ['route' => [$rota->update, $dados->id], 'class' => 'form', 'method' => 'PUT']) !!}
            @include($view->folder . '_form')
            <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Atualizar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop