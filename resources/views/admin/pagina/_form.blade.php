{!! Html::openFormGroup('nome', $errors) !!}
{!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
{!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Digite o nome do perfil']) !!}
{!! Form::error('nome', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('conteudo', $errors) !!}
{!! Form::label('conteudo', 'Coteúdo', ['class' => 'control-label']) !!}
{!! Form::textarea('conteudo', null, ['class' => 'form-control', 'placeholder' => 'Digite o conteudo da página']) !!}
{!! Form::error('conteudo', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="form-group">

    <label for="ativo">
        {!! Form::checkbox('ativo', null, null, ['class' => 'minimal']) !!}
        &nbsp; Ativo?
        {!! Form::error('ativo', $errors) !!}

    </label>

</div>
