{!! Html::openFormGroup('nome', $errors) !!}
{!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
{!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Digite o nome do perfil']) !!}
{!! Form::error('nome', $errors) !!}
{!! Html::closeFormGroup() !!}