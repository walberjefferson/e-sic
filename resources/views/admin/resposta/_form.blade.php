<div class="row">
    {!! Html::openFormGroup('data', $errors, 'col-md-3') !!}
    {!! Form::label('data', 'Data', ['class' => 'control-label']) !!}
    {!! Form::text('data', null, ['class' => 'form-control']) !!}
    {!! Form::error('data', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('data_homologacao', $errors, 'col-md-3') !!}
    {!! Form::label('data_homologacao', 'Data Homologação', ['class' => 'control-label']) !!}
    {!! Form::text('data_homologacao', null, ['class' => 'form-control', 'disabled']) !!}
    {!! Form::error('data_homologacao', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('usuario_id', $errors, 'col-md-3') !!}
    {!! Form::label('usuario_id', 'Usuário', ['class' => 'control-label']) !!}
    {!! Form::select('usuario_id', $usuarios, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Situação']) !!}
    {!! Form::error('usuario_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('usuario_homologado_id', $errors, 'col-md-3') !!}
    {!! Form::label('usuario_homologado_id', 'Usuário Homologado', ['class' => 'control-label']) !!}
    {!! Form::select('usuario_homologado_id', $usuarios, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Solicitante']) !!}
    {!! Form::error('usuario_homologado_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('orgao_id', $errors, 'col-md-6') !!}
    {!! Form::label('orgao_id', 'Orgão', ['class' => 'control-label']) !!}
    {!! Form::select('orgao_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Retorno']) !!}
    {!! Form::error('orgao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('solicitacao_id', $errors, 'col-md-6') !!}
    {!! Form::label('solicitacao_id', 'Solicitação', ['class' => 'control-label']) !!}
    {!! Form::select('solicitacao_id', $solicitacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Solicitação']) !!}
    {!! Form::error('solicitacao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('descricao', $errors) !!}
{!! Form::label('descricao', 'Descrição', ['class' => 'control-label']) !!}
{!! Form::textarea('descricao', null, ['class' => 'form-control', 'placeholder' => 'Descreva a Resposta']) !!}
{!! Form::error('descricao', $errors) !!}
{!! Html::closeFormGroup() !!}

{{--<div class="form-group">--}}
    {{--<label for="ativo">--}}
        {{--{!! Form::checkbox('homologada', null, null, ['class' => 'minimal']) !!}--}}
        {{--Homologado?--}}
        {{--{!! Form::error('homologada', $errors) !!}--}}
    {{--</label>--}}
{{--</div>--}}

@push('js')
    <script>
        $(document).ready(function () {
           $('#data, #data_homologacao').mask('00/00/0000');
        });
    </script>
@endpush