@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                <div class="btn-box-tool">
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i>
                        Adicionar</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th>Solicitação</th>
                        <th width="8%">Data</th>
                        <th width="14%" class="text-center">Data Homologação</th>
                        <th width="12%" class="text-center">Homologada?</th>
                        <th width="15%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dados as $d)
                        <tr>
                            <td>{{$d->id}}</td>
                            <td>{{$d->solicitacao['numero_protocolo']}}
                                - {{ $d->solicitacao->solicitante['name'] }}</td>
                            <td class="text-center">{{$d->data}}</td>
                            <td class="text-center">{{$d->data_homologacao}}</td>
                            <td class="text-center">{!! ($d->homologada) ? '<span class="label bg-green">Sim</span>' : '<span class="label bg-red">Não</span>' !!}</td>
                            <td>
                                <div class="btn-group btn-group-xs btn-group-justified">
                                    <a href="#" data-toggle="modal" data-target="#r-{{$d->id}}"
                                       class="btn bg-navy btn-flat"><i
                                                class="fa fa-folder-open-o"></i></a>
                                    <a href="{{ route($rota->edit, $d->id) }}" class="btn bg-olive btn-flat"><i
                                                class="fa fa-pencil"></i></a>

                                    <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                    <a href="{{ route($rota->destroy, $d->id) }}"
                                       class="btn btn-danger btn-flat"
                                       onclick="if(confirm('Deseja realmente excluir?')) {event.preventDefault(); document.getElementById('{{$deleteForm}}').submit(); }else{ return false; }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    {!! Form::open(['route' => [$rota->destroy, $d->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <nav class="text-center">
                {{ $dados->render() }}
            </nav>
        </div>
    </div>



    @foreach($dados as $d)
        <div class="modal {{ ($d->homologada) ? 'modal-success' : 'modal-danger' }}  fade" id="r-{{$d->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Resposta
                            de {{ $d->solicitacao->solicitante['name'] }}</h4>
                    </div>
                    <div class="modal-body">
                        <h3>Nº Solicitação: {{$d->solicitacao['numero_protocolo']}}</h3>


                        <table class="table table-bordered">
                            <tr>
                                <th>Data</th>
                                <td>{{ $d->data }}</td>
                            </tr>
                            <tr>
                                <th width="20%">Data Homologação</th>
                                <td>{{ $d->data_homologacao }}</td>
                            </tr>
                            <tr>
                                <th>Descrição</th>
                                <td>{{ $d->descricao }}</td>
                            </tr>
                            <tr>
                                <th>Solicitante</th>
                                <td>{{ $d->solicitacao->solicitante['name'] }}</td>
                            </tr>
                            <tr>
                                <th>Orgão</th>
                                <td>{{ $d->orgao['nome'] }}</td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
        </div>
    @endforeach
@stop

@section('css')
    <style>
        .modal-body table tr th {
            text-align: right;
        }
    </style>
@stop

@section('js')

@stop