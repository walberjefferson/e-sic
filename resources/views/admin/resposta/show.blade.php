@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>{{ $dados->nome }}</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th width="15%">Nome</th>
                        <td>{{ $dados->nome }}</td>
                    </tr>
                    <tr>
                        <th>Sigla</th>
                        <td>{{ $dados->sigla }}</td>
                    </tr>
                    <tr>
                        <th>Telefone</th>
                        <td>{{ $dados->telefone }}</td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $dados->email }}</td>
                    </tr>
                    <tr>
                        <th>Responsável</th>
                        <td>{{ $dados->responsavel }}</td>
                    </tr>
                    <tr>
                        <th>Central SIC</th>
                        <td>{{ ($dados->central_sic) ? 'Sim' : 'Não' }}</td>
                    </tr>
                    <tr>
                        <th>Ativo</th>
                        <td>{{ ($dados->ativo) ? 'Sim' : 'Não' }}</td>
                    </tr>
                    <tr>
                        <th>Orgão Superior</th>
                        <td>{{ $dados->orgao_superior['nome'] }}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-footer">
            <div class="btn-group">
                <a href="{{ route($rota->index) }}" class="btn btn-flat btn-primary"> <i class="fa fa-arrow-left"></i> Voltar</a>
                <a href="{{ route($rota->edit, $dados->id) }}" class="btn btn-flat bg-olive"> <i class="fa fa-pencil"></i> Editar</a>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        table > tbody > tr > th {
            text-align: right;

        }

        table > tbody > tr > th:after {
            content: ':';
        }
    </style>

@stop

@section('js')

@stop