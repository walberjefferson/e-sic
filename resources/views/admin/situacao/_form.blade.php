{!! Html::openFormGroup('nome', $errors) !!}
{!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
{!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Digite o nome da situação']) !!}
{!! Form::error('nome', $errors) !!}
{!! Html::closeFormGroup() !!}