<?php
if (isset($dados)) :
    $disable = true;
else :
    $disable = false;
endif;
?>

<div class="row">
    <div class="col-md-4">
        <div class="row">
            {!! Html::openFormGroup('numero_protocolo', $errors, 'col-md-6') !!}
            {!! Form::label('numero_protocolo', 'Nº Protocolo', ['class' => 'control-label']) !!}
            {!! Form::text('numero_protocolo', null, ['class' => 'form-control', 'disabled' => $disable]) !!}
            {!! Form::error('numero_protocolo', $errors) !!}
            {!! Html::closeFormGroup() !!}

            {!! Html::openFormGroup('ano_protocolo', $errors, 'col-md-6') !!}
            {!! Form::label('ano_protocolo', 'Ano Protocolo', ['class' => 'control-label']) !!}
            {!! Form::text('ano_protocolo', null, ['class' => 'form-control', 'disabled' => $disable]) !!}
            {!! Form::error('ano_protocolo', $errors) !!}
            {!! Html::closeFormGroup() !!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="row">
            {!! Html::openFormGroup('data_solicitacao', $errors, 'col-md-4') !!}
            {!! Form::label('data_solicitacao', 'Data Solicitação', ['class' => 'control-label']) !!}
            {!! Form::text('data_solicitacao', null, ['class' => 'form-control', 'disabled' => $disable]) !!}
            {!! Form::error('data_solicitacao', $errors) !!}
            {!! Html::closeFormGroup() !!}

            {!! Html::openFormGroup('data_previsao_resposta', $errors, 'col-md-4') !!}
            {!! Form::label('data_previsao_resposta', 'Previsão de Resposta', ['class' => 'control-label']) !!}
            {!! Form::text('data_previsao_resposta', null, ['class' => 'form-control']) !!}
            {!! Form::error('data_previsao_resposta', $errors) !!}
            {!! Html::closeFormGroup() !!}

            {!! Html::openFormGroup('solicitacao_origem_id', $errors, 'col-md-4') !!}
            {!! Form::label('solicitacao_origem_id', 'Solicitação Origem', ['class' => 'control-label']) !!}
            {!! Form::select('solicitacao_origem_id', $solicitacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Solicitação']) !!}
            {!! Form::error('solicitacao_origem_id', $errors) !!}
            {!! Html::closeFormGroup() !!}

        </div>
    </div>
</div>

<div class="row">
    {!! Html::openFormGroup('solicitantes_id', $errors, 'col-md-3') !!}
    {!! Form::label('solicitantes_id', 'Solicitante', ['class' => 'control-label']) !!}
    {!! Form::select('solicitantes_id', $solicitante, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Solicitante', 'disabled' => $disable]) !!}
    {!! Form::error('solicitantes_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('situacao_id', $errors, 'col-md-3') !!}
    {!! Form::label('situacao_id', 'Situação', ['class' => 'control-label']) !!}
    {!! Form::select('situacao_id', $situacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Situação']) !!}
    {!! Form::error('situacao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_retorno_id', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_retorno_id', 'Tipo de Retorno', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_retorno_id', $tipo_retorno, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Retorno']) !!}
    {!! Form::error('tipo_retorno_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_solicitacao_id', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_solicitacao_id', 'Tipo de Solicitação', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_solicitacao_id', $tipo_solicitacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Solicitação']) !!}
    {!! Form::error('tipo_solicitacao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('descricao_solicitacao', $errors) !!}
{!! Form::label('descricao_solicitacao', 'Descrição da Solicitação', ['class' => 'control-label']) !!}
{!! Form::textarea('descricao_solicitacao', null, ['class' => 'form-control', 'placeholder' => 'Descreva a Solicitação']) !!}
{!! Form::error('descricao_solicitacao', $errors) !!}
{!! Html::closeFormGroup() !!}