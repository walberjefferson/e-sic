@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>{{ $dados->nome }}</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th width="15%">Solicitante</th>
                        <td>{{ $dados->solicitante['name'] }}</td>
                    </tr>
                    <tr>
                        <th>Nº Protocolo</th>
                        <td>{{ $dados->numero_protocolo }}</td>
                    </tr>
                    <tr>
                        <th>Data Solicitação</th>
                        <td>{{ $dados->data_solicitacao }}</td>
                    </tr>
                    <tr>
                        <th>Previsão de Resposta</th>
                        <td>{{ $dados->data_previsao_resposta }}</td>
                    </tr>
                    <tr>
                        <th>Descrição</th>
                        <td>{{ strip_tags($dados->descricao_solicitacao) }}</td>
                    </tr>
                    <tr>
                        <th>Situação</th>
                        <td>{{ $dados->situacao['nome'] }}</td>
                    </tr>
                    <tr>
                        <th>Retorno</th>
                        <td>{{ $dados->tipo_retorno['nome'] }}</td>
                    </tr>
                    <tr>
                        <th>Tipo Solicitação</th>
                        <td>{{ $dados->tipo_solicitacao['nome'] }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-footer">
            <div class="btn-group">

                <a href="{{ route($rota->index) }}" class="btn btn-flat btn-primary"> <i class="fa fa-arrow-left"></i>
                    Voltar</a>
                @if(!$recebido)
                    <a href="{{ route('admin.solicitacao.receber_diligencia', $dados->id) }}"
                       class="btn btn-flat bg-orange">Receber</a>
                @else
                    <a href="#" class="btn btn-flat bg-orange disabled">Recebido</a>
                @endif

            </div>
        </div>
    </div>

    @if(!empty($dados->prorrogacao))
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-clock-o"></i> Prorrogação</h3>
            </div>

            <div class="box-body pad table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th width="15%">Data Prorrogação</th>
                        <td>{{$dados->prorrogacao->data_prorrogacao }}</td>
                    </tr>

                    <tr>
                        <th>Motivo</th>
                        <td>{{ $dados->prorrogacao->motivo }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tramitacao" data-toggle="tab" aria-expanded="true">Tramitações</a></li>
            <li class=""><a href="#resposta" data-toggle="tab" aria-expanded="false">Respostas</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tramitacao">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Despacho</th>
                        <th>Tipo</th>
                        <th>Data Recebimento</th>
                        <th>Orgão origem</th>
                        <th>Orgão destino</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tramitacoes as $movimentacao)
                        <tr>
                            <td width="9%">{{ $movimentacao->data_movimentacao }}</td>
                            <td width="18%"><span data-toggle="tooltip" data-placement="top"
                                                  title="{{ $movimentacao->despacho }}">{{ str_limit($movimentacao->despacho, 20) }}</span>
                            </td>
                            <td>{{ $movimentacao->tipo_movimentacao['nome'] }}</td>
                            <td>{{ $movimentacao->data_recebimento }}</td>
                            <td>{{ $movimentacao->orgao_origem['nome'] }}</td>
                            <td>{{ $movimentacao->orgao_destino['nome'] }}</td>
                            <td width="14%">
                                <div class="btn-group">
                                    <div style="display: inline-block">
                                        <a type="button" id="detalhes"
                                           class="btn btn-flat btn-xs bg-navy dropdown-toggle"
                                           data-toggle="dropdown" role="button" aria-haspopup="true"
                                           aria-expanded="false">
                                            Detalhes <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="detalhes">
                                            <li><a href="#" data-toggle="modal" data-target="#t-{{$movimentacao->id}}">Detalhes</a>
                                            </li>
                                            @if($movimentacao->tipo_movimentacao->nome == "Diligência")
                                                <li><a href="#" data-toggle="modal"
                                                       data-target="#r-{{$movimentacao->id}}">Responder</a></li>
                                            @endif
                                        </ul>
                                    </div>

                                    <div style="display: inline-block;">
                                        <a type="button" id="anexos"
                                           class="btn btn-success btn-flat btn-xs dropdown-toggle"
                                           data-toggle="dropdown" role="button" aria-haspopup="true"
                                           aria-expanded="false">
                                            Anexos <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="anexos">
                                            @forelse($movimentacao->anexo as $anexo)
                                                <li>
                                                    <a href="{{ asset('storage/movimentacao/' . $anexo->url) }}">
                                                        <i class="fa fa-download"></i> Download</a>
                                                </li>
                                            @empty
                                                <li class="dropdown-header">Sem anexos</li>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7"><h4 class="text-center">Nenhuma tramitação até o momento!</h4></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>


            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="resposta">
                @if($dados->respostas()->count())
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="10%">Data</th>
                            <th>Resumo</th>
                            <th>Orgão</th>
                            <th width="15%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($respostas as $resposta)
                            @php
                                if($resposta->homologada) :
                                $status = 'Homologado';
                                $classe = 'label-success';
                                else :
                                $status = 'Não homologado';
                                $classe = 'label-danger';
                                endif;
                            @endphp
                            <tr>
                                <td>{{ $resposta->data }}</td>
                                <td>
                                    {{ str_limit(strip_tags($resposta->descricao), 50, '...') }}
                                    <span class="label {{ $classe }}">{{ $status }}</span>
                                    @forelse($resposta->anexos as $anexo)
                                        <a href="{{ asset('storage/resposta/' . $anexo->url) }}"
                                           class="btn btn-primary btn-xs">
                                            <i class="fa fa-file-text-o"></i>
                                        </a>
                                    @empty
                                        <span class="label label-warning">Sem anexo</span>
                                    @endforelse
                                </td>
                                <td>{{ $resposta->orgao['nome'] }}</td>
                                <td>
                                    <div class="btn-group btn-group-xs btn-block">
                                        @can('user-central')
                                            @if(!$resposta->homologada)
                                                <a href="{{ route('admin.resposta.homologar', [$dados->id, $resposta->id]) }}"
                                                   class="btn btn-flat bg-orange"
                                                   onclick="if(!confirm('Deseja realmente homologar?')){return false;}">
                                                    <i class="fa fa-send-o"></i> Homologar</a>
                                            @else
                                                <a class="btn btn-flat bg-orange" disabled="true">
                                                    <i class="fa fa-send-o"></i> Homologar</a>
                                            @endif
                                        @endcan
                                        <a href="#" class="btn btn-flat bg-navy"
                                           data-toggle="modal" data-target="#r-{{$resposta->id}}">
                                            <i class="fa fa-folder-open-o"></i> Ver</a>
                                    </div>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="r-{{$resposta->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Conteúdo da Resposta</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! $resposta->descricao !!}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h4>Nenhuma resposta até o momento!</h4>
                @endif
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>

    @foreach($tramitacoes as $d)
        <div class="modal modal-primary fade" id="t-{{$d->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h3>Nº Solicitação: {{$d->solicitacao['numero_protocolo']}}</h3>


                        <table class="table table-bordered">
                            <tr>
                                <th>Data Movimentação</th>
                                <td>{{ $d->data_movimentacao }}</td>
                            </tr>
                            <tr>
                                <th width="20%">Data Recebimento</th>
                                <td>{{ $d->data_recebimento }}</td>
                            </tr>
                            <tr>
                                <th>Despacho</th>
                                <td>{!! $d->despacho !!}</td>
                            </tr>
                            <tr>
                                <th>Tipo Movimentacao</th>
                                <td>{{ $d->tipo_movimentacao['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Nº da Solicitação</th>
                                <td>{{ $d->solicitacao['numero_protocolo'] }}</td>
                            </tr>
                            <tr>
                                <th>Orgão de Origem</th>
                                <td>{{ $d->orgao_origem['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Orgão de Destino</th>
                                <td>{{ $d->orgao_destino['nome'] }}</td>
                            </tr>
                            <tr>
                                <th>Usuário de Origem</th>
                                <td>{{ $d->usuario_origem['name'] }}</td>
                            </tr>
                            <tr>
                                <th>Usuário de Recebimento</th>
                                <td>{{ $d->usuario_recebimento['name'] }}</td>
                            </tr>
                        </table>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
        </div>
    @endforeach

    @foreach($tramitacoes as $d)
        <div class="modal fade" id="r-{{$d->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Responder Diligência</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route' => ['admin.solicitacao.responder_diligencia', $d->id], 'files' => true]) !!}

                        {!! Html::openFormGroup('despacho', $errors) !!}
                        {!! Form::label('despacho', 'Despacho', ['class' => 'control-label']) !!}
                        {!! Form::textarea('despacho', null, ['class' => 'form-control', 'placeholder' => 'Descreva a Resposta']) !!}
                        {!! Form::error('despacho', $errors) !!}
                        {!! Html::closeFormGroup() !!}

                        {!! Html::openFormGroup('anexo', $errors) !!}
                        {!! Form::label('anexo', 'Anexo', ['class' => 'control-label']) !!}
                        {!! Form::file('anexo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Descreva sua Solicitação']) !!}
                        {!! Form::error('anexo', $errors) !!}
                        {!! Html::closeFormGroup() !!}

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane-o"></i> Responder
                            </button>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
        </div>
    @endforeach
@stop

@section('css')
    <style>
        table > tbody > tr > th {
            text-align: right;
        }

        table > tbody > tr > th:after {
            content: ':';
        }
    </style>

@stop

@section('js')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop