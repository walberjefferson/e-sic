@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                <div class="btn-box-tool">
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i>
                        Adicionar</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th>Solicitante / Respostas</th>
                        <th>Nº Protocolo</th>
                        <th>Ano Protocolo</th>
                        <th>Forma Retorno</th>
                        <th>Previsão Resposta</th>
                        <th>Situação</th>
                        <th width="15%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dados as $d)
                        <tr>
                            @php
                                if($d->qtd_resposta == 0) :
                                    $class = "label-warning";
                                elseif($d->qtd_resposta == 1) :
                                    $class = "label-info";
                                elseif($d->qtd_resposta == 2) :
                                    $class = "label-primary";
                                else :
                                    $class = "label-success";
                                endif
                            @endphp
                            <td>{{$d->id}}</td>
                            <td>{{$d->solicitante['name'] }} <span
                                        class="label {{ $class }}"> {{ $d->qtd_resposta  }}</span></td>
                            <td>{{$d->numero_protocolo}}</td>
                            <td>{{$d->ano_protocolo}}</td>
                            <td>{{ $d->tipo_retorno['nome'] }}</td>
                            <td>{{$d->data_previsao_resposta}}</td>
                            <td>
                                @if($d->situacao['nome'] == "Respondido")
                                    <span class="label label-success">{{ $d->situacao['nome'] }}</span>
                                @elseif($d->situacao['nome'] == "Negada")
                                    <span class="label label-danger">{{ $d->situacao['nome'] }}</span>
                                @else
                                    <span class="label label-primary">{{ $d->situacao['nome'] }}</span>
                                    @if($d->em_atraso)
                                        <span class="label label-warning">Atrasado</span>
                                    @else
                                        <span class="label label-success">Em dia</span>
                                    @endif
                                @endif
                            </td>

                            @if($d->diligencia())
                                <td>
                                    <div class="btn-group btn-group-xs btn-group-justified">
                                        <a href="{{ route($rota->show, $d->id) }}" class="btn bg-navy btn-flat"><i
                                                    class="fa fa-folder-open-o"></i></a>
                                        <a href="{{ route($rota->edit, $d->id) }}" class="btn bg-olive btn-flat"><i
                                                    class="fa fa-pencil"></i></a>

                                        <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                        <a href="{{ route($rota->destroy, $d->id) }}"
                                           class="btn btn-danger btn-flat"
                                           onclick="if(confirm('Deseja realmente excluir?')) {event.preventDefault(); document.getElementById('{{$deleteForm}}').submit(); }else{ return false; }">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        {!! Form::open(['route' => [$rota->destroy, $d->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            @else
                                <td>
                                    <div class="btn-group btn-group-xs btn-group-justified">
                                        <a href="{{ route('admin.solicitacao.ver_diligencia', $d->id) }}" class="btn bg-navy btn-flat"><i
                                                    class="fa fa-folder-open-o"></i> Diligência</a>
                                    </div>
                                </td>
                            @endif

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <nav class="text-center">{{ $dados->render() }}</nav>

        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop