<?php
if (isset($dados->solicitante)) :
    $name = $dados->solicitante->name;
    $email = $dados->solicitante->email;
    $ativo = $dados->solicitante->ativo;
    $disable = true;
else :
    $name = null;
    $email = null;
    $ativo = null;
    $disable = false;
endif;
?>

<div class="row">
    {!! Html::openFormGroup('name', $errors, 'col-md-6') !!}
    {!! Form::label('name', 'Nome', ['class' => 'control-label']) !!}
    {!! Form::text('name', $name, ['class' => 'form-control', 'disabled' => $disable]) !!}
    {!! Form::error('name', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('email', $errors, 'col-md-6') !!}
    {!! Form::label('email', 'E-mail', ['class' => 'control-label']) !!}
    {!! Form::email('email', $email, ['class' => 'form-control', 'disabled' => $disable]) !!}
    {!! Form::error('email', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('password', $errors, 'col-md-6') !!}
    {!! Form::label('password', 'Senha', ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
    {!! Form::error('password', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('confirm_password', $errors, 'col-md-6') !!}
    {!! Form::label('confirm_password', 'Repita a Senha', ['class' => 'control-label']) !!}
    {!! Form::password('confirm_password', ['class' => 'form-control']) !!}
    {!! Form::error('confirm_password', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('data_nascimento', $errors, 'col-md-3') !!}
    {!! Form::label('data_nascimento', 'Data Nascimento', ['class' => 'control-label']) !!}
    {!! Form::text('data_nascimento', null, ['class' => 'form-control']) !!}
    {!! Form::error('data_nascimento', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_pessoa', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_pessoa', 'Tipo Pessoa', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_pessoa', $tipoPessoa, null, ['class' => 'form-control', 'placeholder' => 'Selecione o tipo de pessoa']) !!}
    {!! Form::error('tipo_pessoa', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_documento', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_documento', 'Tipo Documento', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_documento', $tipoDocumento, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Faixa Etária']) !!}
    {!! Form::error('tipo_documento', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('documento', $errors, 'col-md-3') !!}
    {!! Form::label('documento', 'CPF / CNPJ', ['class' => 'control-label']) !!}
    {!! Form::text('documento', null, ['class' => 'form-control']) !!}
    {!! Form::error('documento', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('orgao_expedidor', $errors, 'col-md-3') !!}
    {!! Form::label('orgao_expedidor', 'Órgão Expedidor *', ['class' => 'control-label', 'required' => 'required']) !!}
    {!! Form::text('orgao_expedidor', null, ['class' => 'form-control']) !!}
    {!! Form::error('orgao_expedidor', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('data_expedicao', $errors, 'col-md-3') !!}
    {!! Form::label('data_expedicao', 'Data Expedição *', ['class' => 'control-label', 'required' => 'required']) !!}
    {!! Form::text('data_expedicao', null, ['class' => 'form-control']) !!}
    {!! Form::error('data_expedicao', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('genero', $errors, 'col-md-3') !!}
    {!! Form::label('genero', 'Gênero', ['class' => 'control-label']) !!}
    {!! Form::select('genero', $genero, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Faixa Etária']) !!}
    {!! Form::error('genero', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('telefone', $errors, 'col-md-3') !!}
    {!! Form::label('telefone', 'Telefone', ['class' => 'control-label']) !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
    {!! Form::error('telefone', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('logradouro', $errors, 'col-md-10') !!}
    {!! Form::label('logradouro', 'Logradouro', ['class' => 'control-label']) !!}
    {!! Form::text('logradouro', null, ['class' => 'form-control']) !!}
    {!! Form::error('logradouro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('numero', $errors, 'col-md-2') !!}
    {!! Form::label('numero', 'Número', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
    {!! Form::error('numero', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">

    {!! Html::openFormGroup('complemento', $errors, 'col-md-6') !!}
    {!! Form::label('complemento', 'Complemento', ['class' => 'control-label']) !!}
    {!! Form::text('complemento', null, ['class' => 'form-control']) !!}
    {!! Form::error('complemento', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('bairro', $errors, 'col-md-4') !!}
    {!! Form::label('bairro', 'Bairro', ['class' => 'control-label']) !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
    {!! Form::error('bairro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cep', $errors, 'col-md-2') !!}
    {!! Form::label('cep', 'CEP', ['class' => 'control-label']) !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
    {!! Form::error('cep', $errors) !!}
    {!! Html::closeFormGroup() !!}

</div>

<div class="row">
    {!! Html::openFormGroup('cidade', $errors, 'col-md-10') !!}
    {!! Form::label('cidade', 'Cidade', ['class' => 'control-label']) !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('cidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('uf', $errors, 'col-md-2') !!}
    {!! Form::label('uf', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::text('uf', null, ['class' => 'form-control']) !!}
    {!! Form::error('uf', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>


<div class="row">
    {!! Html::openFormGroup('profissao_id', $errors, 'col-md-3') !!}
    {!! Form::label('profissao_id', 'Profissão', ['class' => 'control-label']) !!}
    {!! Form::select('profissao_id', $profissoes, null, ['class' => 'form-control']) !!}
    {!! Form::error('profissao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('tipo_telefone_id', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_telefone_id', 'Tipo Telefone', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_telefone_id', $tipoTelefone, null, ['class' => 'form-control', 'placeholder' => 'Selecione o tipo de telefone']) !!}
    {!! Form::error('tipo_telefone_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('escolaridade_id', $errors, 'col-md-3') !!}
    {!! Form::label('escolaridade_id', 'Escolaridade', ['class' => 'control-label']) !!}
    {!! Form::select('escolaridade_id', $escolaridade, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Escolaridade']) !!}
    {!! Form::error('escolaridade_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('faixaetaria_id', $errors, 'col-md-3') !!}
    {!! Form::label('faixaetaria_id', 'Faixa Etária', ['class' => 'control-label']) !!}
    {!! Form::select('faixaetaria_id', $faixaEtaria, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Faixa Etária']) !!}
    {!! Form::error('faixaetaria_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="form-group">
    <label for="ativo">
        {!! Form::checkbox('ativo', null, $ativo, ['class' => 'minimal']) !!}
        Ativo?
        {!! Form::error('ativo', $errors) !!}
    </label>
</div>

@push('js')
    <script>
        $(document).ready(function () {
            var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                    onKeyPress: function (val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };


            $('#telefone').mask(SPMaskBehavior, spOptions);

            var tipoPessoa = $("#tipo_pessoa");
            var valorPessoa = tipoPessoa.val();

            if (valorPessoa) {
                verificaTipoPessoa(valorPessoa);
            }

            tipoPessoa.change(function (event) {
                var valor = $(this).val();
                verificaTipoPessoa(valor);
            });

            function verificaTipoPessoa(valor) {
                if (valor === 'f') {
                    $('#cpf_cnpj').mask('000.000.000-00', {reverse: true});
                    $('#cpf_cnpj').prev().text('CPF')
                }
                if (valor === 'j') {
                    $('#cpf_cnpj').mask('00.000.000/0000-00', {reverse: true});
                    $('#cpf_cnpj').prev().text('CNPJ')
                }
                if (valor === '') {
                    $('#cpf_cnpj').unmask();
                    $('#cpf_cnpj').val('');
                    $('#cpf_cnpj').prev().text('CPF / CNPJ')
                }
            }

        });
    </script>
@endpush