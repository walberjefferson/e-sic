@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>{{ $dados->nome }}</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <th width="15%">Nome</th>
                        <td>{{ $dados->solicitante['name'] }}</td>
                    </tr>
                    <tr>
                        <th>Tipo Pessoa</th>
                        <td id="cpf_cnpj" data-title="{{ $dados->tipo_pessoa }}">{{ $dados->tipoPessoa() }}</td>
                    </tr>
                    <tr>
                        <th id="titulo_cnpj">CPF / CNPJ</th>
                        <td>{{ $dados->documento }}</td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $dados->solicitante['email'] }}</td>
                    </tr>
                    <tr>
                        <th>Telefone</th>
                        <td>{{ $dados->telefone }}</td>
                    </tr>
                    <tr>
                        <th>Endereço</th>
                        <td>{{ $dados->logradouro }}, {{ $dados->numero }} - {{ $dados->complemento }}</td>
                    </tr>
                    <tr>
                        <th>Bairro</th>
                        <td>{{ $dados->bairro }}</td>
                    </tr>
                    <tr>
                        <th>Profissão</th>
                        <td>{{ $dados->profissao['nome'] }}</td>
                    </tr>

                    <tr>
                        <th>Tipo Telefone</th>
                        <td>{{ $dados->tipoTelefone['nome'] }}</td>
                    </tr>

                    <tr>
                        <th>Escolaridade</th>
                        <td>{{ $dados->escolaridade['nome'] }}</td>
                    </tr>

                    <tr>
                        <th>Faixa Etária</th>
                        <td>{{ $dados->faixaEtaria['nome'] }}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-footer">
            <div class="btn-group">
                <a href="{{ route($rota->index) }}" class="btn btn-flat btn-primary"> <i class="fa fa-arrow-left"></i> Voltar</a>
                <a href="{{ route($rota->edit, $dados->id) }}" class="btn btn-flat bg-olive"> <i class="fa fa-pencil"></i> Editar</a>
            </div>
        </div>
    </div>
@stop

@section('css')
    <style>
        table > tbody > tr > th {
            text-align: right;

        }

        table > tbody > tr > th:after {
            content: ':';
        }
    </style>

@stop

@section('js')
    <script>
        $(document).ready(function(){
            var tipo = $("#cpf_cnpj").data('title');
            if(tipo == 'f'){
                $('#titulo_cnpj').text("CPF");
            }
            if(tipo == 'j'){
                $('#titulo_cnpj').text("CNPJ");
            }
        });
    </script>
@stop