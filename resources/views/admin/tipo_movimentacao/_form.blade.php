<div class="row">
    {!! Html::openFormGroup('nome', $errors, 'col-md-8') !!}
    {!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
    {!! Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Digite o nome da situação']) !!}
    {!! Form::error('nome', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('abreviacoes', $errors, 'col-md-4') !!}
    {!! Form::label('abreviacoes', 'Abreviação', ['class' => 'control-label']) !!}
    {!! Form::text('abreviacoes', null, ['class' => 'form-control', 'placeholder' => 'Digite a abreviação']) !!}
    {!! Form::error('abreviacoes', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('despacho_padrao', $errors) !!}
{!! Form::label('despacho_padrao', 'Despacho Padrão', ['class' => 'control-label']) !!}
{!! Form::textarea('despacho_padrao', null, ['class' => 'form-control', 'placeholder' => 'Digite o texto para o despacho padrão']) !!}
{!! Form::error('despacho_padrao', $errors) !!}
{!! Html::closeFormGroup() !!}