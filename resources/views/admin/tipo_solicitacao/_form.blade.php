{!! Html::openFormGroup('nome', $errors) !!}
{!! Form::label('nome', 'Nome', ['class' => 'control-label']) !!}
{!! Form::text('nome', null, ['class' => 'form-control']) !!}
{!! Form::error('nome', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('instancia', $errors) !!}
{!! Form::label('instancia', 'Instância', ['class' => 'control-label']) !!}
{!! Form::text('instancia', null, ['class' => 'form-control']) !!}
{!! Form::error('instancia', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('tipo_seguinte_id', $errors) !!}
{!! Form::label('tipo_seguinte_id', 'Tipo de Solicitação Seguinte', ['class' => 'control-label']) !!}
{!! Form::select('tipo_seguinte_id', $tipo_solicitacao, null, ['class' => 'form-control', 'placeholder' => 'Selecione o tipo de solicitação']) !!}
{!! Form::error('tipo_seguinte_id', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="form-group">
    <div class="">
        <label for="ativo">
            {!! Form::checkbox('ativo', null, null, ['class' => 'minimal']) !!}
            Ativo?
            {!! Form::error('ativo', $errors) !!}
        </label>
    </div>
</div>

