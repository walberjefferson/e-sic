{!! Html::openFormGroup('name', $errors) !!}
{!! Form::label('name', 'Nome', ['class' => 'control-label']) !!}
{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Digite o nome do perfil']) !!}
{!! Form::error('name', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('email', $errors) !!}
{!! Form::label('email', 'E-mail', ['class' => 'control-label']) !!}
{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Digite o e-mail do perfil']) !!}
{!! Form::error('email', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="row">
    {!! Html::openFormGroup('password', $errors, 'col-md-6') !!}
    {!! Form::label('password', 'Senha', ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Digite uma senha']) !!}
    {!! Form::error('password', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('confirm_password', $errors, 'col-md-6') !!}
    {!! Form::label('confirm_password', 'Senha', ['class' => 'control-label']) !!}
    {!! Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => 'Repita a senha']) !!}
    {!! Form::error('confirm_password', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>
@can('user-admin')
    {!! Html::openFormGroup('perfil_id', $errors) !!}
    {!! Form::label('perfil_id', 'Perfil', ['class' => 'control-label']) !!}
    {!! Form::select('perfil_id', $perfil, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Perfil']) !!}
    {!! Form::error('perfil_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('orgaos.*', $errors) !!}
    {!! Form::label('orgaos[]', 'Orgãos', ['class' => 'control-label']) !!}
    {!! Form::select('orgaos[]', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão', 'multiple' => true]) !!}
    {!! Form::error('orgaos.*', $errors) !!}
    {!! Html::closeFormGroup() !!}

    <div class="">
        <label for="ativo">
            {!! Form::checkbox('ativo', null, null, ['class' => 'minimal']) !!}
            Ativo?
            {!! Form::error('ativo', $errors) !!}
        </label>
    </div>
@endcan