@extends('adminlte::page')

@section('title', $view->titulo)

@section('content_header')
    <h1>{{ $view->titulo }}
        <small>Listagem</small>
    </h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools">
                <div class="btn-box-tool">
                    <a href="{{ route($rota->create) }}" class="btn bg-navy btn-flat"><i class="fa fa-plus"></i>
                        Adicionar</a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Perfil</th>
                        <th>Orgãos</th>
                        <th width="15%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dados as $d)
                        <tr @if(!$d->ativo) class="bg-danger" @endif>
                            <td>{{$d->id}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->email}}</td>
                            <td>{{$d->perfil['nome']}}</td>
                            <td>
                                @foreach($d->orgaos as $orgao)
                                    <span class="label label-default">{{ $orgao->nome }}</span>
                                @endforeach
                            </td>
                            <td>
                                <div class="btn-group btn-group-xs btn-group-justified">
                                    <a href="{{ route($rota->edit, $d->id) }}" class="btn bg-olive btn-flat"><i
                                                class="fa fa-pencil"></i></a>
                                    <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                    <a href="{{ route($rota->destroy, $d->id) }}"
                                       class="btn btn-danger btn-flat"
                                       onclick="if(confirm('Deseja realmente excluir?')) {event.preventDefault(); document.getElementById('{{$deleteForm}}').submit(); }else{ return false; }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    {!! Form::open(['route' => [$rota->destroy, $d->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')

@stop