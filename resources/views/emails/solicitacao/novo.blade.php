@component('mail::message')

# Uma nova solicitação foi iniciada.

## Dados da solicitação

**Nº do Protocolo: ** {{ $solicitacao->numero_protocolo }}

**Solicitante: ** {{ $solicitacao->solicitante['name'] }}

**Órgão: ** {{ $solicitacao->orgao->nome }}

**Retorno: ** {{ $solicitacao->tipo_retorno->nome }}

**Previsão de Resposta: ** {{ $solicitacao->data_previsao_resposta }}

**Descrição: ** {{ $solicitacao->descricao_solicitacao }}

***

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
