@component('mail::message')
# Sua solicitação mudou de status!

### Status atual:  * {{ $status }} *

Obrigado, {{ config('app.name') }}
@endcomponent
