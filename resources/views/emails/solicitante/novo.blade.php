@component('mail::message')
# Olá {{ $nome }}

### Seu usuário é: *{{ $email }}*
### Sua senha é: *{{ $senha }}*

#### Guarde estas informações, pois elas são os dados que você pode usar para acessar suas solicitações.

___

@component('mail::button', ['url' => env('APP_URL')])
Acessar
@endcomponent

Obrigado, {{ config('app.name') }}
@endcomponent
