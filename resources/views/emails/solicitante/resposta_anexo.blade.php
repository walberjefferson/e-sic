@component('mail::message')

## {{ $status }}

### Mensagem:
{{ $resposta->descricao }}

***

Obrigado, {{ config('app.name') }}
@endcomponent
