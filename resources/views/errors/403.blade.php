<!DOCTYPE html>
<html>
    <head>
        <title>Algo de errado aconteceu.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #409b62;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
            a{
                color: #409b62;
                text-decoration: none;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Erro 403</div>
                <h2>{{ $exception->getMessage() }}</h2>
                <p><a href="{{ url('/') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a></p>
            </div>
        </div>
    </body>
</html>
