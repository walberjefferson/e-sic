@extends('layouts.'. env('TEMA', 'app'))

@section('title', config('app.name', 'E-sic'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form id="form_charts" class="form-horizontal">
                    <div class="form-group">
                        <label for="exercicio" class="col-md-5 control-label">Selecione o exercício:</label>
                        <div class="col-md-7">
                            <select name="ano" id="exercicio" class="form-control">
                                @foreach($lista_anos as $ano)
                                    <option value="{{ $ano }}"
                                            @if($ano == date("Y")) selected @endif>{{ $ano }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <p id="carregamento" class="text-center" style="position: absolute; top: 300px; left: 45%;">
            <i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
        </p>
        <div class="row">
            <div class="col-md-12">
                <div id="columnchart_material" class="invisivel" style="width: 100%; height: 400px;"></div>
            </div>
        </div>

        <div id="estatisticas_tabela" class="invisivel">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="page-header">
                        <h1>Resumo Anual</h1>
                    </div>
                </div>
            </div>

            <div id="recebida" class="coluna">
                <p>Pendentes</p>
                <h2>0</h2>
            </div>
            <div id="negada" class="coluna">
                <p>Negadas</p>
                <h2>0</h2>
            </div>
            <div id="respondida" class="coluna">
                <p>Respondidas</p>
                <h2>0</h2>
            </div>
        </div>

    </div>
@stop

@push('css')
    <style>
        .invisivel {
            opacity: 0;
            transition: 1s;
        }

        .visivel {
            opacity: 1;
            transition: 1s;
        }
    </style>
@endpush

@push('js')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages': ['bar']});

        function drawChart(dados) {
            var data = google.visualization.arrayToDataTable(dados);
            var options = {
                chart: {
                    title: 'Estatísticas das Solicitações',
                    subtitle: 'Estatísticas referentes ao exercício de ' + dados[0][0]
                },
                animation: {
                    duration: 1000,
                    easing: 'inAndOut',
                    startup: true
                },
                colors: ['rgb(66, 133, 244)', 'rgb(219, 68, 55)', 'rgb(244, 180, 0)'],
                hAxis: {
                    title: 'Estatísticas Anual',
                    titleTextStyle: {
                        fontSize: 18,
                        bold: true
                    },

                },
                legend: {
                    alignment: 'center'
                },
                vAxis: {
                    title: dados[0][0],
                    titleTextStyle: {
                        fontSize: 14,
                        bold: true
                    }
                }
            };
            var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>

    <script>
        (function () {
            function carregamento() {
                setTimeout(function () {
                    $('#carregamento').fadeToggle();
                    var ano = $("#exercicio").val();
                    $.get('/estatisticas', {exercicio: ano}, function (data) {
                        $('#columnchart_material, #estatisticas_tabela').removeClass('invisivel').addClass('visivel');
                        google.charts.setOnLoadCallback(drawChart(data.status));
                        $("#negada h2").text(data.total_lista.negadas);
                        $("#recebida h2").text(data.total_lista.pendentes);
                        $("#respondida h2").text(data.total_lista.respondidas);
                    });
                }, 500);
            }

            carregamento();
            $("#exercicio").change(function () {
                $('#carregamento').fadeToggle();
                $('#columnchart_material, #estatisticas_tabela').removeClass('visivel').addClass('invisivel');
                carregamento();
            })
        })();
    </script>
@endpush