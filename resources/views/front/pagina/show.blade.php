@extends('layouts.'. env('TEMA', 'app'))

@section('title', config('app.name', 'E-sic') . ' - ' . $pagina->nome)

@section('content_header')

@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h2>{{ $pagina->nome }}</h2>
                {!! $pagina->conteudo !!}
            </div>
        </div>
    </div>
@stop