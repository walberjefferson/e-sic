<h4 class="title-section">Tipo de Solicitante</h4>
<hr>
<div class="form-group">
    <label for="" style="margin-right: 20px">{{ Form::radio('tipo_pessoa', 'f', true) }} Pessoa Física</label>
    <label for="">{{ Form::radio('tipo_pessoa', 'j') }} Pessoa Jurídica</label>
</div>

@if(false)
    <h4 class="title-section">Identificação</h4>
    <hr>

    <div class="row">
        {!! Html::openFormGroup('tipo_documento', $errors, 'col-md-3') !!}
        {!! Form::label('tipo_documento', 'Tipo de Documento *', ['class' => 'control-label']) !!}
        {!! Form::select('tipo_documento', $tipoDocumento, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Documento', 'required' => 'required']) !!}
        {!! Form::error('tipo_documento', $errors) !!}
        {!! Html::closeFormGroup() !!}

        {!! Html::openFormGroup('documento', $errors, 'col-md-3') !!}
        {!! Form::label('documento', 'Documento de Identificação *', ['class' => 'control-label']) !!}
        {!! Form::text('documento', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! Form::error('documento', $errors) !!}
        {!! Html::closeFormGroup() !!}
        <div id="no-cpf">
            {!! Html::openFormGroup('orgao_expedidor', $errors, 'col-md-3') !!}
            {!! Form::label('orgao_expedidor', 'Órgão Expedidor *', ['class' => 'control-label', 'required' => 'required']) !!}
            {!! Form::text('orgao_expedidor', null, ['class' => 'form-control']) !!}
            {!! Form::error('orgao_expedidor', $errors) !!}
            {!! Html::closeFormGroup() !!}

            {!! Html::openFormGroup('data_expedicao', $errors, 'col-md-3') !!}
            {!! Form::label('data_expedicao', 'Data Expedição *', ['class' => 'control-label', 'required' => 'required']) !!}
            {!! Form::text('data_expedicao', null, ['class' => 'form-control']) !!}
            {!! Form::error('data_expedicao', $errors) !!}
            {!! Html::closeFormGroup() !!}
        </div>
    </div>
@endif

<h4 class="title-section">Dados do Solicitante</h4>
<hr>
{!! Html::openFormGroup('name', $errors) !!}
{!! Form::label('name', 'Nome *', ['class' => 'control-label']) !!}
{!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
{!! Form::error('name', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('email', $errors) !!}
{!! Form::label('email', 'E-mail', ['class' => 'control-label']) !!}
{!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
{!! Form::error('email', $errors) !!}
{!! Html::closeFormGroup() !!}

<div class="row">
    {!! Html::openFormGroup('cep', $errors, 'col-md-2') !!}
    {!! Form::label('cep', 'CEP', ['class' => 'control-label']) !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
    {!! Form::error('cep', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('logradouro', $errors, 'col-md-8') !!}
    {!! Form::label('logradouro', 'Logradouro', ['class' => 'control-label']) !!}
    {!! Form::text('logradouro', null, ['class' => 'form-control']) !!}
    {!! Form::error('logradouro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('numero', $errors, 'col-md-2') !!}
    {!! Form::label('numero', 'Número', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
    {!! Form::error('numero', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('bairro', $errors, 'col-md-4') !!}
    {!! Form::label('bairro', 'Bairro', ['class' => 'control-label']) !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
    {!! Form::error('bairro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cidade', $errors, 'col-md-5') !!}
    {!! Form::label('cidade', 'Cidade', ['class' => 'control-label']) !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('cidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('uf', $errors, 'col-md-3') !!}
    {!! Form::label('uf', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::select('uf', [], null, ['class' => 'form-control']) !!}
    {!! Form::error('uf', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('complemento', $errors) !!}
{!! Form::label('complemento', 'Complemento', ['class' => 'control-label']) !!}
{!! Form::text('complemento', null, ['class' => 'form-control']) !!}
{!! Form::error('complemento', $errors) !!}
{!! Html::closeFormGroup() !!}


<h4 class="title-section">Dados da Solicitação</h4>
<hr>

{!! Html::openFormGroup('orgao_id', $errors) !!}
{!! Form::label('orgao_id', 'Orgão *', ['class' => 'control-label']) !!}
{!! Form::select('orgao_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão', 'required' => 'required']) !!}
{!! Form::error('orgao_id', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('tipo_retorno_id', $errors) !!}
{!! Form::label('tipo_retorno_id', 'Tipo de Retorno *', ['class' => 'control-label']) !!}
{!! Form::select('tipo_retorno_id', $tipoRetorno, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Retorno', 'required' => 'required']) !!}
{!! Form::error('tipo_retorno_id', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('descricao_solicitacao', $errors) !!}
{!! Form::label('descricao_solicitacao', 'Solicitação *', ['class' => 'control-label']) !!}
{!! Form::textarea('descricao_solicitacao', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Descreva sua Solicitação']) !!}
{!! Form::error('descricao_solicitacao', $errors) !!}
{!! Html::closeFormGroup() !!}

{{--{!! Html::openFormGroup('anexo', $errors) !!}--}}
{{--{!! Form::label('anexo', 'Anexo', ['class' => 'control-label']) !!}--}}
{{--{!! Form::file('anexo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Descreva sua Solicitação']) !!}--}}
{{--{!! Form::error('anexo', $errors) !!}--}}
{{--{!! Html::closeFormGroup() !!}--}}