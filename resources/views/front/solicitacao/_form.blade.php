{!! Html::openFormGroup('orgao_id', $errors) !!}
{!! Form::label('orgao_id', 'Orgão *', ['class' => 'control-label']) !!}
{!! Form::select('orgao_id', $orgaos, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Orgão', 'required' => 'required']) !!}
{!! Form::error('orgao_id', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('tipo_retorno_id', $errors) !!}
{!! Form::label('tipo_retorno_id', 'Tipo de Retorno *', ['class' => 'control-label']) !!}
{!! Form::select('tipo_retorno_id', $tipoRetorno, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Retorno', 'required' => 'required']) !!}
{!! Form::error('tipo_retorno_id', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('descricao_solicitacao', $errors) !!}
{!! Form::label('descricao_solicitacao', 'Solicitação *', ['class' => 'control-label']) !!}
{!! Form::textarea('descricao_solicitacao', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Descreva sua Solicitação']) !!}
{!! Form::error('descricao_solicitacao', $errors) !!}
{!! Html::closeFormGroup() !!}

{{--{!! Html::openFormGroup('anexo', $errors) !!}--}}
{{--{!! Form::label('anexo', 'Anexo', ['class' => 'control-label']) !!}--}}
{{--{!! Form::file('anexo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Descreva sua Solicitação']) !!}--}}
{{--{!! Form::error('anexo', $errors) !!}--}}
{{--{!! Html::closeFormGroup() !!}--}}