@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Solicitações')

@section('content')

    <div class="container">
        <div id="login">
            <div class="row main-home no-padding no-margin">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Consultar Solicitação</div>
                        <div class="panel-body form-login-home">
                            <form method="post" action="{{ route('front.solicitacao.consulta') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="protocolo">Número do Protocolo</label>
                                    <input type="text" class="form-control" name="numero_protocolo" id="protocolo"
                                           placeholder="Número do Protocolo">
                                </div>
                                <button type="submit" class="btn btn-default pull-right">Consultar</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Faça seu Pedido</div>
                        <div class="panel-body form-login-home">
                            <form method="post" action="{{ route('front.solicitante.login') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" name="email" id="email"
                                           placeholder="Usuário">
                                </div>
                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input type="password" class="form-control" id="password" placeholder="Senha"
                                           name="password">
                                </div>
                                <button type="submit" class="btn btn-default pull-right">Acessar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        .table {
            margin-bottom: 0;
        }

        .list-group-item {
            padding: 0 !important;
            border-bottom: 4px solid #337ab7 !important;
        }

        table > tbody > tr > th {
            text-align: right;
            width: 25%;
        }
    </style>
@endpush