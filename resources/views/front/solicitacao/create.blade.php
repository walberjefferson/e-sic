@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Novo Cadastro')

@section('content')
    <div class="container">

        <div class="col-sm-8 no-padding">
            <div class="col-sm-12 no-padding">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <b>Faça sua Solicitação</b>
                    </div>
                    <div class="panel-body" style="padding: 5px;">
                        <div class="col-sm-12" style="padding-bottom:20px;">
                            {!! Form::open(['route' => 'front.solicitacao.store', 'files' => true]) !!}
                            @include('front.solicitacao._form')
                            {{ Form::submit('Enviar', ['class'=> 'btn btn-primary pull-right']) }}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 no-padding-right">
            <div class="panel panel-primary">
                <div class="panel-heading"><b>Atenção!</b></div>
                <div class="panel-body">
                    <p style="text-align: justify;">
                        Faça seu pedido de informação de maneira específica e de forma clara e precisa.
                        Não serão atendidos pedidos de informação genérico, despropocionais ou desarrazoados
                        ou que exijam trabalhos adicionais de análise, interpretação ou consolidação de dados e
                        informações, serviço de produção ou tratamento de dados (Art. 9. III e Art. 10, I, II e III
                        do Decreto n° 26.320/2013.
                    </p>

                    <span>
                        <hr>
                        <strong>Prazo final coincidindo com final de semana ou feriado</strong>
                        <hr>
                    </span>
                    <p style="text-align: justify;">
                        Quando o prazo final para responder a solicitação coincidir com final de semana ou
                        feriado previsto em portaria do Ministério do Planejamento, Orçamento e Gestão e em
                        portaria publicada pelo Governo Estadual de Alagoas, ele será postergado para o próximo
                        dia útil. Por isso, o prazo para envio de resposta pode não ser exatamente o de 20 ou,
                        caso haja prorrogação, 30 dias corridos.
                    </p>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            var tipoDocumento = $("#tipo_documento").val();
            if (tipoDocumento == 1) {
                $("#no-cpf").hide();
            }
            var tipo_selecionado = $("input[name='tipo_pessoa']:checked").val();
            mostraTipo(tipo_selecionado);

            $("input[name='tipo_pessoa']").change(function () {
                var selecionado = $(this).val();
                mostraTipo(selecionado)
            });

            $("#tipo_documento").change(function () {
                var selecionado = $(this).val();
                if (selecionado == 1) {
                    $("#no-cpf").hide();
                } else {
                    $("#no-cpf").show()
                }
            });

            function mostraTipo(tipo) {
                var nome = $('label[for=name]');
                var dataNascimento = $('label[for=data_nascimento]');
                var genero = $('label[for=genero]');
                var documento = $('label[for=documento]');
                var tipo_documento = $('label[for=tipo_documento]');
                if (tipo == 'f') {
                    nome.text('Nome *');
                    dataNascimento.text('Data Nascimento');
                    documento.text('Documento de Identificação *');
                    documento.parent().removeClass('col-md-6').addClass('col-md-3');
                    $("#no-cpf").show();
                    nome.parent().removeClass('col-md-9').addClass('col-md-6');
                    tipo_documento.parent().show();
                    genero.parent().show();
                }
                if (tipo == 'j') {
                    nome.text('Razão Social *');
                    dataNascimento.text('Data Fundação');
                    documento.text("CNPJ *");
                    nome.parent().removeClass('col-md-6').addClass('col-md-9');
                    documento.parent().removeClass('col-md-3').addClass('col-md-6');
                    $("#no-cpf").hide();
                    tipo_documento.parent().hide();
                    genero.parent().hide();
                }
            }
        });
    </script>
@endpush