@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Solicitações')

@section('content')

    <div class="container">

        <div class="col-sm-12 no-padding">
            <div class="panel panel-default hidden">
                <div class="panel-body">
                    <form action="" method="GET" name="pesquisa">
                        <div class="col-sm-12">
                            <div class="col-sm-6  col-xs-12">
                                <div class="form-group input-group" style="margin-bottom: 10px;">
                                    <span class="input-group-addon"><i class="fa fa-search fa-fw"></i></span>
                                    <input class="form-control input-sm" id="id_pesquisa" maxlength="200"
                                           name="pesquisa" placeholder="Pesquisar..." type="text">
                                </div>
                            </div>
                            <div class="col-sm-6  col-xs-12 ">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <select class="form-control input-sm" id="id_status" name="status">
                                        <option value="" selected="selected">Todos</option>
                                        <option value="E">Enviado</option>
                                        <option value="A">Aberto</option>
                                        <option value="P">Prorrogado</option>
                                        <option value="AP">Aberto e Prorrogado</option>
                                        <option value="T">Em tramitação</option>
                                        <option value="R">Solicitação Atendida</option>
                                        <option value="N">Solicitação Negada</option>
                                        <option value="AN">Solicitação Não Atendida</option>
                                        <option value="PV">Prazo Vencido</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-3  col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <div class="input-group date">
                                        <input class="form-control input-sm date-picker" data-date-format="dd/mm/yyyy"
                                               id="id_data_inicial" name="data_inicial" placeholder="Data Inicial"
                                               type="text" value="20/10/2017">
                                        <div class="input-group-addon" id="data_inicial">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <div class="input-group date">
                                        <input class="form-control input-sm date-picker" data-date-format="dd/mm/yyyy"
                                               id="id_data_final" name="data_final" placeholder="Data Final" type="text"
                                               value="19/12/2017">
                                        <div class="input-group-addon" id="data_final">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <a href="/solicitacao/listar/" class="btn btn-danger btn-sm btn-block">
                                        Limpar
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group" style="margin-bottom: 10px;">
                                    <a href="/solicitacao/novo/" class="btn btn-default btn-sm btn-block">
                                        Nova Solicitação
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8 no-padding">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Minhas Solicitações
                </div>

                <ul class="list-group">
                    @forelse($dados as $d)
                        <?php
                        if ($d->situacao_id == 2):
                            $class = 'label-success';
                        elseif ($d->situacao_id == 3):
                            $class = 'label-danger';
                        else:
                            $class = 'label-primary';
                        endif;
                        ?>
                        <li class="list-group-item">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th width="25%">Nº Solicitação:</th>
                                    <td>{{ $d->numero_protocolo }}</td>
                                </tr>

                                <tr>
                                    <th>Data da Solicitação:</th>
                                    <td>{{ $d->data_solicitacao }}</td>
                                </tr>

                                <tr>
                                    <th>Previsão de Resposta:</th>
                                    <td>{{ $d->data_previsao_resposta }}</td>
                                </tr>
                                <tr>
                                    <th>Situação:</th>
                                    <td>
                                        <span class="label {{ $class }}">{{ $d->situacao['nome'] }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Anexos:</th>
                                    <td>{{ ($d->anexos->count()) ? 'Sim' : 'Não' }}</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2" class="text-center">
                                        <a href="{{ route('front.solicitacao.show', $d->id) }}"><i
                                                    class="fa fa-eye"></i> Visualizar</a>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </li>
                    @empty
                        <li class="list-group-item">
                            Você ainda não realizou solicitações.
                            <a href="{{ route('front.solicitacao.create') }}">
                                <span style="color: #c0392b; font-size: 2rem"> <b>Faça uma solicitação agora.</b> </span>
                            </a>
                        </li>
                    @endforelse
                </ul>
                @if(empty($dados))
                    <div class="panel-footer">
                        <div class="text-center">Nenhum resultado encontrado.</div>
                    </div>
                @endif
            </div>
            <nav class="text-center">{{ $dados }}</nav>
        </div>
        <div class="col-sm-4" style="padding-right:0px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Legenda - Situação da Solicitação
                </div>
                <div class="panel-body">
                    <p>
                        <span class="label label-default">Enviado</span>
                        Solicitação entregue ao órgão mas <strong>não visualizada</strong> pelo gestor da SIC.<br>
                    </p>
                    <p>
                        <span class="label label-info">Aberta</span>
                        Solicitação entregue ao órgão e visualizada pelo gestor da SIC.
                    </p>
                    <p>
                        <span class="label label-primary">Prorrogado</span>
                        Solicitação prorrogada pelo gestor da SIC.
                    </p>
                    <p>
                        <span class="label label-primary">Em tramitação</span>
                        Solicitação enviada para outro órgão e ainda <strong>não visualizada</strong> pelo órgão de
                        destino. Consulte o motivo da movimentação em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-danger">Negado</span>
                        Pedido negado pelo órgão competente. Consulte o motivo em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-success">Solicitação Respondida</span>
                        Pedido respondido pelo órgão competente. Consulte a resposta em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-danger">Diligência Aberta</span> &nbsp;
                        Foi aberta uma diligência para responder a um recurso. Fique atento aos novos prazos.
                    </p>
                    <p>
                        <span class="label label-info">Diligência - Informações Complementares</span>
                        Foi aberta uma diligência para responder a um recurso. O órgão destino complementou as
                        informações enviadas na solicitação inicial.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        .table {
            margin-bottom: 0;
        }

        .list-group-item {
            padding: 0 !important;
            border-bottom: 4px solid #337ab7 !important;
        }

        table > tbody > tr > th {
            text-align: right;
            width: 25%;
        }
    </style>
@endpush