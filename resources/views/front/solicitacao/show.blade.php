@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Visualização da Solicitação')

@section('content')

    <div class="container">
        <div class="col-sm-8 no-padding">
            <div class="col-sm-12 no-padding">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Detalhes da Solicitação
                    </div>

                    <ul class="list-group">

                        <?php
                        if ($dados->situacao_id == 2):
                            $class = 'label-success';
                        elseif ($dados->situacao_id == 3):
                            $class = 'label-danger';
                        else:
                            $class = 'label-primary';
                        endif;
                        ?>
                        <li class="list-group-item">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th width="25%">Nº Solicitação:</th>
                                    <td>{{ $dados->numero_protocolo }}</td>
                                </tr>
                                <tr>
                                    <th>Data da Solicitação:</th>
                                    <td>{{ $dados->data_solicitacao }}</td>
                                </tr>
                                <tr>
                                    <th>Previsão de Resposta:</th>
                                    <td>{{ $dados->data_previsao_resposta }}</td>
                                </tr>
                                <tr>
                                    <th>Situação:</th>
                                    <td><span class="label {{ $class }}">{{ $dados->situacao['nome'] }}</span></td>
                                </tr>
                                <tr>
                                    <th>Descrição:</th>
                                    <td>{{ $dados->descricao_solicitacao }}</td>
                                </tr>
                                <tr>
                                    <th>Tipo Solicitação:</th>
                                    <td>{{ $dados->tipo_solicitacao['nome'] }}</td>
                                </tr>
                                <tr>
                                    <th>Anexo:</th>
                                    <td>
                                        @forelse($dados->anexos as $anexo)
                                            <a href="{{ asset('storage/solicitacao/' . $anexo->url) }}">{{ $anexo->nome }}</a>
                                        @empty
                                            <p>Nenhum anexo</p>
                                        @endforelse
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>

            @if($negada and !$existe)
                <div class="row">
                    <div class="col-sm-12">
                        <a href="#" data-toggle="modal" data-target="#recurso" class="btn btn-danger btn-lg btn-block">Abrir
                            Recurso</a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="recurso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Abrir Recurso da Solicitação</h4>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(['route' => 'front.solicitacao.recurso', 'files' => true, 'id' => 'form_recurso']) !!}
                                    <input type="hidden" name="solicitacao_origem_id" value="{{ $dados->id }}">
                                    @include('front.solicitacao._form')
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" form="form_recurso" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($existe)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <p class="text-center"><strong>Solicitação já tem recurso aberto!</strong></p>
                        </div>
                    </div>
                </div>
            @endif

            @if($respostas->count())
                <div class="col-sm-12 no-padding" style="margin-top: 20px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Respostas</div>
                        <table id="respostas" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th width="5%">Data</th>
                                <th>Descrição</th>
                                <th width="15%">Anexos</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($respostas as $resposta)
                                <tr>
                                    <td>{{ $resposta->data }}</td>
                                    <td>{!! $resposta->descricao !!}</td>
                                    <td class="text-center">
                                    @if($resposta->anexos->count())
                                        <!-- Split button -->
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-success btn-xs dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="fa fa-download"></i> Download <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    @foreach($resposta->anexos as $anexo)
                                                        <li>
                                                            <a href="{{ asset('storage/resposta/' . $anexo->url) }}">Anexo {{ $loop->iteration }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @else
                                            <span class="label label-warning">Sem anexo</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-sm-4" style="padding-right:0px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Legenda - Situação da Solicitação
                </div>
                <div class="panel-body">
                    <p>
                        <span class="label label-default">Enviado</span>
                        Solicitação entregue ao órgão mas <strong>não visualizada</strong> pelo gestor da SIC.<br>
                    </p>
                    <p>
                        <span class="label label-info">Aberta</span>
                        Solicitação entregue ao órgão e visualizada pelo gestor da SIC.
                    </p>
                    <p>
                        <span class="label label-primary">Prorrogado</span>
                        Solicitação prorrogada pelo gestor da SIC.
                    </p>
                    <p>
                        <span class="label label-primary">Em tramitação</span>
                        Solicitação enviada para outro órgão e ainda <strong>não visualizada</strong> pelo órgão de
                        destino. Consulte o motivo da movimentação em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-danger">Negado</span>
                        Pedido negado pelo órgão competente. Consulte o motivo em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-success">Solicitação Respondida</span>
                        Pedido respondido pelo órgão competente. Consulte a resposta em "Ver detalhes".
                    </p>
                    <p>
                        <span class="label label-danger">Diligência Aberta</span>
                        Foi aberta uma diligência para responder a um recurso. Fique atento aos novos prazos.
                    </p>
                    <p>
                        <span class="label label-info">Diligência - Informações Complementares</span>
                        Foi aberta uma diligência para responder a um recurso. O órgão destino complementou as
                        informações enviadas na solicitação inicial.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        .table {
            margin-bottom: 0;
        }

        .list-group-item {
            padding: 0 !important;
            border-bottom: 4px solid #337ab7 !important;
        }

        table > tbody > tr > th {
            text-align: right;
            width: 25%;
        }
    </style>
@endpush