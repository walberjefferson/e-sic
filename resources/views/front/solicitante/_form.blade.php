<h4 class="title-section">Tipo de Solicitante</h4>
<hr>
<div class="form-group">
    <label for="" style="margin-right: 20px">{{ Form::radio('tipo_pessoa', 'f', true) }} Pessoa Física</label>
    <label for="">{{ Form::radio('tipo_pessoa', 'j') }} Pessoa Jurídica</label>
</div>

<h4 class="title-section">Dados de Acesso</h4>
<hr>
<div class="row">
    {!! Html::openFormGroup('email', $errors, 'col-md-12') !!}
    {!! Form::label('email', 'E-mail (Login) *', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! Form::error('email', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('password', $errors, 'col-md-6') !!}
    {!! Form::label('password', 'Senha *', ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
    {!! Form::error('password', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('password_confirmation', $errors, 'col-md-6') !!}
    {!! Form::label('password_confirmation', 'Confirmação de Senha *', ['class' => 'control-label']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
    {!! Form::error('password_confirmation', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

@if(false)
    <h4 class="title-section">Identificação</h4>
    <hr>

    <div class="row">
        {!! Html::openFormGroup('tipo_documento', $errors, 'col-md-3') !!}
        {!! Form::label('tipo_documento', 'Tipo de Documento', ['class' => 'control-label']) !!}
        {!! Form::select('tipo_documento', $tipoDocumento, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Documento']) !!}
        {!! Form::error('tipo_documento', $errors) !!}
        {!! Html::closeFormGroup() !!}

        {!! Html::openFormGroup('documento', $errors, 'col-md-3') !!}
        {!! Form::label('documento', 'Documento de Identificação', ['class' => 'control-label']) !!}
        {!! Form::text('documento', null, ['class' => 'form-control']) !!}
        {!! Form::error('documento', $errors) !!}
        {!! Html::closeFormGroup() !!}
        <div id="no-cpf">
            {!! Html::openFormGroup('orgao_expedidor', $errors, 'col-md-3') !!}
            {!! Form::label('orgao_expedidor', 'Órgão Expedidor', ['class' => 'control-label']) !!}
            {!! Form::text('orgao_expedidor', null, ['class' => 'form-control']) !!}
            {!! Form::error('orgao_expedidor', $errors) !!}
            {!! Html::closeFormGroup() !!}

            {!! Html::openFormGroup('data_expedicao', $errors, 'col-md-3') !!}
            {!! Form::label('data_expedicao', 'Data Expedição', ['class' => 'control-label']) !!}
            {!! Form::text('data_expedicao', null, ['class' => 'form-control']) !!}
            {!! Form::error('data_expedicao', $errors) !!}
            {!! Html::closeFormGroup() !!}
        </div>
    </div>
@endif


<h4 class="title-section">Dados Pessoais</h4>
<hr>
<div class="row">
    {!! Html::openFormGroup('name', $errors, 'col-md-9') !!}
    {!! Form::label('name', 'Nome *', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    {!! Form::error('name', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('data_nascimento', $errors, 'col-md-3') !!}
    {!! Form::label('data_nascimento', 'Data de Nascimento', ['class' => 'control-label']) !!}
    {!! Form::text('data_nascimento', null, ['class' => 'form-control']) !!}
    {!! Form::error('data_nascimento', $errors) !!}
    {!! Html::closeFormGroup() !!}

{{--    {!! Html::openFormGroup('genero', $errors, 'col-md-3') !!}--}}
{{--    {!! Form::label('genero', 'Gênero *', ['class' => 'control-label']) !!}--}}
{{--    {!! Form::select('genero', $genero, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Gênero']) !!}--}}
{{--    {!! Form::error('genero', $errors) !!}--}}
{{--    {!! Html::closeFormGroup() !!}--}}
</div>
<div class="row">
    {!! Html::openFormGroup('tipo_telefone_id', $errors, 'col-md-3') !!}
    {!! Form::label('tipo_telefone_id', 'Tipo de Telefone', ['class' => 'control-label']) !!}
    {!! Form::select('tipo_telefone_id', $tipoTelefone, null, ['class' => 'form-control', 'placeholder' => 'Selecione o Tipo de Telefone']) !!}
    {!! Form::error('tipo_telefone_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('telefone', $errors, 'col-md-3') !!}
    {!! Form::label('telefone', 'Telefone', ['class' => 'control-label']) !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
    {!! Form::error('telefone', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('escolaridade_id', $errors, 'col-md-3') !!}
    {!! Form::label('escolaridade_id', 'Escolaridade', ['class' => 'control-label']) !!}
    {!! Form::select('escolaridade_id', $escolaridade, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Escolaridade']) !!}
    {!! Form::error('escolaridade_id', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('profissao_id', $errors, 'col-md-3') !!}
    {!! Form::label('profissao_id', 'Profissão', ['class' => 'control-label']) !!}
    {!! Form::select('profissao_id', $profissao, null, ['class' => 'form-control', 'placeholder' => 'Selecione a Profissão']) !!}
    {!! Form::error('profissao_id', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<h4 class="title-section">Endereço</h4>
<hr>
<div class="row">
    {!! Html::openFormGroup('cep', $errors, 'col-md-2') !!}
    {!! Form::label('cep', 'CEP', ['class' => 'control-label']) !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
    {!! Form::error('cep', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('logradouro', $errors, 'col-md-8') !!}
    {!! Form::label('logradouro', 'Logradouro', ['class' => 'control-label']) !!}
    {!! Form::text('logradouro', null, ['class' => 'form-control']) !!}
    {!! Form::error('logradouro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('numero', $errors, 'col-md-2') !!}
    {!! Form::label('numero', 'Número', ['class' => 'control-label']) !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
    {!! Form::error('numero', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

<div class="row">
    {!! Html::openFormGroup('bairro', $errors, 'col-md-4') !!}
    {!! Form::label('bairro', 'Bairro', ['class' => 'control-label']) !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
    {!! Form::error('bairro', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('cidade', $errors, 'col-md-5') !!}
    {!! Form::label('cidade', 'Cidade', ['class' => 'control-label']) !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
    {!! Form::error('cidade', $errors) !!}
    {!! Html::closeFormGroup() !!}

    {!! Html::openFormGroup('uf', $errors, 'col-md-3') !!}
    {!! Form::label('uf', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::select('uf', [], null, ['class' => 'form-control']) !!}
    {!! Form::error('uf', $errors) !!}
    {!! Html::closeFormGroup() !!}
</div>

{!! Html::openFormGroup('complemento', $errors) !!}
{!! Form::label('complemento', 'Complemento', ['class' => 'control-label']) !!}
{!! Form::text('complemento', null, ['class' => 'form-control']) !!}
{!! Form::error('complemento', $errors) !!}
{!! Html::closeFormGroup() !!}