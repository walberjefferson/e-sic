@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Novo Cadastro')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Cadastre-se
                    </div>
                    <div class="panel-body">
                        <div id="form-cadastro">
                            {!! Form::open(['route' => 'front.solicitante.store', 'class' => 'form']) !!}
                            @include('front.solicitante._form')
                            <hr>

                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <button type="submit" class="btn btn-primary pull-right">Cadastrar</button>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            var tipoDocumento = $("#tipo_documento").val();
            if (tipoDocumento == 1) {
                $("#no-cpf").hide();
            }
            var tipo_selecionado = $("input[name='tipo_pessoa']:checked").val();
            mostraTipo(tipo_selecionado);

            $("input[name='tipo_pessoa']").change(function () {
                var selecionado = $(this).val();
                if(selecionado == 'j'){
                    $('#documento').mask('00.000.000/0000-00', {reverse: true, placeholder: "xx.xxx.xxx/xxxx-xx"});
                }else{
                    $('#documento').unmask();
                    $('#documento').attr('placeholder', '');
                }
                mostraTipo(selecionado)
            });

            $("#tipo_documento").change(function () {
                var selecionado = $(this).val();
                if (selecionado == 1) {
                    $("#no-cpf").hide();
                    $('#documento').mask('000.000.000-00', {reverse: true, placeholder: "xxx.xxx.xxx-xx"});
                } else {
                    $("#no-cpf").show();
                    $('#documento').unmask();
                    $('#documento').attr('placeholder', '');
                }
            });

            $.getJSON('/estados-cidades.json', function (data) {
                var items = [];
                var options = '<option value="">UF</option>';
                $.each(data.estados, function (key, val) {
                    options += '<option value="' + val.sigla + '">' + val.sigla + '</option>';
                });
                $("#uf").html(options);

                $("#uf").change(function () {

                    var options_cidades = '<option value="">Região</option>';
                    var str = "";

                    $("#uf option:selected").each(function () {
                        str += $(this).text();
                    });

                    $.each(data.estados, function (key, val) {
                        if (val.sigla == str) {
                            $.each(val.cidades, function (key_city, val_city) {
                                options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                            });
                        }
                    });
                    $("#cidade").html(options_cidades);

                }).change();

            });

            function mostraTipo(tipo) {
                var nome = $('label[for=name]');
                var dataNascimento = $('label[for=data_nascimento]');
                // var genero = $('label[for=genero]');
                var documento = $('label[for=documento]');
                var tipo_documento = $('label[for=tipo_documento]');
                if (tipo == 'f') {
                    nome.text('Nome *');
                    dataNascimento.text('Data Nascimento');
                    documento.text('Documento de Identificação');
                    documento.parent().removeClass('col-md-6').addClass('col-md-3');
                    $("#no-cpf").show();
                    // nome.parent().removeClass('col-md-9').addClass('col-md-9');
                    tipo_documento.parent().show();
                    // genero.parent().show();
                }
                if (tipo == 'j') {
                    nome.text('Razão Social *');
                    dataNascimento.text('Data Fundação');
                    documento.text("CNPJ");
                    nome.parent().removeClass('col-md-6').addClass('col-md-9');
                    documento.parent().removeClass('col-md-3').addClass('col-md-6');
                    $("#no-cpf").hide();
                    tipo_documento.parent().hide();
                    // genero.parent().hide();
                }
            }

            $('#data_nascimento, #data_expedicao').mask('99/99/9999', {placeholder: "__/__/____"});
            $('#cep').mask('00000-000');

            var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                    onKeyPress: function(val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };

            $('#telefone').mask(SPMaskBehavior, spOptions);

            $('#cep').keyup(function () {
                var valor = $(this).val();
                var cep = valor.replace(/[^0-9]/g,'');
                if(cep.length == 8){
                    $.get("http://api.postmon.com.br/v1/cep/" + cep + "?format=json", function (dados) {
                        $("#uf").val(dados.estado);
                        $("#cidade").val(dados.cidade);
                    }).fail(function (dados) {
                        alert(dados.statusText);
                    });
                }
            });

        });
    </script>
@endpush