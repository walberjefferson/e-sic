@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Painel Solicitante')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-8 no-padding">
                <div class="col-sm-12 no-padding">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><b> Bem-vindo, {{ auth()->guard('solicitante')->user()->name }}! </b>
                        </div>
                        <div class="panel-body" style="padding: 5px;">
                            <ul class="col-sm-12 no-padding list-unstyled text-center" role="tablist">
                                <li role="presentation" class="col-sm-3 col-xs-6">
                                    <h2>{{ $solicitacoes['total'] }}</h2>
                                    <hr>
                                    Total
                                </li>
                                <li role="presentation" class="col-sm-3 col-xs-6">
                                    <h2>{{ $solicitacoes['pendentes'] }}</h2>
                                    <hr>
                                    Pendentes
                                </li>
                                <li role="presentation" class="col-sm-3 col-xs-6">
                                    <h2>{{ $solicitacoes['negadas'] }}</h2>
                                    <hr>
                                    Negadas
                                </li>
                                <li role="presentation" class="col-sm-3 col-xs-6">
                                    <h2>{{ $solicitacoes['respondidas'] }}</h2>
                                    <hr>
                                    Respondidas
                                </li>
                            </ul>
                        </div>
                        <div class="panel-footer">
                            <div class="text-center">
                                <a href="{{ route('front.solicitacao.index') }}">Minhas Solicitações</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a href="{{ route('front.solicitacao.create') }}" class="btn btn-success  btn-lg btn-block">Nova
                            Solicitação</a>
                    </div>
                </div>

                <div class="col-sm-12 no-padding" style="margin-top: 20px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <b>Minhas Últimas Solicitações</b>
                        </div>
                        <ul class="list-group">
                            @forelse($solicitacoes['ultimas_solicitacoes'] as $d)
                                <?php
                                if ($d->situacao_id == 2):
                                    $class = 'label-success';
                                elseif ($d->situacao_id == 3):
                                    $class = 'label-danger';
                                else:
                                    $class = 'label-primary';
                                endif;
                                ?>
                                <li class="list-group-item">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th>Nº Solicitação:</th>
                                            <td>{{ $d->numero_protocolo }}</td>
                                        </tr>

                                        <tr>
                                            <th>Data da Solicitação:</th>
                                            <td>{{ $d->data_solicitacao }}</td>
                                        </tr>

                                        <tr>
                                            <th>Previsão de Resposta:</th>
                                            <td>{{ $d->data_previsao_resposta }}</td>
                                        </tr>
                                        <tr>
                                            <th>Situação:</th>
                                            <td><span class="label {{ $class }}">{{ $d->situacao['nome'] }}</span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            @empty
                                <li class="list-group-item">
                                    Você ainda não realizou solicitações.
                                    <a href="{{ route('front.solicitacao.create') }}">
                                        <span style="color: #c0392b; font-size: 2rem"> <b>Faça uma solicitação agora.</b> </span>
                                    </a>
                                </li>
                            @endforelse
                        </ul>
                        @if(empty($solicitacoes['ultimas_solicitacoes']))
                            <div class="panel-footer">
                                <div class="text-center">Nenhum resultado encontrado.</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-4 no-padding-right">
                <div class="panel panel-primary">
                    <div class="panel-heading"><b>Atenção!</b></div>
                    <div class="panel-body">
                        <p style="text-align: justify;">
                            Faça seu pedido de informação de maneira específica e de forma clara e precisa.
                            Não serão atendidos pedidos de informação genérico, despropocionais ou desarrazoados
                            ou que exijam trabalhos adicionais de análise, interpretação ou consolidação de dados e
                            informações, serviço de produção ou tratamento de dados (Art. 9. III e Art. 10, I, II e III
                            do Decreto n° 26.320/2013.
                        </p>
                        <span>
                        <hr>
                        <strong>Pedidos realizados após as 17:00 h</strong>
                        <hr>
                    </span>
                        <p style="text-align: justify;">
                            Devido ao horário de funcionamento dos protocolos, os pedidos, recursos e
                            reclamações realizados entre 17hs e 23h59 serão considerados como se tivessem
                            sido realizados no dia útil seguinte e a contagem só terá início a partir do primeiro
                            dia útil posterior. Exemplo: um pedido registrado às 19h de 16/05 será registrado como
                            um pedido de 17/05. Portanto, a contagem do prazo para resposta começará em 18/05, caso
                            este seja um dia útil.
                        </p>
                        <span>
                        <hr>
                        <strong>Prazo final coincidindo com final de semana ou feriado</strong>
                        <hr>
                    </span>
                        <p style="text-align: justify;">
                            Quando o prazo final para responder a solicitação coincidir com final de semana ou
                            feriado previsto em portaria do Ministério do Planejamento, Orçamento e Gestão e em
                            portaria publicada pelo Governo Estadual de Alagoas, ele será postergado para o próximo
                            dia útil. Por isso, o prazo para envio de resposta pode não ser exatamente o de 20 ou,
                            caso haja prorrogação, 30 dias corridos.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        .table {
            margin-bottom: 0;
        }
        .list-group-item{
            padding: 0 !important;
            border-bottom: 4px solid #337ab7 !important;
        }

        table > tbody > tr > th {
            text-align: right;
            width: 25%;
        }
    </style>
@endpush