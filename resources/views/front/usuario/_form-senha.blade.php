<h4 class="title-section">Alterar Senha</h4>
<hr>
{!! Html::openFormGroup('password', $errors) !!}
{!! Form::label('password', 'Nova Senha *', ['class' => 'control-label']) !!}
{!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
{!! Form::error('password', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Html::openFormGroup('password_confirmation', $errors) !!}
{!! Form::label('password_confirmation', 'Confirmar Nova Senha *', ['class' => 'control-label']) !!}
{!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
{!! Form::error('password_confirmation', $errors) !!}
{!! Html::closeFormGroup() !!}
