@extends('layouts.'. env('TEMA', 'app'))

@section('title', 'Painel Solicitante')

@section('content')
    <div class="container">
        <div class="col-sm-12 main-home no-padding no-margin">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Meu Perfil
                    <div class="pull-right">
                        <a class="btn btn-block btn-danger btn-xs" href="{{ route('front.usuario.altera-senha') }}">
                            <i class="fa fa-key"></i> Alterar Senha
                        </a>
                    </div>
                </div>
                <div class="panel-body form-login-home">
                    {!! Form::model($solicitante, ['route' => ['front.usuario.update', $solicitante->id], 'class' => 'form', 'method' => 'put']) !!}
                    @include('front.usuario._form')
                    <hr>

                    <div class="row">
                        <div class="col-sm-4">
                            <a href="#" class="btn btn-default pull-left">Voltar</a>
                        </div>
                        <div class="col-sm-4">

                        </div>
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary pull-right">Atualizar</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var tipoDocumento = $("#tipo_documento").val();
            if (tipoDocumento == 1) {
                $("#no-cpf").hide();
            }
            var tipo_selecionado = $("input[name='tipo_pessoa']:checked").val();
            mostraTipo(tipo_selecionado);

            $("input[name='tipo_pessoa']").change(function () {
                var selecionado = $(this).val();
                if(selecionado == 'j'){
                    $('#documento').mask('00.000.000/0000-00', {reverse: true, placeholder: "  .   .   /    -  "});
                }else{
                    $('#documento').unmask();
                }
                mostraTipo(selecionado)
            });

            $("#tipo_documento").change(function () {
                var selecionado = $(this).val();
                if (selecionado == 1) {
                    $("#no-cpf").hide();
                    $('#documento').mask('000.000.000-00', {reverse: true, placeholder: "   .   .   -  "});
                } else {
                    $("#no-cpf").show();
                    $('#documento').unmask();
                }
            });

            function mostraTipo(tipo) {
                var nome = $('label[for=name]');
                var dataNascimento = $('label[for=data_nascimento]');
                var genero = $('label[for=genero]');
                var documento = $('label[for=documento]');
                var tipo_documento = $('label[for=tipo_documento]');
                if (tipo == 'f') {
                    nome.text('Nome *');
                    dataNascimento.text('Data Nascimento');
                    documento.text('Documento de Identificação *');
                    documento.parent().removeClass('col-md-6').addClass('col-md-3');
                    $("#no-cpf").show();
                    nome.parent().removeClass('col-md-9').addClass('col-md-6');
                    tipo_documento.parent().show();
                    genero.parent().show();
                }
                if (tipo == 'j') {
                    nome.text('Razão Social *');
                    dataNascimento.text('Data Fundação');
                    documento.text("CNPJ *");
                    nome.parent().removeClass('col-md-6').addClass('col-md-9');
                    documento.parent().removeClass('col-md-3').addClass('col-md-6');
                    $("#no-cpf").hide();
                    tipo_documento.parent().hide();
                    genero.parent().hide();
                }
            }

            $('#data_nascimento, #data_expedicao').mask('99/99/9999', {placeholder: "__/__/____"});
            $('#cep').mask('00000-000');
            var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                },
                spOptions = {
                    onKeyPress: function(val, e, field, options) {
                        field.mask(SPMaskBehavior.apply({}, arguments), options);
                    }
                };

            $('#telefone').mask(SPMaskBehavior, spOptions);

        });
    </script>
@endpush