@extends('layouts.'. env('TEMA', 'app'))

@section('title', config('app.name', 'E-sic'))

@php
    header("Content-type: application/pdf");
@endphp

@section('content')
    <div class="container">
        <div id="login">
            <div class="row main-home">
                <div class="col-sm-10 col-sm-offset-1">
                    <h1 class="page-header">Acesso à Informação</h1>
                    <p class="text-justify">
                        A Lei nº 12.527/2011 regulamenta o direito constitucional de acesso às informações públicas.
                        Essa
                        norma entrou em vigor em 16 de maio de 2012 e criou mecanismos que possibilitam, a qualquer
                        pessoa,
                        física ou jurídica, sem necessidade de apresentar motivo, o recebimento de informações públicas
                        dos
                        órgãos e entidades. A Lei vale para os três Poderes da União, Estados, Distrito Federal e
                        Municípios, inclusive aos Tribunais de Conta e Ministério Público. Entidades privadas sem fins
                        lucrativos também são obrigadas a dar publicidade a informações referentes ao recebimento e à
                        destinação dos recursos públicos por elas recebidos.
                    </p>
                </div>
            </div>
        </div>

        <div id="botoes-home" class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <hr>
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <div class="panel panel-default">
                            <a href="#" data-toggle="modal" data-target="#modalSolicitacao">
                                <div class="panel-body">
                                    <h1>
                                        <i class="icon icon-finance-023"></i>
                                        <br>
                                        <small>Solicitar Informação</small>
                                    </h1>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6 text-center">
                        <div class="panel panel-default">
                            <a href="{{ route('front.solicitacao.consulta') }}">
                                <div class="panel-body">
                                    <h1>
                                        <i class="icon icon-finance-132"></i>
                                        <br>
                                        <small>Acompanhar Informação</small>
                                    </h1>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <div class="panel panel-default">
                            <a data-link="{{ asset('pdf/Lai.pdf') }}"
                               data-toggle="modal"
                               data-target="#modalLei">
                                <div class="panel-body">
                                    <i class="icon icon-education-134"></i> <span>Lei de Acesso à Informação</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="panel panel-default">
                            <a
                                    @if(!empty($dados->decreto))
                                    data-link="{{ url('/storage/sic/' . $dados->decreto) }}"
                                    data-toggle="modal"
                                    data-target="#modalDecreto"
                                    title="Visualiar"
                                    @else
                                    href="#"
                                    @endif
                            >
                                <div class="panel-body">
                                    <i class="icon icon-education-134"></i> <span>Decreto Municipal</span>
                                </div>
                            </a>
                        </div>
                    </div><!-- coluna decreto -->
                    <div class="col-sm-4 text-center">
                        <div class="panel panel-default">
                            <a href="{{ route('front.estatisticas') }}">
                                <div class="panel-body">
                                    <i class="icon icon-finance-012"></i> <span>Estatísticas</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                @if(!empty($dados))
                    <h2 class="page-header">SIC Físico</h2>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-sm-8">

                            @if(!empty($dados->orgao['nome']))
                                <p><b>Órgão vinculado:</b> {{ $dados->orgao['nome'] }}</p>
                            @endif

                            @if(!empty($dados->endereco) || !empty($dados->numero) || !empty($dados->bairro))
                                <p><i class="fa fa-map-marker"></i> {{ $dados->endereco }}, {{ $dados->numero }}
                                    - {{ $dados->bairro }}</p>
                            @endif

                            @if(!empty($dados->cidade) || !empty($dados->estado) || !empty($dados->cep))
                                <p>
                                    <i class="fa fa-map-pin"></i>
                                    {{ $dados->cidade }}-{{ $dados->estado }} - {{ $dados->cep }}
                                </p>
                            @endif

                            @if(!empty($dados->orgao['telefone']))
                                <p>
                                    <i class="fa fa-phone"></i>
                                    {{ $dados->orgao['telefone'] }}
                                </p>
                            @endif

                            @if(!empty($dados->orgao['email']))
                                <p>
                                    <i class="fa fa-envelope-o"></i>
                                    <a href="mailto:{{ $dados->orgao['email'] }}"
                                       class="email">{{ $dados->orgao['email'] }}</a>
                                </p>
                            @endif

                            @if($dados->horario_funcionamento)
                                <p><i class="fa fa-clock-o"></i> {{ $dados->horario_funcionamento }}</p>
                            @endif

                            @if($dados->informacao)
                                <p>{{ $dados->informacao }}</p>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if($dados->formulario_requerimento)
                                <div class="panel panel-default text-center">
                                    <a data-link="{{ url('storage/sic/' . $dados->formulario_requerimento) }}"
                                       data-toggle="modal"
                                       data-target="#modalDecreto"
                                       title="Visualiar">
                                        <div class="panel-body">
                                            <i class="icon icon-education-008"></i>
                                            <span>Formulário de Requerimento</span>
                                        </div>
                                    </a>
                                </div>
                            @endif

                            @if($dados->formulario_recurso)
                                <div class="panel panel-default text-center">
                                    <a data-link="{{ url('storage/sic/' . $dados->formulario_recurso) }}"
                                       data-toggle="modal"
                                       data-target="#modalDecreto"
                                       title="Visualiar">
                                        <div class="panel-body">
                                            <i class="icon icon-education-008"></i> <span>Formulário de Recurso</span>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    </div>

    <!-- Modal Solicitação -->
    <div class="modal fade" id="modalSolicitacao" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Enviar Solicitação</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('front.solicitacao.nova') }}" class="btn btn-primary btn-block btn-lg">Acompanhar
                                solicitação por e-mail</a>
                            <br>
                            <p class="text-center">
                                A solicitação pode ser acompanhada por e-mail ou por número de protocolo, sem a
                                necessidade de fazer pre-cadastro no sistema.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('front.solicitante.novo') }}" class="btn btn-primary btn-block btn-lg">Acompanhar
                                solicitação pelo sistema</a>
                            <br>
                            <p class="text-center">
                                Para fazer a solicitação é necessário realizar um cadastro de usuário, permite
                                acompanhar e gerenciamento todas as solicitações realizadas pelo usuário.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalLei" tabindex="-2" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lei de Acesso a Informação</h4>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    @if(!empty($dados))
        <!-- Modal Decreto -->
        <div class="modal fade" id="modalDecreto" style="z-index: 1055;" tabindex="-2" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Visualização PDF</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@push('css')
    <style>
        body {
            overflow-x: hidden;
        }

        .page-header {
            margin-bottom: 10px;
            margin-top: 0;
        }

        .modal-body {
            padding: 20px 15px;
        }

        .modal-body .table tr th {
            width: 25%;
            text-align: right;
        }

        a {
            cursor: pointer;
        }
    </style>
@endpush

@push('js')
    <script>
        $(function () {
            mostraPdf("#modalDecreto");
            mostraPdf("#modalLei");
        });

        function mostraPdf(seletor) {
            $(seletor).find('.modal-dialog').width('80%');
            $(seletor).find('.modal-body').css('padding', '5px 5px 0 5px');
            $(seletor).on('shown.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('target');
                var url = button.data('link');
                $(id + ' .modal-body').html('');
                var html = '<embed src="' + url + '" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html" height="500" width="100%">';
                $(id + ' .modal-body').html(html);
            });
            $(seletor).on('hidden.bs.modal', function () {
                $(this).find('.modal-body').html('')
            });
        }
    </script>
@endpush