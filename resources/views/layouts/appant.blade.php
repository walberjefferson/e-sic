<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilo_estatisticas.css') }}">
    @stack('css')
</head>
<body>
<div id="app">
    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h1><a href="{{ url('/') }}">Logo</a></h1>
                </div>
                <div class="col-md-9"></div>
            </div>
        </div>
    </header>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                @if(auth()->guard('solicitante')->check())
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Início</a></li>
                        <li><a href="{{ route('front.solicitacao.index') }}"><i class="fa fa-file-text-o"></i> Minhas
                                Solicitações</a></li>
                        <li><a href="{{ route('front.solicitacao.create') }}"><i class="fa fa-plus"></i> Nova
                                Solicitação</a></li>
                        <li><a href="{{ route('front.usuario.ver') }}"><i class="fa fa-user"></i> Meu Perfil</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><p class="navbar-text">Olá <b>{{ auth()->guard('solicitante')->user()->name }}</b>!</p></li>
                        <li>
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Sair </a>
                            <form id="logout-form" action="{{ route('front.solicitante.logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                @else
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('front.estatisticas') }}">Estatísticas</a></li>
                        @foreach($paginas as $p)
                            <li><a href="{{ route('front.pagina.show', $p->slug) }}">{{ $p->nome }}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 no-padding">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                            <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                    @endif

                    @if (session('erro'))
                        <div class="alert alert-danger">
                            {{ session('erro') }}
                            <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @yield('content')
    </main>

    <footer id="footer">

    </footer>
</div>

<!-- Scripts -->
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
{{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js"></script>

@stack('js')
</body>
</html>
