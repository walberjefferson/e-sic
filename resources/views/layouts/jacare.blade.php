<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/line-icons-pro/styles.css">
    <link rel="stylesheet"
          href="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/style.css">

    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/footers/footer-v1.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/animate.css">
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons/line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons-pro/styles.css') }}">
    <link rel="stylesheet"
          href="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/theme-colors/blue.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/theme-skins/dark.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/custom-jacare.css">

    <link rel="stylesheet" href="{{ asset('css/estilo_estatisticas.css') }}">
@stack('css')

<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
    ]) !!};
    </script>
    <style>
        header {margin-bottom: 35px;}
        #login h1.page-header {margin-top: 0px;}
        .footer-v1 .footer {margin-top: 20px}

        #botoes-home .panel-default{
            background-color: #0167bc;
            border-color: #003399;
            transition: all .3s;
        }

        #botoes-home .panel-default:hover{
            background-color: #003399;
        }

        #botoes-home .panel-body {
            font-size: 1.3rem;
            /*line-height: 1.5rem;*/
        }

        #botoes-home .panel-body span {
            position: relative;
            bottom: 4px;
            margin-left: 3px;
        }

        #botoes-home .panel-body .icon{
            font-size: 1.8rem;
        }

        #botoes-home .panel-body h1 .icon{
            font-size: 4rem;
        }
        #botoes-home .panel-body h1 small{
            font-size: 2rem;
        }
        #botoes-home .panel-body h1, #botoes-home .panel-body h1 small{
            color: #FFF;
            margin: 0px;
            font-weight: normal !important;
        }

        #botoes-home a {
            color: #0167bc;
            transition: all .3s;
        }

        #botoes-home a.email:hover{
            color: #9a0f12;
        }

        #botoes-home a:hover {
            text-decoration: none;
            color: #FFF;
        }

        #botoes-home .panel-default a, #botoes-home .panel-default a h1, #botoes-home .panel-default a h1 small {
            color: #FFF;
            transition: all .3s;
        }

        #botoes-home .panel-default a:hover, #botoes-home .panel-default a:hover h1, #botoes-home .panel-default a:hover h1 small{
            text-decoration: none;
            color: #FFF;

        }
    </style>
</head>
<body>
<header>
    <div class="container">
        <div class="col-md-3 col-xs-12 col-sm-12">
            <a href="{{ url('/') }}">
                <img src="{{ env('URL_TRANSPARENCIA') }}/storage/configuracao/logo-jacare-dos-homens-2-1511224839.png"
                     alt="Prefeitura Municipal de Jacaré dos Homens" class="img-responsive logo">
            </a>
        </div>
        <div class="col-md-9 col-xs-12 col-sm-12">
            @include('layouts.jacare.menu-header')
        </div>
    </div>
</header>

<div class="wrapper">
    <!--=== Content Part  ===-->
    <div id="app">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 no-padding">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif

                @if (session('erro'))
                    <div class="alert alert-danger">
                        {{ session('erro') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @yield('content')
</div><!--/container-->
<!--=== End Content Part  ===-->

<!--=== Footer Version 1 ===-->
<div class="footer-v1">
    @include('layouts.jacare.menu-footer')

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="text-center">
                        &copy; <?= date('Y'); ?> - Todos os direitos reservados.
                    </p>
                </div>
            </div>
        </div>
    </div><!--/copyright-->
</div><!--/footer-v1-->
<!--=== End Footer Version 1 ===-->

<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/back-to-top.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/site.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/app.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/apps.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js"
        type="text/javascript"></script>
@stack('js')
</body>
</html>
