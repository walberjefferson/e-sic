<div class="footer">
    <div class="container">
        <div class="row">
            <!-- About -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Institucional</h3>
                    <ul id="menu-institucional-rodape" class="menu">
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}" target="_blank">Poder Executivo</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}/controladoria/"
                               target="_blank">Controladoria</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}/procuradoria/" target="_blank">Procuradoria</a>
                        </li>
                    </ul>
                </section>

            </div><!--/col-md-3-->
            <!-- End About -->

            <!-- Latest -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Transparência</h3>
                    <ul>
                        <li><a title="Demonstrativos" href="{{ env('URL_TRANSPARENCIA', '#') }}/demonstrativos"
                               target="_blank">Demonstrativos</a>
                        </li>
                        <li><a title="LRF - Lei de Responsabilidade Fiscal"
                               href="{{ env('URL_TRANSPARENCIA', '#') }}/lrf" target="_blank">LRF - Lei de
                                Responsabilidade Fiscal</a>
                        </li>
                        <li><a title="Receitas" href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Receitas</a>
                        </li>
                        <li>
                            <a title="Despesas" href="{{ env('URL_DESPESAS', '#') }}" target="_blank">Despesas</a>
                        </li>
                        <li>
                            <a title="Contratos" href="{{ env('URL_TRANSPARENCIA', '#') }}/contrato" target="_blank">Contratos</a>
                        </li>
                        <li>
                            <a title="Convênios"
                               href="http://www.portaldatransparencia.gov.br/convenios/ConveniosLista.asp?UF=al&amp;CodMunicipio=2767"
                               target="_blank">Convênios</a>
                        </li>
                        <li><a title="Licitações" href="{{ env('URL_TRANSPARENCIA', '#') }}/licitacao" target="_blank">Licitações</a>
                        </li>
                        <li><a title="Orçamento" href="{{ env('URL_TRANSPARENCIA', '#') }}/orcamento" target="_blank">Orçamento</a>
                        </li>
                        <li><a title="Servidores" href="{{ env('URL_TRANSPARENCIA', '#') }}/servidores" target="_blank">Servidores</a>
                        </li>
                        <li><a title="Legislação" href="{{ env('URL_TRANSPARENCIA', '#') }}/legislacao" target="_blank">Legislação</a>
                        </li>
                    </ul>
                </section>
            </div><!--/col-md-3-->
            <!-- End Latest -->

            <!-- Link List -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Secretarias</h3>
                    <ul id="menu-secretarias-rodape" class="menu">
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-agricultura-e-infraestrutura/"
                               target="_blank">Agricultura,
                                Meio Ambiente e Infraestrutura</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-administracao-controle-e-financas/"
                               target="_blank">Controle,
                                Finanças e Administração</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-cultura/"
                               target="_blank">Cultura</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-educacao-e-esportes/"
                               target="_blank">Educação e
                                Esportes</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-saude/" target="_blank">Saúde</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-assistencia-social/"
                               target="_blank">Trabalho e
                                Assistência Social</a>
                        </li>
                    </ul>

                </section>
            </div><!--/col-md-3-->
            <!-- End Link List -->

            <!-- Address -->
            <div class="col-md-3 md-margin-bottom-40">
                <div class="informacoes-contato">
                    <p>
                        <i class="fa fa-home"></i>
                        Rua José Alves Feitosa, S/N - Centro - Jacaré dos Homens/AL - 57.430-000
                    </p>
                    <p>
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:prefjdoshomens@gmail.com">prefjdoshomens@gmail.com</a>
                    </p>
                    <p><i class="fa fa-phone"></i> (82) 3534-1194</p>
                </div>
            </div><!--/col-md-3-->
            <!-- End Address -->
        </div>
    </div>
</div>