<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/style-lagoa.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons/line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons-pro/styles.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/theme-colors/default-lagoa.css">
    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ env('URL_TRANSPARENCIA') }}/assets/css/custom-lagoa.css">
    <link rel="stylesheet" href="{{ asset('css/estilo_estatisticas.css') }}">
    @stack('css')

    <style>
        body {
            overflow-x: hidden;
        }

        .header .header-socials {
            width: 400px !important;
        }

        .header {
            margin-bottom: 30px;
        }

        #botoes-home .panel-default {
            background-color: #089ed5;
            border-color: #038abb;
            transition: all .3s;
        }

        #botoes-home .panel-default:hover {
            background-color: #038abb;
        }

        #botoes-home .panel-body {
            font-size: 1.3rem;
            /*line-height: 1.5rem;*/
        }

        #botoes-home .panel-body span {
            position: relative;
            bottom: 4px;
            margin-left: 3px;
        }

        #botoes-home .panel-body .icon {
            font-size: 1.8rem;
        }

        #botoes-home .panel-body h1 .icon {
            font-size: 4rem;
        }

        #botoes-home .panel-body h1 small {
            font-size: 2rem;
        }

        #botoes-home .panel-body h1, #botoes-home .panel-body h1 small {
            color: #FFF;
            margin: 0px;
            font-weight: normal !important;
        }

        #botoes-home a {
            color: #089ed5;
            transition: all .3s;
        }

        #botoes-home a:hover {
            text-decoration: none;
            color: #fac412;
        }

        #botoes-home .panel-default a, #botoes-home .panel-default a h1, #botoes-home .panel-default a h1 small {
            color: #FFF;
            transition: all .3s;
        }

        #botoes-home .panel-default a:hover, #botoes-home .panel-default a:hover h1, #botoes-home .panel-default a:hover h1 small {
            text-decoration: none;
            color: #fac412;

        }
    </style>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
    'csrfToken' => csrf_token(),
    ]) !!};
    </script>
</head>
<body style="overflow-x: hidden;">

<div class="wrapper">
    <!--=== Header ===-->
    <div class="header">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container ">
                <!-- Brand  -->
                <div class="navbar-header container-header1">
                    <ul class="header-socials list-inline float-right">
                        <li style="display:block; text-align: left">
                            <h1 style="margin-top: 0">e-SIC</h1>
                            <p style="font-size: 14px;">Sistema Eletrônico do Serviço de Informação ao Cidadão</p>
                        </li>

                    </ul>
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/lagoa/426_120.png') }}"
                             alt="Prefeitura Municipal de Lagoa da Canoa" class="img-responsive">
                    </a>
                </div>
            </div>
            @include('layouts.lagoa.menu-header')

        </div>
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->


    <!--=== Content Part  ===-->
    <div id="app">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 no-padding">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif

                @if (session('erro'))
                    <div class="alert alert-danger">
                        {{ session('erro') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif
            </div>
        </div>
        @yield('content')
    </div><!--/container-->
    <!--=== End Content Part  ===-->

    <!--=== Footer Version 1 ===-->
    <!--=== Footer===-->
    <div class="footer-v1">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 map-img md-margin-bottom-40" style="min-height: 280px">
                        <a href="{{ url('/') }}" id="btn-brasao">
                            <img src="{{ asset('img/lagoa/225_200.png') }}"
                                 alt="Prefeitura Municipal de Lagoa da Canoa" class="img-responsive brasao">
                        </a>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>O Município</h2></div>
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="{{ env('URL_INSTITUCIONAL') }}/noticias" target="_blank">Notícias</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_INSTITUCIONAL') }}/pagina/historico" target="_blank">Histórico</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/legislacao" target="_blank">Legislação</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_INSTITUCIONAL') }}/pagina/poder_executivo" target="_blank">Poder Executivo Municipal</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Portal da Transparência</h2></div>
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Receitas</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Despesas</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/licitacao" target="_blank">Licitações</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/servidores" target="_blank">Servidores</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Contatos</h2></div>
                        <address class="md-margin-bottom-40">
                            Av. Antônio Albuquerque, s/n, Centro <br>
                            Lagoa da Canoa - AL <br>
                            Cep: 57330-000 <br>
                            Telefone: +55 82 3528-1150 <br>
                            <a href="mailto:pref.lagoadacanoa@gmail.com"> pref.lagoadacanoa@gmail.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            &copy; <?= date('Y'); ?> - Todos os direitos reservados.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Footer Version 1 ===-->

</div>

<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" type="text/javascript"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/assets/plugins/back-to-top.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/site.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/app.js"></script>
<script src="{{ env('URL_TRANSPARENCIA') }}/js/apps.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js"
        type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>

@stack('js')
</body>
</html>
