<div class="footer">
    <div class="container">
        <div class="row">
            <!-- About -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Institucional</h3>
                    <ul id="menu-institucional-rodape" class="menu">
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}" target="_blank">Poder Executivo</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}/controladoria/" target="_blank">Controladoria</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}/procuradoria/" target="_blank">Procuradoria</a></li>
                    </ul>
                </section>
                <section>
                    <h3 class="widget-title">Secretarias</h3>
                    <ul id="menu-secretarias-rodape" class="menu">
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-administracao/" target="_blank">Administração</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-agricultura-e-meio-ambiente/" target="_blank">Agricultura
                                e Meio Ambiente</a></li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-assistencia-social/" target="_blank">Assistência
                                Social</a></li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-cultura-e-esportes/" target="_blank">Cultura
                                e Esportes</a></li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-educacao/" target="_blank">Educação</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-financas/" target="_blank">Finanças</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-obras-saneamento-e-urbanismo/" target="_blank">Obras,
                                Saneamento e Urbanismo</a></li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-saude/" target="_blank">Saúde</a>
                        </li>
                        <li>
                            <a href="{{ env('URL_INSTITUCIONAL', '#') }}/secretaria-de-transporte-e-transito/" target="_blank">Transporte
                                e Trânsito</a></li>
                    </ul>

                </section>
            </div><!--/col-md-3-->
            <!-- End About -->

            <!-- Latest -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Transparência</h3>
                    <ul>
                        <li><a title="Demonstrativos" href="{{ env('URL_TRANSPARENCIA', '#') }}/demonstrativos" target="_blank">Demonstrativos</a></li>
                        <li><a title="LRF - Lei de Responsabilidade Fiscal" href="{{ env('URL_TRANSPARENCIA', '#') }}/lrf" target="_blank">LRF - Lei de
                                Responsabilidade Fiscal</a></li>
                        <li><a title="Receitas" href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Receitas</a></li>
                        <li><a title="Despesas" href="{{ env('URL_DESPESAS', '#') }}" target="_blank">Despesas</a></li>
                        <li><a title="Contratos" href="{{ env('URL_TRANSPARENCIA', '#') }}/contrato" target="_blank">Contratos</a></li>
                        <li><a title="Convênios" href="http://www.portaldatransparencia.gov.br/convenios/ConveniosLista.asp?UF=al&amp;CodMunicipio=2843" target="_blank">Convênios</a></li>
                        <li><a title="Licitações" href="{{ env('URL_TRANSPARENCIA', '#') }}/licitacao" target="_blank">Licitações</a></li>
                        <li><a title="Orçamento" href="{{ env('URL_TRANSPARENCIA', '#') }}/orcamento" target="_blank">Orçamento</a></li>
                        <li><a title="Servidores" href="{{ env('URL_TRANSPARENCIA', '#') }}/servidores">Servidores</a></li>
                        <li><a title="Legislação" href="{{ env('URL_TRANSPARENCIA', '#') }}/legislacao" target="_blank">Legislação</a></li>
                    </ul>
                </section>
            </div><!--/col-md-3-->
            <!-- End Latest -->

            <!-- Link List -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Serviços</h3>
                    <ul>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}" target="_blank">Nota Fiscal Eletrônica</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}" target="_blank">2º Via de Tributos</a></li>
                        <li><a href="{{ env('URL_INSTITUCIONAL', '#') }}" target="_blank">Agenda de Coleta de Lixo</a></li>
                    </ul>
                </section>
            </div><!--/col-md-3-->
            <!-- End Link List -->

            <!-- Address -->
            <div class="col-md-3 md-margin-bottom-40">
                <img src="{{ env('URL_TRANSPARENCIA', '#') }}/storage/configuracao/brasao-poco-3-1510689042.png" alt="Prefeitura Municipal de Poço das Trincheiras" class="img-responsive brasao">
                <div class="informacoes-contato">
                    <p>
                        <i class="fa fa-map-marker"></i>
                        Praça Leopoldo Wanderley, nº 91, Centro - Poço das Trincheiras - AL - 57.510-000
                    </p>
                    <p>
                        <i class="fa fa-envelope"></i>
                        <a href="mailto:pocodastrincheiras@hotmail.com"> pocodastrincheiras@hotmail.com</a>
                    </p>
                    <p><i class="fa fa-phone"></i> (82) 3626-1151 / 3626-1296</p>
                </div>
            </div><!--/col-md-3-->
            <!-- End Address -->
        </div>
    </div>
</div>