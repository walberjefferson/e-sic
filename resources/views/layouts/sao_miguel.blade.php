<!DOCTYPE html>
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="alternate" href="{{ url('/') }}" hreflang="pt-br">

    <title>@yield('title')</title>


    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons/line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/line-icons-pro/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl-carousel/owl-carousel/owl.carousel.css') }}">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

    <link rel="stylesheet" href="{!! asset('assets/css/footers/footer-v1.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/theme-colors/teal.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/theme-skins/dark.css') !!}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom/smdm.css') }}">
    <link rel="stylesheet" href="{{ asset('css/estilo_estatisticas.css') }}">
    @stack('css')
</head>
<body style="overflow-x: hidden;">

<div class="wrapper">
    <!--=== Header ===-->
    <div class="header">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container ">
                <!-- Brand  -->
                <div class="navbar-header container-header1">
                    <ul class="header-socials list-inline float-right">
                        <li style="display:block; text-align: left">
                            <h1 style="margin-top: 0">e-SIC</h1>
                            <p style="font-size: 14px;">Sistema Eletrônico do Serviço de Informação ao Cidadão</p>
                        </li>

                    </ul>
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('img/sao_miguel/logo.png') }}"
                             alt="Prefeitura Municipal de São Miguel dos Milagres" class="img-responsive">
                    </a>
                </div>
            </div>
            @include('layouts.sao_miguel.menu-header')

        </div>
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->


    <!--=== Content Part  ===-->
    <div id="app">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 no-padding">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif

                @if (session('erro'))
                    <div class="alert alert-danger">
                        {{ session('erro') }}
                        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                @endif
            </div>
        </div>
        @yield('content')
    </div><!--/container-->
    <!--=== End Content Part  ===-->

    <!--=== Footer Version 1 ===-->
    <!--=== Footer===-->
    <div class="footer-v1">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 map-img md-margin-bottom-40" style="min-height: 280px">
                        <a href="{{ url('/') }}" id="btn-brasao">
                            <img src="{{ asset('img/sao_miguel/logo_footer_sm.png') }}"
                                 alt="Prefeitura Municipal de São Miguel dos Milagres" class="img-responsive center-block brasao">
                        </a>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>O Município</h2></div>
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="{{ env('URL_NOTICIAS', '#') }}" target="_blank">Notícias</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_HISTORICO', '#') }}" target="_blank">Histórico</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/legislacao" target="_blank">Legislação</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_PODER_EXECUTIVO', '#') }}" target="_blank">Poder Executivo Municipal</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Portal da Transparência</h2></div>
                        <ul class="list-unstyled link-list">
                            <li>
                                <a href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Receitas</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_RECEITAS', '#') }}" target="_blank">Despesas</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/licitacao" target="_blank">Licitações</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="{{ env('URL_TRANSPARENCIA') }}/servidores" target="_blank">Servidores</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3 md-margin-bottom-40">
                        <div class="headline"><h2>Contatos</h2></div>
                        <address class="md-margin-bottom-40">
                            <p>R. Vigário Belo, 111 – Centro, <br>
                            São Miguel dos Milagres - AL <br>
                                57940-000</p>

                            <p>(82) 3295.1212</p>

                            <p>Horário de funcionamento: <br>
                                De Seg. à Sex. das 8:00hs às 14:00hs</p>
{{--                            <a href="mailto:pref.lagoadacanoa@gmail.com"> pref.lagoadacanoa@gmail.com</a>--}}
                        </address>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            &copy; <?= date('Y'); ?> - Todos os direitos reservados.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== End Footer Version 1 ===-->

</div>

<!-- Scripts -->
<script src="{{ asset('assets/js/scripts.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/back-to-top.js') }}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>

@stack('js')
</body>
</html>
