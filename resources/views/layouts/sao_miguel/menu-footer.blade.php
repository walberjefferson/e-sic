<div class="footer">
    <div class="container">
        <div class="row">
            <!-- About -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Institucional</h3>
                    <ul id="menu-institucional-rodape" class="menu">
                        <li><a href="{{config('url.principal')}}">Poder Executivo</a></li>
                        <li><a href="{{config('url.principal')}}/controladoria/">Controladoria</a></li>
                        <li><a href="{{config('url.principal')}}/procuradoria/">Procuradoria</a></li>
                    </ul>
                </section>

            </div><!--/col-md-3-->
            <!-- End About -->

            <!-- Latest -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Transparência</h3>
                    <ul>
                        <li><a title="Demonstrativos" href="{{ route('front.demonstrativos') }}">Demonstrativos</a></li>
                        <li><a title="LRF - Lei de Responsabilidade Fiscal" href="{{route('front.lrf')}}">LRF - Lei de
                                Responsabilidade Fiscal</a></li>
                        <li><a title="Receitas" href="{{ config('url.receita') }}">Receitas</a></li>
                        <li><a title="Despesas" href="{{config('url.despesa')}}">Despesas</a></li>
                        <li><a title="Contratos" href="{{ route('front.contrato') }}">Contratos</a></li>
                        <li><a title="Convênios" href="{{ config('url.convenio') }}">Convênios</a></li>
                        <li><a title="Licitações" href="{{ route('front.licitacao') }}">Licitações</a></li>
                        <li><a title="Orçamento" href="{{ route('front.orcamento') }}">Orçamento</a></li>
                        <li><a title="Servidores" href="#">Servidores</a></li>
                        <li><a title="Legislação" href="{{ route('front.legislacao') }}">Legislação</a></li>
                    </ul>
                </section>
            </div><!--/col-md-3-->
            <!-- End Latest -->

            <!-- Link List -->
            <div class="col-md-3 md-margin-bottom-40">
                <section>
                    <h3 class="widget-title">Secretarias</h3>
                    <ul id="menu-secretarias-rodape" class="menu">
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-administracao/">Administração</a>
                        </li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-agricultura-e-meio-ambiente/">Agricultura
                                e Meio Ambiente</a></li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-assistencia-social/">Assistência
                                Social</a></li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-cultura-e-esportes/">Cultura
                                e Esportes</a></li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-educacao/">Educação</a>
                        </li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-financas/">Finanças</a>
                        </li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-obras-saneamento-e-urbanismo/">Obras,
                                Saneamento e Urbanismo</a></li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-saude/">Saúde</a>
                        </li>
                        <li>
                            <a href="{{config('url.principal')}}/secretaria-de-transporte-e-transito/">Transporte
                                e Trânsito</a></li>
                    </ul>

                </section>
            </div><!--/col-md-3-->
            <!-- End Link List -->

            <!-- Address -->
            <div class="col-md-3 md-margin-bottom-40">
                @if($config->brasao)
                    <img src="{{ asset('storage/configuracao/'.$config->brasao) }}"
                         alt="{{ $config->titulo }}" class="img-responsive brasao">
                @endif
                <div class="informacoes-contato">

                </div>
            </div><!--/col-md-3-->
            <!-- End Address -->
        </div>
    </div>
</div><!--/footer-->