<nav class="navbar-menu">
    <div class="container">
        <div class="collapse navbar-collapse navbar-responsive-collapse">
            @if(auth()->guard('solicitante')->check())
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Início</a></li>
                    <li><a href="{{ route('front.solicitacao.index') }}"><i class="fa fa-file-text-o"></i> Minhas
                            Solicitações</a></li>
                    <li><a href="{{ route('front.solicitacao.create') }}"><i class="fa fa-plus"></i> Nova
                            Solicitação</a></li>
                    <li><a href="{{ route('front.usuario.ver') }}"><i class="fa fa-user"></i> Meu Perfil</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Sair </a>
                        <form id="logout-form" action="{{ route('front.solicitante.logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            @else
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Início</a></li>
                    <li><a href="{{ env('URL_INSTITUCIONAL') }}">Institucional</a></li>
                    <li><a href="{{ env('URL_TRANSPARENCIA') }}">Portal da Transparência</a></li>
                    <li><a href="{{ route('front.estatisticas') }}">Estatísticas</a></li>
                    {{--@foreach($paginas as $p)--}}
                        {{--<li><a href="{{ route('front.pagina.show', $p->slug) }}">{{ $p->nome }}</a></li>--}}
                    {{--@endforeach--}}
                </ul>
            @endif
        </div>

    </div>
</nav>