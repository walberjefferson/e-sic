@if (session('message'))
    <div class="alert alert-success">
        {{ session('message') }}
        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
    </div>
@endif

@if (session('erro'))
    <div class="alert alert-danger">
        {{ session('erro') }}
        <button type="button" class="close fade in" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
    </div>
@endif