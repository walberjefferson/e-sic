<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'front.', 'namespace' => 'Front'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/estatisticas', 'HomeController@estatisticas')->name('estatisticas');
    Route::get('solicitacao/nova', 'SolicitacaoController@nova')->name('solicitacao.nova');
    Route::post('solicitacao/nova', 'SolicitacaoController@storerNova')->name('solicitacao.storeNova');
    Route::get('solicitacao/consulta', 'SolicitacaoController@consulta')->name('solicitacao.consulta');
    Route::post('solicitacao/consulta', 'SolicitacaoController@consultaProtocolo')->name('solicitacao.consulta');

    Route::prefix('solicitante')->name('solicitante.')->group(function () {
        Route::get('/novo', 'SolicitanteController@index')->name('novo');
        Route::resource('/', 'SolicitanteController', ['except' => ['edit', 'show', 'update', 'destroy', 'create']]);

        Route::group(['namespace' => 'Auth'], function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login')->name('login');
            Route::post('logout', 'LoginController@logout')->name('logout');
        });
    });

    Route::group(['middleware' => 'auth:solicitante'], function () {
        Route::post('solicitacao/recurso', 'SolicitacaoController@recurso')->name('solicitacao.recurso');
        Route::resource('solicitacao', 'SolicitacaoController', ['except' => ['edit', 'update', 'destroy']]);

        Route::get('usuario/ver', 'SolicitanteController@create')->name('usuario.ver');
        Route::get('usuario/alterar_senha', 'SolicitanteController@alteraSenha')->name('usuario.altera-senha');
        Route::put('usuario/alterar_senha', 'SolicitanteController@updateSenha')->name('usuario.update-senha');
        Route::resource('usuario', 'SolicitanteController', ['except' => ['show', 'destroy', 'create', 'store']]);
    });

    Route::resource('pagina', 'PaginaController');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.', 'middleware' => 'auth'], function () {
    Route::get('sic_fisico/remove-arquivo/{id}/{field}', 'SicFisicoController@destroyFile')->name('sic_fisico.destroy-file');
    Route::resource('sic_fisico', 'SicFisicoController');

    Route::resource('/', 'DashboardController');
    Route::resource('orgao', 'OrgaoController', ['except' => []]);
    Route::resource('movimentacao', 'MovimentacaoController', ['except' => []]);
    Route::resource('resposta', 'RespostaController', ['except' => []]);
    Route::resource('pagina', 'PaginaController', ['except' => []]);
    Route::resource('solicitacao', 'SolicitacaoController', ['except' => []]);
    Route::resource('solicitante', 'SolicitanteController', ['except' => []]);


    Route::get('resposta/homologar/{solicitacao_id}/{resposta_id}', 'RespostaController@homologar')->middleware(['can:user-central'])->name('resposta.homologar');

    Route::post('solicitacao/prorrogar/{id}', 'SolicitacaoController@prorrogar')->middleware(['can:user-central'])->name('solicitacao.prorrogar');
    Route::get('solicitacao/receber/{id}', 'SolicitacaoController@receber')->name('solicitacao.receber');
    Route::get('solicitacao/receber_diligencia/{id}', 'SolicitacaoController@receber_diligencia')->name('solicitacao.receber_diligencia');
    Route::post('solicitacao/responder/{id}', 'SolicitacaoController@responder')->name('solicitacao.responder');
    Route::post('solicitacao/responder_diligencia/{id}', 'SolicitacaoController@responder_diligencia')->name('solicitacao.responder_diligencia');
    Route::post('solicitacao/tramitar/{id}', 'SolicitacaoController@tramitar')->name('solicitacao.tramitar');
    Route::post('solicitacao/diligencia/{id}', 'SolicitacaoController@diligencia')->name('solicitacao.diligencia');
    Route::get('solicitacao/diligencia/{id}', 'SolicitacaoController@ver_diligencia')->name('solicitacao.ver_diligencia');

    Route::middleware(['can:user-admin'])->group(function () {
        Route::resource('perfil', 'PerfilController', ['except' => ['show']]);
        Route::resource('situacao', 'SituacaoController', ['except' => ['show']]);
        Route::resource('escolaridade', 'EscolaridadeController', ['except' => ['show']]);
        Route::resource('usuarios', 'UsersController', ['except' => []]);
        Route::resource('tipo_retorno', 'TipoRetornoController', ['except' => ['show']]);
        Route::resource('tipo_solicitacao', 'TipoSolicitacaoController', ['except' => ['show']]);
        Route::resource('tipo_movimentacao', 'TipoMovimentacaoController', ['except' => ['show']]);
        Route::resource('faixa-etaria', 'FaixaEtariaController', ['except' => ['show']]);
        Route::resource('tipo_telefone', 'TipoTelefonesController', ['except' => ['show']]);
        Route::resource('config_entidade', 'ConfiguracaoEntidadeController', ['except' => ['show', 'destroy']]);
        Route::resource('config_lai', 'ConfiguracaoLaiController', ['except' => ['show', 'destroy']]);
    });
});

Route::group(['namespace' => 'Auth'], function () {
    //Login
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('logout', 'LoginController@logout')->name('logout');
    // Registration Routes...
    //Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    //Route::post('register', 'RegisterController@register');
    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
});
