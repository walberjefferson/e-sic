let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/assets/js/scripts.js');

// Custom
mix.sass('resources/assets/sass/style.scss', 'public/assets/css');

// Custom Municipio
mix.sass('resources/assets/sass/custom/smdm.scss', 'public/assets/css/custom');

// mix.scripts([
//     'node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
//     'node_modules/blueimp-file-upload/js/jquery.fileupload.js',
//     'node_modules/jquery-mask-plugin/dist/jquery.mask.js',
//     'node_modules/axios/dist/axios.js',
//     'resources/assets/js/masks.js'
// ], 'public/assets/js/all.js');

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
